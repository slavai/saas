<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
$this->breadcrumbs=array(
	UserModule::t("Profile View") => array('/user/profile'),
	UserModule::t("Change Password"),
);
$this->pageTitleContent = UserModule::t("Change Password");

$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'))
		:array()),
    ((UserModule::isAdmin())
        ?array('label'=>UserModule::t('List User'), 'url'=>array('/user'))
        :array()),
    array('label'=>UserModule::t('Profile View'), 'url'=>array('/user/profile')),
    array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
);
?>

<!-- general form elements -->
<div class="box box-primary">
    <div class="box-body">
        <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
            'id'=>'changepassword-form',
            'enableAjaxValidation'=>true,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>true,
                'validateOnType'=>true,
            ),
        )); ?>

        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
        <?php echo $form->errorSummary($model); ?>

        <div class="form-group">
            <?php echo $form->passwordFieldGroup($model,'oldPassword'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->passwordFieldGroup($model,'password'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->passwordFieldGroup($model,'verifyPassword'); ?>
        </div>

        <div class="form-actions text-right">
            <?php $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType' => 'submit',
                    'context' => 'primary',
                    'label' => UserModule::t('Save')
                )
            ); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div><!-- /.box-header -->
</div><!-- /.box -->
<div class="form">

</div><!-- form -->