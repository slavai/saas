<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\date\DatePicker;
use app\models\MyFormatter;
?>

<!-- Main content -->
<section class="patient-form clearfix">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header text-center">
                <?= Yii::t('app', 'PATIENT REFERRAL FORM'); ?>
            </h2>
        </div><!-- /.col -->
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'referral-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => true,
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-6 invoice-col">
            <label><?= Yii::t('app', "NAME") ?>:</label>
            <div class="m-bottom-8"><?= $patient->name ?></div>
        </div>
        <div class="col-md-6 invoice-col">
            <label><?= Yii::t('app', "S.S.#.:") ?>:</label>
            <div class="m-bottom-8"><?= MyFormatter::ssFormatter($patient->ss)?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 uppercase">
            <label><?= Yii::t('app', "address") ?>:</label>
            <div class="m-bottom-8"><?= nl2br($patient->address) ?></div>
        </div>
        <div class="col-md-6 uppercase">
            <label><?= Yii::t('app', "D.O.B.") ?>:</label>
            <div class="m-bottom-8"><?= $patient->birthday ?></div>
            <label><?= Yii::t('app', "phone") ?>:</label>
            <div class="m-bottom-8"><?= MyFormatter::phoneFormatter($patient->phone) ?></div>
        </div>
    </div>
    <hr class="bold-5">

    <h3 class="uppercase"><?= Yii::t('app', "insurance");?></h3>
    <div class="row">
        <div class="col-md-6 capitalize">
            <label><?= Yii::t('app', "Plan #1") ?>:</label>
            <div class="m-bottom-8"><?= $patient->insurance_plan1 ?></div>
        </div>
        <div class="col-md-6 capitalize">
            <label><?= Yii::t('app', "Policy No.") ?>:<span class="pull-right">(e.g., AARP)</span></label>
            <div class="m-bottom-8"><?= $patient->insurance_policy1 ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 capitalize">
            <label><?= Yii::t('app', "Plan #2") ?>:</label>
            <div class="m-bottom-8"><?= $patient->insurance_plan2 ?></div>
        </div>
        <div class="col-md-6 capitalize">
            <label><?= Yii::t('app', "Policy No.") ?>:<span class="pull-right">(e.g., Medicare)</span></label>
            <div class="m-bottom-8"><?= $patient->insurance_policy2 ?></div>
        </div>
    </div>
    <hr class="bold-5">

    <h3 class="uppercase"><?= Yii::t('app', "emergency contact");?></h3>
    <div class="row">
        <div class="col-md-6 capitalize">
            <label><?= Yii::t('app', "name") ?>:</label>
            <div class="m-bottom-8"><?= $patient->emergency_contact_name ?></div>
        </div>
        <div class="col-md-6 capitalize">
            <label><?= Yii::t('app', "phone") ?>:</label>
            <div class="m-bottom-8"><?= $patient->emergency_contact_phone ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 capitalize">
            <label><?= Yii::t('app', "address") ?>:</label>
            <div class="m-bottom-8"><?= nl2br($patient->emergency_contact_address) ?></div>
        </div>
    </div>
    <hr class="bold-5">

    <div class="row">
        <div class="col-md-12 capitalize">
            <label><?= Yii::t('app', "primary diagnosis") ?>:</label>
            <div class="m-bottom-8"><?= nl2br($patient_diagnosis->primary_diagnosis) ?></div>
        </div>
    </div>
    <div class="row capitalize"><p class="col-md-12"><strong>medically necessary home care services:</strong></p>
        <div class="col-md-12">
            <?php echo $form->field($patient_diagnosis, 'necessary_home_services')->inline(true)
                ->checkboxList($necessary, [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        ($checked) ? $val_checked = 'checked' : $val_checked = '';
                        return '<label class="col-sm-4 styleCheck">
                            <input type="checkbox" name="'.$name.'" '.$val_checked.' value="'.$value.'" disabled>
                            <span class="mark pull-left"></span>
                            <span class="txt pull-left">'.$label.'</span>
                        </label>';
                    },
                    'class' => 'frameSelector levelSelector'])->label(false) ?>
        </div>
    </div>

    <hr class="bold-5">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">IF PATIENT’S PRIMARY INSURANCE IS <span class="underline">TRADITIONAL MEDICARE</span>,<br>
                PLEASE COMPLETE THIS SECTION</h3>
            <label><?= Yii::t('app', "DATE OF LAST FACE TO FACE ENCOUNTER") ?>:</label>
            <?php if ($face_to_face != NULL) : ?>
                <div class="m-bottom-8"><?= $face_to_face->date_of_drawing ?></div>
            <?php else: ?>
                <div class="m-bottom-8"><?= Yii::t('app', "No FACE TO FACE ENCOUNTER") ?></div>
            <?php endif ?>
            <?= Yii::t('app', "Traditional Medicare patients are required to have a face to face encounter with a MD, APRN or PA within 90 days prior
            to, or 30 days following, the start of home care.") ?>
        </div>
        <div class="col-md-12 m-top-20">
            <label><?= Yii::t('app', "CLINICAL FINDINGS TO SUPPORT NEED FOR HOME CARE") ?>:</label>
            <div class="m-bottom-8"><?= nl2br($patient->clinical_findings) ?></div>
        </div>
        <div class="col-md-12">
            <label><?= Yii::t('app', "REASON PATIENT IS HOMEBOUND") ?>:</label>
            <div class="m-bottom-8"><?= $patient->reason_patient_homebound ?></div>
        </div>
        <div class="col-md-7">
            <label><?= Yii::t('app', "PHYSICIAN SIGNATURE") ?>:</label>
            <div class="m-bottom-8"><?= $physician->name ?></div>
        </div>
        <div class="col-md-5">
            <label><?= Yii::t('app', "DATE") ?>:</label>
            <div class="m-bottom-8"><?= date('m/d/Y', $patient->date_create) ?></div>
        </div>

        <div class="col-md-12">
            <label><?= Yii::t('app', "PHYSICIAN NAME PRINTED") ?>:</label>
            <div class="m-bottom-8"><?= $physician->name ?></div>
        </div>

    </div>

    <hr class="bold-5">
    <div class="text-center">
        <h4>PLEASE CALL TO CONFIRM OUR RECIEPT OF THIS FAX</h4>
        <h4>
<!--            --><?//= $form->field($model, 'population')->checkbox(); ?>
        CHECK BOX IF NEXT DAY VISIT NEEDED</h4>
        <h4>FAX LINES: 203.458.4388 or 1.866.862.0999 (toll free)</h4>
        <h4>INTAKE LINES: 203.458.4275 or 1.866.862.0888 (toll free)</h4>
    </div>

    <?php ActiveForm::end(); ?>
</section>