<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
?>
<section class="patient-form clearfix">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header text-center">
                <?= Yii::t('app', "PHYSICIAN FACE-TO-FACE ENCOUNTER FORM"); ?>
            </h2>
        </div><!-- /.col -->
    </div>
    <?php
        $option = [
            'clientOptions' => ['height' => 100],
            'options' => ['rows' => 6],
            'preset' => 'basic'
        ];
    ?>
    <?php $form = ActiveForm::begin([
        'id' => 'face-to-face-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => false,
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'offset' => '',
                'wrapper' => 'col-md-12',
            ],
        ],
        'options' => ['enctype' => 'multipart/form-data','class' => 'form-full-width-field']
    ]); ?>
    <!-- info row -->
    <div class="row m-top-20">
        <div class="col-md-9 invoice-col">
            <div class="form-group">
                <label for="patients-name" class="control-label"><?= Yii::t('app', "Patient Name") ?>:</label>
                <div class="form-group">
                    <div class="form-control"><?= $patient->name ?></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 invoice-col">
            <div class="form-group">
                <label for="patients-birthday" class="control-label"><?= Yii::t('app', "Date of Birth") ?>:</label>
                <div class="form-group">
                    <div value="1445299200" class="datePickerJob form-control"><?= $patient->birthday ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-top-20">
        <p class="col-md-12" style="margin: 0;"><strong><?= Yii::t('app', "Date of Face-to-Face Encounter:") ?>&nbsp;</strong>
            <?= Yii::t('app', "I certify that this patient is under my care and that I, or a Nurse
            Practitioner or Physician Assistant working with me, had a face-to-face encounter with this patient that
            meets the physician face-to-face encounter requirements (please insert date that visit occurred).") ?></p>

        <div class="col-md-2 invoice-col">
            <div class="form-group">
                <div class="form-group">
                    <div class="form-control"><?= $encounter->date_encounter ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-top-20">
        <p class="col-md-12"><b><?= Yii::t('app', "Medical Condition:")?> </b><?= Yii::t('app', "The encounter with the patient was in whole, or in part, for the following medical
                condition which is the primary reason for home care. (Please list") ?>&nbsp;<b><?= Yii::t('app', "ALL") ?></b>&nbsp;<?= Yii::t('app', "medical conditions).") ?></p>

        <div class="medical-condition col-md-12">
            <div class="m-bottom-8"><?= nl2br($encounter->medical_conditions) ?></div>
        </div>
    </div>

    <div class="row m-top-20"><p class="col-md-12"><strong><?= Yii::t('app', "Medical Necessity") ?>:</strong>
        I certify, that based on my findings, the following services are medically necessary
        home care services (Please check all that apply).
        </p>
        <div class="col-md-12">
            <?= $form->field($encounter, 'medical_necessity')->inline(true)
                ->checkboxList($necessity_list, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    ($checked) ? $val_checked = 'checked' : $val_checked = '';
                    return '
                        <label class="col-sm-6 styleCheck">
                            <input type="checkbox" '.$val_checked.' name="'.$name.'" value="'.$value.'" disabled>
                            <span class="mark pull-left"></span>
                            <span class="txt pull-left">'.$label.'</span>
                        </label>
                    ';
                },
            'class' => 'frameSelector levelSelector'])->label(false) ?>
        </div>
    </div>
    <div class="row m-top-20"><p class="col-md-12"><strong><?= Yii::t('app', "Clinical Findings")?>:</strong>
            My clinical findings support the need for the above services <span class="text-underline">because</span>:
        </p>
        <div class="medical-condition col-md-12">
            <div class="m-bottom-8"><?= nl2br($encounter->clinical_findings) ?></div>
        </div>
    </div>
    <div class="row m-top-20"><p class="col-md-12"><strong><?= Yii::t('app', "Homebound Status") ?>:</strong>
            Further, I certify that my clinical findings support that this patient is homebound because:
        </p>
        <div class="medical-condition col-md-12">
            <div class="m-bottom-8"><?= nl2br($encounter->homebound_status) ?></div>
        </div>
    </div>
    <div class="row m-top-30">
        <div class="col-md-9 invoice-col">
            <div class="form-group field-physician-name required">
                <label class="control-label"><dt><?= Yii::t('app', "Physician Signature") ?>:</dt></label>
                <div class="form-group">
                    <div class="form-control"><?= $physician->name ?></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 invoice-col">
            <label><dt><?= Yii::t('app', "Date") ?>: </dt></label>
            <div class="form-group">
                <div class="form-control"><?= $encounter->date_of_drawing ?></div>
            </div>
        </div>
        <div class="col-md-12 invoice-col">
            <label><dt><?= Yii::t('app', "Physician Printed Name") ?>: </dt></label>
            <div class="form-group">
                <div class="form-control"><?= $physician->name ?></div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>

