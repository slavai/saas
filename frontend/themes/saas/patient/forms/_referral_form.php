<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\date\DatePicker;
?>

<!-- Main content -->
<section class="patient-form clearfix">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header text-center">
                <?= Yii::t('app', 'PATIENT REFERRAL FORM'); ?>
            </h2>
        </div><!-- /.col -->
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'referral-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => true,
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-6 invoice-col">
            <?= $form->field($patient, 'name')->label(Yii::t('app', "NAME") . ':'); ?>
        </div>
        <div class="col-md-6 invoice-col">
            <?= $form->field($patient,'ss')->widget(MaskedInput::className(), [
                'mask' => '***/**/***',
            ])->label(Yii::t('app', "S.S.#.:")) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 uppercase">
            <?= $form->field($patient, 'address')->textarea(array('rows'=>4))
                ->label(Yii::t('app', "address:")); ?>
        </div>
        <div class="col-md-6 uppercase">
            <?= $form->field($patient,'birthday')->widget(DatePicker::classname(), [
                'options' => ['class' => 'datePickerJob', 'id'=>'patient-birthday-referral-form'],
                'type' => DatePicker::TYPE_INPUT,
                'language' => Yii::$app->language,
                'size' => 'md',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'mm/dd/yyyy'
                ]
            ])->label(Yii::t('app', "D.O.B.:")) ?>
            <?= $form->field($patient,'phone')->widget(MaskedInput::className(), [
                'mask' => '(999)999-9999',
                'clientOptions'=>[
                    'removeMaskOnSubmit' => true,
                ]
            ])->label(Yii::t('app', "phone:")) ?>
        </div>
    </div>
    <hr class="bold-5">

    <h3 class="uppercase"><?= Yii::t('app', "insurance");?></h3>
    <div class="row">
        <div class="col-md-6 capitalize">
            <?= $form->field($patient, 'insurance_plan1')->inline()->label(Yii::t('app', "Plan #1:")); ?>
        </div>
        <div class="col-md-6 capitalize">
            <?= $form->field($patient, 'insurance_policy1')->label(Yii::t('app', "Policy No.:").'<span class="pull-right">(e.g., AARP)</span>') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 capitalize">
            <?= $form->field($patient, 'insurance_plan2')->label(Yii::t('app', "Plan #2:")); ?>
        </div>
        <div class="col-md-6 capitalize">
            <?= $form->field($patient, 'insurance_policy2')->label(Yii::t('app', "Policy No.:").'<span class="pull-right">(e.g., Medicare)</span>'); ?>
        </div>
    </div>
    <hr class="bold-5">

    <h3 class="uppercase"><?= Yii::t('app', "emergency contact");?></h3>
    <div class="row">
        <div class="col-md-6 capitalize">
            <?= $form->field($patient, 'emergency_contact_name')->label(Yii::t('app', "name:")); ?>
        </div>
        <div class="col-md-6 capitalize">
            <?= $form->field($patient,'emergency_contact_phone')->label(Yii::t('app', "phone:")) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 capitalize">
            <?= $form->field($patient, 'emergency_contact_address')->textarea(array('rows'=>2))
                ->label(Yii::t('app', "address:")); ?>
        </div>
    </div>
    <hr class="bold-5">

    <div class="row">
        <div class="col-md-12 capitalize">
            <?= $form->field($patient_diagnosis, 'primary_diagnosis')->textarea(array('rows'=>2))->label(Yii::t('app', "primary diagnosis:")); ?>
        </div>
    </div>
    <div class="row capitalize"><p class="col-md-12"><strong>medically necessary home care services:</strong></p>
        <div class="col-md-12">
            <?php echo $form->field($patient_diagnosis, 'necessary_home_services')->inline(true)
                ->checkboxList($necessary, [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return '<label class="col-sm-4 styleCheck">
                            <input type="checkbox" name="'.$name.'" value="'.$value.'" class="">
                            <span class="mark pull-left"></span>
                            <span class="txt pull-left">'.$label.'</span>
                        </label>';
                    },
                    'class' => 'frameSelector levelSelector'])->label(false) ?>
        </div>
    </div>

    <hr class="bold-5">
    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">IF PATIENT’S PRIMARY INSURANCE IS <span class="underline">TRADITIONAL MEDICARE</span>,<br>
                PLEASE COMPLETE THIS SECTION</h3>
            <?= $form->field($face_to_face, 'date_of_drawing',
                ['enableError' => false,
                    'inputOptions' => ['class'=>'datePickerJob'],
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-sm-2',
                        'label' => 'col-sm-2',
                    ]])
                ->widget(DatePicker::classname(), [
                    'options' => ['class' => 'datePickerJob'],
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'size' => 'md',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'mm/dd/yyyy'
                    ]
                ])->label(Yii::t('app', "DATE OF LAST FACE TO FACE ENCOUNTER:"));?>
            Traditional Medicare patients are required to have a face to face encounter with a MD, APRN or PA within 90 days prior
            to, or 30 days following, the start of home care.
        </div>
        <div class="col-md-12 m-top-20">
            <?= $form->field($patient, 'clinical_findings')->textarea(array('rows'=>2))
                ->label(Yii::t('app', "CLINICAL FINDINGS TO SUPPORT NEED FOR HOME CARE:")); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($patient, 'reason_patient_homebound')
                ->label(Yii::t('app', "REASON PATIENT IS HOMEBOUND:")); ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($physician, 'name')
                ->label(Yii::t('app', "PHYSICIAN SIGNATURE:")); ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($patient, 'date_create',
                ['enableError' => false,
                    'inputOptions' => ['class'=>'datePickerJob'],
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-sm-2',
                        'label' => 'col-sm-2',
                    ]])
                ->widget(DatePicker::classname(), [
                    'options' => ['class' => 'datePickerJob'],
                    'type' => DatePicker::TYPE_INPUT,
                    'language' => Yii::$app->language,
                    'size' => 'md',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'mm/dd/yyyy'
                    ]
                ])->label(Yii::t('app', "DATE"));?>
        </div>

        <div class="col-md-12">
            <?= $form->field($physician, 'name')
                ->label(Yii::t('app', "PHYSICIAN NAME PRINTED:")); ?>
        </div>

    </div>

    <hr class="bold-5">
    <div class="text-center">
        <h4>PLEASE CALL TO CONFIRM OUR RECIEPT OF THIS FAX</h4>
        <h4>
<!--            --><?//= $form->field($model, 'population')->checkbox(); ?>
        CHECK BOX IF NEXT DAY VISIT NEEDED</h4>
        <h4>FAX LINES: 203.458.4388 or 1.866.862.0999 (toll free)</h4>
        <h4>INTAKE LINES: 203.458.4275 or 1.866.862.0888 (toll free)</h4>
        <p class="text-right"><?= Yii::t('app', 'Created') ?> <?=date('m').'/'.date('d').'/'.date('Y')?></p>
    </div>
    <?= Html::submitButton(Yii::t('app', 'Save & Send'), ['class'=> 'btn btn-primary pull-right']) ;?>
    <?php ActiveForm::end(); ?>
</section>