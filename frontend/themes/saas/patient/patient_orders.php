<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
?>

<button style="margin-bottom: 10px;" type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#sendRequestMedication"><?= Yii::t('app', "Send request"); ?></button>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Pending Medication/Treatment Orders Requests') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?= Yii::t('app', 'Date Requested')?></th>
                            <th><?= Yii::t('app', 'Medication/Treatment Order details')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 0;
                            if (count($order_patient) > 0) : ?>
                            <?php foreach($order_patient as $order) : ?>
                                <?php if ($order->type == \app\models\OrdersRequest::AGENCY_SEND && $order->date_resend == NULL) {
                                    $count++;
                                    ?>
                                    <tr>
                                        <td><?= date('m/d/Y', (int)$order->date_create); ?></td>
                                        <td><?= $order->text ?></td>
                                    </tr>
                                <?php } ?>
                            <?php endforeach; ?>
                            <?php if ($count == 0) : ?>
                                <tr>
                                    <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                                </tr>
                            <?php endif; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'New Medication/Treatment Orders Signed') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?= Yii::t('app', 'Date Sent To HHA')?></th>
                            <th><?= Yii::t('app', 'Medication/Treatment Order Sent')?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 0;
                        if (count($order_patient) > 0) { ?>
                            <?php foreach($order_patient as $order) : ?>
                                <?php
                                    if ($order->type == \app\models\OrdersRequest::AGENCY_SEND && $order->date_resend != NULL && $order->date_accept == NULL) {
                                        $count++;
                                ?>
                                    <tr>
                                        <td><?= date('m/d/Y', (int)$order->date_resend); ?></td>
                                        <td><?= $order->text ?></td>
                                        <td style="width: 1%"><a class="accept-patient btn btn-sm bg-olive" data-placement="bottom" title="<?= Yii::t('app', 'Accept Order') ?>" data-toggle="tooltip" href="javascript:;" onclick="App.Api.acceptOrder(<?=$order->id ?>,this);"><?= Yii::t('app', 'OK') ?></a></td>
                                    </tr>
                                <?php } elseif($order->type == \app\models\OrdersRequest::PHYSICIAN_SEND && $order->date_accept == NULL) {
                                        $count++;
                                        ?>
                                        <tr>
                                            <td><?= date('m/d/Y', (int)$order->date_create); ?></td>
                                            <td><?= $order->text ?></td>
                                            <td style="width: 1%"><a class="accept-patient btn btn-sm bg-olive" data-placement="bottom" title="<?= Yii::t('app', 'Accept Order') ?>" data-toggle="tooltip" href="javascript:;" onclick="App.Api.acceptOrder(<?=$order->id ?>,this);"><?= Yii::t('app', 'OK') ?></a></td>
                                        </tr>
                                <?php  } ?>
                            <?php endforeach; ?>
                            <?php if ($count == 0) : ?>
                                <tr>
                                    <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                                </tr>
                            <?php endif; ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'History of Medication and Treatment Orders') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover history-medication">
                    <thead>
                        <tr>
                            <th><?= Yii::t('app', 'Date Orders Fulfilled')?></th>
                            <th><?= Yii::t('app', 'Medication/Treatment Order Sent')?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 0;
                        if (count($order_patient) > 0) : ?>
                            <?php foreach($order_patient as $order) : ?>
                                <?php if ($order->date_accept != NULL) {
                                    $count++;
                                    ?>
                                    <tr>
                                        <td><?= date('m/d/Y', (int)$order->date_accept); ?></td>
                                        <td><?= $order->text ?></td>
                                    </tr>
                                <?php } ?>
                            <?php endforeach; ?>
                            <?php if ($count == 0) : ?>
                                <tr>
                                    <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                                </tr>
                            <?php endif; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
