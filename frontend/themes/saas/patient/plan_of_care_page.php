<?php
use common\models\LoginData;
$this->title = Yii::$app->name.' - '.Yii::t('app','Plan of Care');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plan of Care')];
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Plan of Care'); ?></h3>
                <?php if($plan_care->isNewRecord && $role == LoginData::TYPE_AGENCY_NAME) { ?>
                    <span class='label label-warning pull-right'>The form hasn't been sent yet</span>
                <?php } else { ?>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <div class="btn-group">
                            <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>

            </div><!-- /.box-header -->
            <?php if(!$plan_care->isNewRecord) : ?>
                <div class="box-body">
                    <?=
                    $this->render('forms/_plan_of_care', [
                        'patient' => $patient,
                        'agencys' => $agencys,
                        'physician' => $physician,
                        'plan_care' => $plan_care,
                        'fun_limitations' => $fun_limitations,
                        'activities_permitted' => $activities_permitted,
                        'mental_status' => $mental_status,
                        'prognosis' => $prognosis,
                        'data' => $data
                    ]); ?>
                </div><!-- ./box-body -->
            <?php endif; ?>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
