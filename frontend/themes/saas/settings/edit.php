<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\ckeditor\CKEditor;
use common\models\LoginData;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

$this->title = Yii::$app->name . ' - '. Yii::t('app', "My Profile");

$this->params['breadcrumbs'][] = Yii::t('app', 'Profile Detail');
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit');

$this->params['pageTitleContent'] = Yii::t('app', 'My Profile');
$this->params['pageSubTitleContent'] = Yii::t('app','Edit');

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyAH3VMe0m6UPiblV96_PsAGS0aaCOC-JnM&libraries=places', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/account_info.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$option = [
    'clientOptions' => ['height' => 200],
    'preset' => 'basic'
];

?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu',[
            'model'=>$model,
            'detail'=>$detail
        ]) ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id'          => 'detail-form',
                    'options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template'     => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => true,
                ]); ?>

                <?= $form->field($detail,'name'); ?>

                <?php if ($role == LoginData::TYPE_PHYSICIAN_NAME) { ?>

                        <?= $form->field($detail,'manager_name'); ?>

                <?php } elseif ($role == LoginData::TYPE_AGENCY_NAME) { ?>

                        <?= $form->field($detail,'admin_name'); ?>

                <?php } ?>

                <?= $form->field($detail, 'avatar_filename')->widget(FileInput::classname(), [
                    'options' => ['multiple' => false, 'accept' => 'image/*'],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'browseLabel' => '',
                        'removeLabel' => '',
                        'removeClass' => 'btn btn-danger',
                        'mainClass' => 'input-group-sm',
                        'initialPreview' => [
                            Html::img($detail->logo, ['class' => 'file-preview-image'])
                        ],
                    ]
                ]); ?>

                <?= $form->field($detail,'phone')->widget(MaskedInput::className(), [
                    'mask' => '(999)999-9999',
                    'clientOptions'=>[
                        'removeMaskOnSubmit' => true,
                    ]
                ]); ?>

                <?= $form->field($detail,'phone_fax')->widget(MaskedInput::className(), [
                    'mask' => '(999)999-9999',
                    'clientOptions'=>[
                        'removeMaskOnSubmit' => true,
                    ]
                ]); ?>

                <?= $form->field($detail,'address'); ?>

                <?= $form->field($detail, 'summary', [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-md-12',
                    ]
                ])->widget(CKEditor::className(), $option); ?>

                <?php if ($role == LoginData::TYPE_PHYSICIAN_NAME) { ?>

                    <?= $form->field($detail,'physician_email')->input('email'); ?>

                    <?= $form->field($detail,'education'); ?>

                    <?= $form->field($detail,'license'); ?>

                    <?= $form->field($detail,'years_practice'); ?>

                    <?= $form->field($detail, 'specialization', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                <?php } elseif ($role == LoginData::TYPE_AGENCY_NAME) { ?>

                    <?= $form->field($detail,'agency_email')->input('email');; ?>

                    <?= $form->field($detail, 'skilled_nursing', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                    <?= $form->field($detail, 'physical_therapy', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                    <?= $form->field($detail, 'speech_language_therapy', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                    <?= $form->field($detail, 'occupational_therapy', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                    <?= $form->field($detail, 'home_health_aide', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                    <?= $form->field($detail, 'medical_social_services', [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-md-12',
                        ]
                    ])->widget(CKEditor::className(), $option); ?>

                <?php } ?>

                <?= $form->field($detail, 'city', [
                    'inputOptions' => [
                        'id' => 'autocomplete-city',
                        'placeholder' => $detail->getAttributeLabel('city'),
                    ]]); ?>

                <?= $form->field($detail, 'state', [
                    'inputOptions' => [
                        'placeholder' => $detail->getAttributeLabel('state'),
                    ]]); ?>

                <?php
                    $phoneCode = [];
                    foreach ($data['countries'] as $country)
                        $phoneCode[$country->countries_iso_code_2] = array('data-phone-code' => $country->phone_prefix);

                    echo $form->field($detail, 'country_code')->listBox(
                        ArrayHelper::map($data['countries'], 'countries_iso_code_2',
                            function($model, $defaultValue) {
                                return $model->countries_name.' (+'.$model->phone_prefix.')';
                            }),
                        ['prompt'=>Yii::t('app', 'Country'), 'multiple' => false, 'size'=>1, 'id'=>'field_country']
                    )->label(Yii::t('app', 'Country').':');
                ?>

                <?= $form->field($detail, 'postCode', [
                    'inputOptions' => [
                        'placeholder' => $detail->getAttributeLabel('postCode'),
                    ]]); ?>

                <?= Html::submitButton('Save', ['class'=> 'btn btn-primary pull-right']) ;?>

            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->

