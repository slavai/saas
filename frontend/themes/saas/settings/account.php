<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $model dektrium\user\models\SettingsForm
 */

$this->title = Yii::t('app', 'Change password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu', [
            'model'=>$model,
            'detail'=>$detail
        ]) ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php $form_changepassword = ActiveForm::begin([
                    'id' => 'changePassword',
                    'options' => ['class' => 'form-horizontal'],
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                    'fieldConfig' => [
                        'template'     => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'layout' => 'horizontal'
                ]); ?>

                <?= $form_changepassword->field($model, 'currentPassword', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('app', 'Old password'),
                    ]])->passwordInput() ?>
                <?= $form_changepassword->field($model, 'passwordNew', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('app', 'New password'),
                    ]])->passwordInput() ?>
                <?= $form_changepassword->field($model, 'verifyPassword', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('app', 'Verify password'),
                    ]])->passwordInput() ?>
                <div class="row">
                    <div class="col-sm-2 pull-right">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class'=> 'btn btn-block btn-success']) ;?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
