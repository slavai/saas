<?php

Yii::$app->layout = 'clean';
?>

<div class="row">
    <div class="col-sm-6">
        <h1><?= Yii::t('app', 'oops... something went wrong.'); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <p><?= Yii::t('app', 'The page you were looking for appears to have been moved, deleted or does not exist. You could go back to where you were or head straight to our home page.'); ?></p>
        <p><a href="#" onclick="window.history.back();" class="btn btn-primary"><?= Yii::t('app', 'Go back'); ?></a></p>
    </div>
</div>

<?php if (YII_ENV === 'dev'): ?>
    
    <div class="debug" style="margin-top: 250px">
        <hr/>
        <h2>DEBUG</h2>
        <div>
             <label for="">Message:</label> <?= $exception->getMessage() ?>           
        </div>
        <div>
            <label for="">File:</label> <?= $exception->getFile() ?>
        </div>
        <div>
            <label for="">Line:</label> <?= $exception->getLine() ?>
        </div>
    </div>

    <pre><?= $exception->getTraceAsString() ?></pre>
<?php endif; ?>