<?php
/**
 * Created by PhpStorm.
 * User: zhuk
 * Date: 19.10.15
 * Time: 15:57
 */
use frontend\controllers\MessageController;
?>
<!-- Modal -->
<div class="modal fade" id="sendMessage"
     ng-init="chat.id = <?= MessageController::actionInitUserChat($user_data->id)->id ?>"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-recepient="<?= $user_data->id ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('app', "Send message to") ?>&nbsp;<b><?= $user_data->name ?></b></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissable disp-none">
                    <div class="error-message"><ul></ul></div>
                </div>
                <div class="input-group">
                    <textarea  ng-model="chat.message" ng-keypress="keyPressOnInputMessage($event);"
                               class="form-control message-text" rows="3">
                    </textarea>
                    <span class="input-group-addon" smilies-selector="chat.message" smilies-placement="right" smilies-title="Smilies"></span>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= Yii::t('app', "Close") ?></button>
                <button type="button" class="btn btn-primary send-message" ng-click="sendPrivateMessage()">
                    <?= Yii::t('app', "Send") ?>
                </button>
            </div>
            <div class="section-fixed section-loading disp-none">
                <div class="loading-icon"><img src="/app/images/loading.gif" alt=""/></div>
            </div>
        </div>
    </div>
</div>