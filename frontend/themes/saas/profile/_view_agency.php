<?php
use yii\helpers\Html;
use yii\web\UrlManager;
use \app\models\MyFormatter;
use \yii\helpers\Url;

//use nfy\extensions\webNotifications\WebNotifications;
$this->registerJsFile(Yii::$app->request->baseUrl.'/app/js/message.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::$app->name.' - '.Yii::t('app', 'Agency').' - ' . $user_data->name;
$this->params['breadcrumbs'][] = $user_data->name;

$this->params['pageTitleContent'] = Yii::t('app', 'Profile View');
$this->params['pageSubTitleContent'] = Yii::t('app','Agency');

//$this->widget('nfy.extensions.webNotifications.WebNotifications', array('url'=>$this->createUrl('/nfy/default/poll', array('id'=>'queueComponentId'))));
?>

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-body profile-card row">
                <div class="col-sm-6 col-md-3">
                    <img src="<?= $user_data->logo ?>" alt="" class="img-rounded img-responsive" />
                </div>
                <div class="col-sm-6 col-md-9 profile-overview-content">
                    <h2 class="text-left margin-left-8"><?= $user_data->name?></h2>
                    <h5 class="lastactive">
                        <?= Yii::t('app', "Last login") ?>:
                        <?= ($model->last_login != 0) ? yii\timeago\TimeAgo::widget(['timestamp' => $model->last_login]) : 'never'?>
                    </h5>
                    <div class="box-body no-padding">
                        <table class="table table-noborder">
                            <?php if (!empty($user_data->admin_name)): ?>
                                <tr>
                                    <th><?= Yii::t('app', "Admin Name");?></th>
                                    <td>
                                        <?= $user_data->admin_name;  ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (count($userLocation) > 0) : ?>
                            <tr>
                                <th><?= Yii::t('app', "Address");?></th>
                                <td>
                                    <?= implode($userLocation, ', '); ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <?php if ($user_data->postCode): ?>
                                <tr>
                                    <th><?= Yii::t('app', "Post Code");?></th>
                                    <td>
                                        <?= $user_data->postCode; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (!empty($user_data->phone)): ?>
                                <tr>
                                    <th><?= Yii::t('app', "Phone");?></th>
                                    <td>
                                        <?= MyFormatter::phoneFormatter($user_data->phone);  ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (!empty($user_data->phone_fax)): ?>
                                <tr>
                                    <th><?= Yii::t('app', "Phone Fax");?></th>
                                    <td>
                                        <?= MyFormatter::phoneFormatter($user_data->phone_fax);  ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (!empty($user_data->agency_email)): ?>
                                <tr>
                                    <th><?= Yii::t('app', "Email");?></th>
                                    <td>
                                        <?= Html::a(Html::encode($user_data->agency_email), 'mailto:' . Html::encode($user_data->agency_email)) ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (!empty($user_data->summary)): ?>
                                <tr>
                                    <th><?= Yii::t('app', "Summary");?></th>
                                    <td>
                                        <?= $user_data->summary;  ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                    <?php if ($model->id != \Yii::$app->user->id) : ?>
                    <div class="btn-group pull-right margin-top-12">
                        <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#sendMessage"><?= Yii::t('app', "Send message"); ?></button>
                        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><?= Html::a(Yii::t('app', "Open chat page"), Url::toRoute(['/message', 'shortId' => $model->short_id], true)); ?></li>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-briefcase"></i><h3 class="box-title margin-left-2"><?= Yii::t('app', "Professional details"); ?></h3>
            </div>
            <div class="box-body">
                <?php if (!empty($user_data->skilled_nursing)): ?>
                    <div>
                        <h4><?= Yii::t('app', "Skilled-Nursing");?></h4>
                        <span class="volunteering-interests-hint volunteering-interests-special-header"><?= $user_data->skilled_nursing; ?></span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($user_data->physical_therapy)): ?>
                    <div>
                        <h4><?= Yii::t('app', "Physical Therapy");?></h4>
                        <span class="volunteering-interests-hint volunteering-interests-special-header"><?= $user_data->physical_therapy; ?></span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($user_data->speech_language_therapy)): ?>
                    <div>
                        <h4><?= Yii::t('app', "Speech-Language Therapy");?></h4>
                        <span class="volunteering-interests-hint volunteering-interests-special-header"><?= $user_data->speech_language_therapy; ?></span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($user_data->occupational_therapy)): ?>
                    <div>
                        <h4><?= Yii::t('app', "Occupational Therapy");?></h4>
                        <span class="volunteering-interests-hint volunteering-interests-special-header"><?= $user_data->occupational_therapy; ?></span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($user_data->home_health_aide)): ?>
                    <div>
                        <h4><?= Yii::t('app', "Home Health Aide");?></h4>
                        <span class="volunteering-interests-hint volunteering-interests-special-header"><?= $user_data->home_health_aide; ?></span>
                    </div>
                <?php endif; ?>
                <?php if (!empty($user_data->medical_social_services)): ?>
                    <div>
                        <h4><?= Yii::t('app', "Medical Social Services");?></h4>
                        <span class="volunteering-interests-hint volunteering-interests-special-header"><?= $user_data->medical_social_services; ?></span>
                    </div>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>

<?php
    echo $this->render('_modal_message', [
        'user_data' => $user_data
    ]);
?>
