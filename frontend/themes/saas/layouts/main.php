<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\BowerAsset;
use kartik\alert\AlertBlock;
use kartik\alert\Alert;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Url;
use frontend\controllers\SiteController;

/* @var $this \yii\web\View */
/* @var $content string */

BowerAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <?php $this->head() ?>
    <script>
        var socket = io.connect('<?= Yii::$app->params['socketUrl'] ?>');
    </script>
</head>
<body class="skin-blue layout-boxed" ng-app="ChatApp" ng-controller="ChatController">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header clearfix container-fluid">
            <div id="header" class="row skin-blue">
                <?php
                    NavBar::begin([
                        'brandLabel' => '<img src= "/app/images/logo.png" class="img-responsive logo">',
                        'brandUrl' => Yii::$app->homeUrl,
                        'options' => [
                            'class' => 'navbar navbar-default',
                        ],
                    ]);
                    $menuItems = SiteController::getMenuItems();

//                    Yii::$app->user->isGuest ?
//                        $menuItems[] = ['label' => 'Sign in', 'url' => ['/user/security/login']] :
//                        $menuItems[] = ['label' => 'Sign out (' . Yii::$app->user->identity->username . ')',
//                            'url' => ['/user/security/logout'],
//                            'linkOptions' => ['data-method' => 'post']];
//                    $menuItems[] = ['label' => 'Register', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest];

                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-right'],
                        'items' => $menuItems,
                    ]);
                    NavBar::end();
                ?>
            </div>
            <div id="header" class="row skin-blue">
            <?php
            if (!Yii::$app->user->isGuest) :
                ?>

                <!-- Header Navbar: style can be found in header.less -->
                <?php
                    NavBar::begin([
                        'options' => [
                            'class' => 'navbar-default',
                        ],
                    ]);

                    $menuSubItems = SiteController::getSubMenuItems();
                    $menuSubItemsRight = [
                        '<ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu"
                                ng-init="getCountUnreadMessages()">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" ng-click="getUnreadMessages()";>'.
                                    FA::icon('envelope').'
                                    <span class="label label-success message-title-count">{{ unreadMessages.count}}</span>
                                </a>
                                <ul class="dropdown-menu" >
                                    <li ng-show="unreadMessages.count > 0" class="header">
                                        '.Yii::t("app", "You have").' {{ unreadMessages.count}} '.Yii::t("app", "new messages").'
                                    </li>
                                    <li ng-show="unreadMessages.count == 0" class="header">
                                        '.Yii::t("app", "You doesn't have new messages").'
                                    </li>
                                    <li>
                                        <div ng-show="fetching" class="background-preloader">
                                            <span class="preloader"></span>
                                        </div>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu head-message  list-messages"
                                            scroll-messages="unreadMessages.messages"
                                            on-scroll="unread-message"
                                            read-msg="readMessage"
                                            >
                                            <li class="unread-message"
                                                ng-repeat="message in unreadMessages.messages"
                                                data-message="{{message}}"
                                                ng-mouseover="readMessage(message)"
                                                ng-class="{\'unread\': isUnreadMessage(message)}">
                                                <a href="/message?id={{ unreadMessages.users[message.sender.sender].short_id }}">
                                                    <div class="pull-left">
                                                        <img style="object-fit: cover;" ng-src="{{getUrlPrefix(unreadMessages.users[message.sender.sender].avatar_filename)}}" onError="this.onerror=null;this.src=\'https://s3-us-west-2.amazonaws.com/88creatives-dev/assets/avatar2.jpg\';" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        {{ unreadMessages.users[message.sender.sender].name }}
                                                        <small><i class="fa fa-clock-o"></i>{{ message.date_create}}</small>
                                                    </h4>
                                                    <p smilies="message.message">{{ message.message}}</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer" ng-show="unreadMessages.count > 0">
                                        <a href="/message?id={{ unreadMessages.messages[0].sender.sender}}">View all</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    '.FA::icon('bell').'
                                    <span class="label label-warning notifications-count-title"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">'.Yii::t("app", "You have").' <span class="notifications-count"></span> '.Yii::t("app", "notifications").'</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu head-notifications">
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span class="capitalize">'.$this->context->user->userData->name.' <i class="caret"></i></span>
                                </a>
                                <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img class="img-circle settings-global-ava" src="'. $this->context->user->userData->logo .'" 
                                        alt="'.$this->context->user->userData->name.'">
                                    <p class="capitalize">
                                        '. $this->context->user->userData->name .' - '.$this->context->userRole.'
                                        <small>Member since</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="'.Url::toRoute(['/settings/detail']).'" class="btn btn-default btn-flat">
                                            '. Yii::t("app", "Profile") .'
                                        </a>
                                    </div>
                                    <div class="pull-right">
                                        <a data-method="post" href="'.Url::toRoute(['/site/logout']).'"
                                            class="btn btn-default btn-flat">'. Yii::t("app", "Sign out") .'</a>
                                    </div>
                                </li>
                                </ul>
                            </li>
                        </ul>'
                    ];

                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-left'],
                        'items' => $menuSubItems,
                    ]);

                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-right'],
                        'items' => $menuSubItemsRight,
                    ]);
                    NavBar::end();
                ?>

            <?php endif;  ?>
            </div>
        </header>

        <div id="content" class="content-wrapper">
            <?php
            if (!Yii::$app->user->isGuest || isset($this->params['pageTitleContent'])) :
                ?>
                <aside class="top-side">
                    <section class="content-header clearfix">
                        <h1 class="pull-left">
                            <?php if(isset($this->params['pageTitleContent'])): ?>
                                <?= $this->params['pageTitleContent']; ?>
                            <?php endif ?>
                            <?php if(isset($this->params['pageSubTitleContent'])): ?>
                                <small><?= $this->params['pageSubTitleContent']; ?></small>
                            <?php endif ?>
                        </h1>
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= AlertBlock::widget([
//                            'type' => AlertBlock::TYPE_ALERT,
                            'useSessionFlash' => true,
                            'delay' => 0,
                            'options' => ['style' => 'clear:both']
                        ]); ?>
                    </section>
                </aside>
            <?php endif; ?>
            <section class="content">
                <?= $content ?>
            </section>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company 2015-<?= date('Y') ?></p>
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>
    </div>
    <div id="info-msg"></div>
    <?php if(!Yii::$app->user->isGuest): ?>
        <script>
            var current_user = {
                name: '<?= $this->context->user->userData->name ?>',
                user_id: '<?= \Yii::$app->user->id ?>'
            };
            socket.emit('init user', {user_id: current_user.user_id});
        </script>
    <?php endif; ?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
