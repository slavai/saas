<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Agencys */

$this->title = Yii::t('app', 'Create Agencys');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agencys-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
