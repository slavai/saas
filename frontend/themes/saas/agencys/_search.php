<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\AgencysSearch */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs(
    '$("document").ready(function(){

//        console.log("ertert");
        $("#new_id_agency").on("pjax:end", function() {
            console.log("ertert");
//            $.pjax.reload({container:"#grid-agency"}); //Reload GridView
            $(".grid-recommendation-agency").yiiGridView("applyFilter");
        });
    });'
);
?>

<div class="agencys-search">

    <?php
    Pjax::begin(['id' => 'new_id_agency']);
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['data-pjax' => true]
    ]); ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'admin_name') ?>

    <?= $form->field($model, 'phone') ?>

    <?= $form->field($model, 'phone_fax') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'agency_email') ?>

    <?php // echo $form->field($model, 'skilled_nursing') ?>

    <?php // echo $form->field($model, 'physical_therapy') ?>

    <?php // echo $form->field($model, 'speech_language_therapy') ?>

    <?php // echo $form->field($model, 'occupational_therapy') ?>

    <?php // echo $form->field($model, 'home_health_aide') ?>

    <?php // echo $form->field($model, 'medical_social_services') ?>

    <?php // echo $form->field($model, 'summary') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end();
    Pjax::end();?>

</div>
