<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Agencys */

$this->title = $model->id_user;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agencys-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_user], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_user], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_user',
            'name',
            'admin_name',
            'phone',
            'phone_fax',
            'address',
            'agency_email:email',
            'skilled_nursing:ntext',
            'physical_therapy:ntext',
            'speech_language_therapy:ntext',
            'occupational_therapy:ntext',
            'home_health_aide:ntext',
            'medical_social_services:ntext',
            'summary:ntext',
            'logo',
        ],
    ]) ?>

</div>
