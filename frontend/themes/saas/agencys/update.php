<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agencys */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Agencys',
]) . ' ' . $model->id_user;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id' => $model->id_user]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agencys-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
