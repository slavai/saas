<?php
/**
 * Created by PhpStorm.
 * User: zhuk
 * Date: 19.10.15
 * Time: 14:37
 */?>

<div class="modal fade modal-info" id="sendRequestMedication" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= Yii::t('app', "Send request medication") ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissable disp-none">
                    <div class="error-message"><ul></ul></div>
                </div>
                <textarea class="form-control message-text" rows="3"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><?= Yii::t('app', "Close") ?></button>
                <button type="button" class="btn btn-default request-medication"><?= Yii::t('app', "Send request") ?></button>
            </div>
            <div class="section-fixed section-loading disp-none">
                <div class="loading-icon"><img src="/app/images/loading.gif" alt=""/></div>
            </div>
        </div>
    </div>
</div>