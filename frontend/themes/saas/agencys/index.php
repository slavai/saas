<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Home Health Agency Dashboard';

$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Agency')];
$this->params['pageTitleContent'] = Yii::t('app','Agency');
$this->params['pageSubTitleContent'] = Yii::t('app','Landing page');
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('app', "New Patient Referrals") ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-danger"><?= Yii::t('app', "waiting for approval") ?></span>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Physician Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th></th>
                    </tr>
                    <?php if (count($patient_coming) > 0) : ?>
                        <?php foreach($patient_coming as $patient) : ?>
                            <tr class="patient">
                                <td><?= Html::a($patient->name, Url::toRoute(['patient/index', 'id' => $patient->id], true)) ?></td>
                                <td><?= Html::a($patient->physician->name, Url::toRoute(['profile/show', 'shortId' => $patient->physician->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($patient->physician->phone)?></td>
                                <td style="white-space:nowrap;" class="text-right">
                                    <?= Html::a(Yii::t('app', "View Patient Referral Form"), Url::toRoute(['patient/view-patient-referral-form', 'id' => $patient->id], true), [
                                        'title' => Yii::t('app', "View Patient Referral Form"),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'class' => 'btn btn-sm bg-orange'
                                    ]); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No referrals patient');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    <div class="col-xs-6">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('app', "Patient Queue Table") ?></h3>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Physician Name')?></th>
                        <th><?= Yii::t('app', 'Date') ?></th>
                        <th><?= Yii::t('app', 'Status') ?></th>
                        <th><?= Yii::t('app', 'Action Items') ?></th>
                    </tr>
                    <?php if (count($patient_queue) > 0) : ?>
                        <?php foreach($patient_queue as $param) : ?>
                            <tr class="patient">
                                <td><?= Html::a($param['patient_name'], Url::toRoute(['patient/index', 'id' => $param['patient_id']], true)) ?></td>
                                <td><?= Html::a($param['physician_name'], Url::toRoute(['profile/show', 'shortId' => $param['physician_short_id']], true)) ?></td>
                                <td><?= date('m/d/Y', $param['date']) ?></td>
                                <td><?= $param['status'] ?></td>
                                <td><?= $param['button'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No referrals patient');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>


