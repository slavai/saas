<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Recommendation Home Halth Agencies');

$this->params['breadcrumbs'][] = ['label' => 'Physician', 'url' => ['index']];
$this->params['pageTitleContent'] = Yii::t('app','Physician');
$this->params['pageSubTitleContent'] = Yii::t('app','Landing page');

$url = Url::to(['agencylist']);
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-warning">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <div class="box-tools physician-search">
                    <?php
                        echo Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'name',
                            'language' => Yii::$app->language,
                            'options' => ['placeholder' => 'Search ...'],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { console.log(params); return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                            'pluginEvents' => [
                                "change" => "function() { console.log('change'); }",
                                "select2:opening" => "function() { console.log('select2:opening'); }",
                                "select2:open" => "function() { console.log('open'); }",
                                "select2:closing" => "function() { console.log('close'); }",
                                "select2:close" => "function() { console.log('close'); }",
                                "select2:selecting" => "function() { console.log('selecting'); }",
                                "select2:select" => "function(markup) {
                                    $.pjax({
                                        type       : 'GET',
                                        url        : '/physician',
                                        container  : '#grid-recommendation-agency',
                                        data       : {'AgencysSearch[name]' : markup.params.data.text},
                                        push       : true,
                                        replace    : false,
                                        timeout    : 10000,
                                        'scrollTo' : false
                                    })}",
                                "select2:unselecting" => "function(par) {
                                    $.pjax({
                                        url         : '/physician',
                                        type        : 'GET',
                                        container   : '#grid-recommendation-agency',
                                        timeout     : 2000,
                                        data       : {'AgencysSearch[name]' : ''},
                                        push       : true,
                                        replace    : false,
                                        timeout    : 10000,
                                        'scrollTo' : false
                                    })}",
                                "select2:unselect" => "function() { console.log('unselect'); }"
                            ]
                        ]);
                    ?>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <div id="grid-view-selector"></div>
                <?php
                    echo GridView::widget([
                        'id' => 'grid-recommendation-agency',
                        'dataProvider'=> $dataProvider,
//                        'filterModel' => $searchModel,
//                        'showPageSummary'=>true,
                        'pjax'=>true,
                        'striped'=>true,
                        'hover'=>true,
                        'columns' => [
                            [
                                'attribute' => 'name',
                                'content' => function($data){
                                    return Html::a($data->name, Url::toRoute(['profile/show', 'shortId' => $data->user->short_id], true), [
                                        'data-pjax' => '0'
                                    ]);
                                }
                            ],
                            'admin_name',
                            'phone',
                            ['label'=>'',
                                'format' => 'raw',
                                'value'=>function ($data) {
                                    return Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/new-patient-referral-form', 'shortId' => $data->user->short_id], true), [
                                        'title' => Yii::t('app', "Send Patient Referral"),
                                        'data-pjax' => '0',
                                        'class' => 'btn btn-sm bg-orange',
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom"
                                    ]);
                                },
                                'hAlign' => 'center'
                            ],
                        ],
                        'pjaxSettings'=>[
                            'neverTimeout'=>false
                        ],
                        'toolbar' => [
                            [
                                'content'=>
                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                                        'class' => 'btn btn-default',
                                        'title' => Yii::t('app', "Reset Grid")
                                ]),
                            ],
                            '{toggleData}'
                        ],
                        'toggleDataContainer' => ['class' => 'btn-group-sm'],
                        'exportContainer' => ['class' => 'btn-group-sm']
                    ]);
                ?>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('app','Pacient Queue') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">

                    <tr>
                        <th><?= Yii::t('app','Patient Name') ?></th>
                        <th><?= Yii::t('app','Agency Name') ?></th>
                        <th><?= Yii::t('app','Date') ?></th>
                        <th><?= Yii::t('app','Action Items') ?></th>
                    </tr>
                    <?php foreach($patient_array as $param) : ?>
                        <tr>
                            <td><?= Html::a($param['patient_name'], Url::toRoute(['patient/index', 'id' => $param['patient_id']], true)) ?></td>
                            <td><?= Html::a($param['agency_name'], Url::toRoute(['profile/show', 'shortId' => $param['agency_short_id']], true)) ?></td>
                            <td><?= date('m/d/Y', $param['date']) ?></td>
                            <td><?= $param['button'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
