<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pending Patient Referrals');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pending Patient')];
$this->params['pageTitleContent'] = Yii::t('app', 'Pending Patient Referrals');

$url = Url::to(['agencylist']);
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-danger">waiting for approval</span>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Agency Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Date')?></th>
                    </tr>
                    <?php if (count($patient_sending) > 0) : ?>
                        <?php foreach($patient_sending as $param) : ?>
                            <tr>
                                <td><?= Html::a($param->patient->name, Url::toRoute(['patient/index', 'id' => $param->patient->id], true)) ?></td>
                                <td><?= Html::a($param->agency->name, Url::toRoute(['profile/show', 'shortId' => $param->agency->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($param->agency->phone)?></td>
                                <td><?= date('m/d/Y', (int)$param->date_send); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>

    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Rejected Patient Referrals</h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-primary">rejected</span>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Agency Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Date')?></th>
                        <th><?= Yii::t('app', 'Reason')?></th>
                    </tr>
                    <?php if (count($patient_reject) > 0) : ?>
                        <?php foreach($patient_reject as $param) : ?>
                            <tr>
                                <td><?= Html::a($param->patient->name, Url::toRoute(['patient/index', 'id' => $param->patient->id], true)) ?></td>
                                <td><?= Html::a($param->agency->name, Url::toRoute(['profile/show', 'shortId' => $param->agency->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($param->agency->phone)?></td>
                                <td><?= date('m/d/Y', (int)$param->date_send); ?></td>
                                <td><?= $param->comment; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<p>
    <?php //Html::a('Create Physician Recomendation Agency', ['create'], ['class' => 'btn btn-success']) ?>
</p>


