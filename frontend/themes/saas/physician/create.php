<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PhysicianRecomendationAgency */

$this->title = 'Create Physician Recomendation Agency';
$this->params['breadcrumbs'][] = ['label' => 'Physician Recomendation Agencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="physician-recomendation-agency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
