<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MyFormatter;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhysicianRecomendationAgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Discharged Patients');

$this->params['breadcrumbs'][] = ['label' => 'Discharged Patients'];
$this->params['pageTitleContent'] = Yii::t('app', 'Discharged Patients');

$url = Url::to(['agencylist']);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title ?></h3>
                <div class="box-tools">
                    <div class="input-group">
                        <span class="label label-success"><?= Yii::t('app', 'treatment completed');?></span>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th><?= Yii::t('app', 'Patient Name')?></th>
                        <th><?= Yii::t('app', 'Agency Name')?></th>
                        <th><?= Yii::t('app', 'Phone')?></th>
                        <th><?= Yii::t('app', 'Date Patient Care Ended')?></th>
                    </tr>
                    <?php if (count($patient_care_end) > 0) : ?>
                        <?php foreach($patient_care_end as $patient) : ?>
                            <tr>
                                <td><?= Html::a($patient->name, Url::toRoute(['patient/index', 'id' => $patient->id], true)) ?></td>
                                <td><?= Html::a($patient->agency->name, Url::toRoute(['profile/show', 'shortId' => $patient->agency->user->short_id], true)) ?></td>
                                <td><?= MyFormatter::phoneFormatter($patient->agency->phone)?></td>
                                <td><?= date('m/d/Y', (int)$patient->associateAgency->date_send); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5"><?= Yii::t('app', 'No result');?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

