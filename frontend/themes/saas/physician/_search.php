<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
//use frontend\models\City;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\PhysicianRecomendationAgencySearch */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::to(['city-list']);

$this->registerJs(
    '$("document").ready(function(){
        $("#new_id_agency").on("pjax:end", function() {
            $.pjax.reload({container:"#grid-recommendation-agency"});  //Reload GridView
        });
    });'
);

// The widget

// Get the initial city description
//$cityDesc = empty($model->city) ? '' : City::findOne($model->city)->description;

?>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'id' => 'new_id_agency']]); ?>

    <?= $form->field($model, 'id_agency') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

