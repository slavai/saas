<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;
use frontend\widgets\Alert;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$this->title = Yii::t('app', 'Saas');
?>
<?= Alert::widget(); ?>
<div class="login-box-body">
    <?php $form = ActiveForm::begin([
        'id'                     => 'login-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'validateOnBlur'         => false,
        'validateOnType'         => false,
        'validateOnChange'       => true,
    ]) ?>

        <p class="login-box-msg"><?= Yii::t('app', 'Sign in to start your session')?></p>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'email', [
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    'tabindex' => '1',
                    'placeholder' => $model->getAttributeLabel('email'),
                ],
            ]) ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password', [
                'inputOptions' => [
                    'class' => 'form-control',
                    'tabindex' => '2',
                    'placeholder' => $model->getAttributeLabel('password'),
                ],
            ])->passwordInput(); ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox">
                    <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
                </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']) ?>
            </div><!-- /.col -->
        </div>
    <?php ActiveForm::end(); ?>

    <p class="text-center">
<!--            --><?//= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
    </p>

    <p class="text-center">
        <?= Html::a(Yii::t('app', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
    </p>
</div>