<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password col-md-offset-3 col-md-6">
    <div class="login-logo">
        <div><?= Html::encode($this->title) ?></div>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Please choose your new password:</p>

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <div class="row">
                <div class="col-xs-4">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
</div>