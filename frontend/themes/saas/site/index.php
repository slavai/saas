<?php
use yii\bootstrap\Carousel;
/* @var $this yii\web\View */
$this->title = 'Saas';
?>
<div class="site-index">
    <?php
    /* @var $this SiteController */

    ?>
    <div class="row">
        <div class="col-md-8">
            <?php
                echo Carousel::widget([
                    'items' => [
                        // the item contains only the image
                        [
                            'content' => '<img src="https://cs7052.vk.me/c7008/v7008865/211d9/2pLlTjnJyYg.jpg"/>',
                            'options' => ['style' => 'width:100%;']
                        ],
                        // equivalent to the above
                        [
                            'content' => '<img src="https://cs7052.vk.me/c7008/v7008865/211d9/2pLlTjnJyYg.jpg"/>',
                            'options' => ['style' => 'width:100%;']
                        ],
                        // the item contains both the image and the caption
                        [
                            'content' => '<img src="https://cs7052.vk.me/c7008/v7008865/211d9/2pLlTjnJyYg.jpg"/>',
                            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
                            'options' => ['style' => 'width:100%;']
                        ],
                    ],
                    'options' => [
                        'style' => 'width:100%;',
                        'class' => 'slide'
                    ]
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $this->render('login', [
                'model'  => $model,
            ]); ?>
        </div>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                banner
            </div>
        </div>
    </div>
</div>
