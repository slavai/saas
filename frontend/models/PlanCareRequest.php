<?php

namespace app\models;

use common\models\Agencys;
use common\models\Patients;
use common\models\Physician;
use Yii;
use dektrium\user\models\Profile;
use common\models\User;

/**
 * This is the model class for table "plan_care_request".
 *
 * @property integer $id
 * @property integer $id_patient
 * @property integer $id_agency
 * @property string $type
 * @property string $medications
 * @property string $dme_supplies
 * @property string $treatments
 * @property string $discharge_plans
 * @property integer $date_send
 * @property integer $date_read_physician
 * @property integer $date_resend
 * @property integer $date_read_approved
 */
class PlanCareRequest extends \yii\db\ActiveRecord
{
    const PHYSICIAN_SEND = '0';
    const AGENCY_SEND = '1';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_care_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_patient', 'id_agency', 'medications', 'dme_supplies', 'treatments', 'discharge_plans', 'date_send'], 'required'],
            [['id_patient', 'id_agency', 'date_send', 'date_read_physician', 'date_resend', 'date_read_approved'], 'integer'],
            [['type', 'treatments', 'discharge_plans'], 'string'],
            [['medications', 'dme_supplies'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_patient' => Yii::t('app', 'Id Patient'),
            'id_agency' => Yii::t('app', 'Id Agency'),
            'type' => Yii::t('app', 'Type'),
            'medications' => Yii::t('app', 'Medications'),
            'dme_supplies' => Yii::t('app', 'Dme Supplies'),
            'treatments' => Yii::t('app', 'Treatments'),
            'discharge_plans' => Yii::t('app', 'Discharge Plans'),
            'date_send' => Yii::t('app', 'Date Send'),
            'date_read_physician' => Yii::t('app', 'Date Read Physician'),
            'date_resend' => Yii::t('app', 'Date Resend'),
            'date_read_approved' => Yii::t('app', 'Date Read Approved'),
        ];
    }

    /**
     * @inheritdoc
     * @return PlanCareRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlanCareRequestQuery(get_called_class());
    }

//    public static function getAllActiveCareRequest() {
//        $message_array = parent::find()
//            ->
//            ->where(['recepient_id' => \Yii::$app->user->id, 'status_read' => '0'])
//            ->with(['plan'])
//            ->all();
//        return $message_array;
//    }
    public function getAgency()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_agency']);
    }

    public function getPlan()
    {
        return $this->hasOne(PlanCare::className(), ['id_patient' => 'id_patient']);
    }

    public function getPhysician()
    {
        return $this->hasOne(Physician::className(), ['id' => 'id_physician'])
            ->viaTable(Patients::tableName(), ['id' => 'id_patient']);
    }

    public function getPatients()
    {
        return $this->hasOne(Patients::className(), ['id' => 'id_patient']);
    }
}
