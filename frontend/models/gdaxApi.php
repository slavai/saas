<?php


namespace app\models;


class BTCeApi
{
    protected $API_KEY;
    protected $API_SECRET;
    protected $API_PASSPHRASE;

    protected $public_api = 'https://btc-e.com/api/3/';
    protected $private_api = 'https://btc-e.com/tapi/';

    public function __construct($key, $secret, $passphrase) {
        $this->API_KEY = $key;
        $this->API_SECRET = $secret;
        $this->passphrase = $passphrase;
    }

    public function signature($request_path='', $body='', $timestamp=false, $method='GET') {
        $body = is_array($body) ? json_encode($body) : $body;
        $timestamp = $timestamp ? $timestamp : time();

        $what = $timestamp.$method.$request_path.$body;

        return base64_encode(hash_hmac("sha256", $what, base64_decode($this->secret), true));
    }
    /*******************
     *   Returns information about the user’s current balance, API-key privileges, the number of open orders and Server Time.
     *   To use this method you need a privilege of the key info.
     *
     *   Response
     *
     *   funds: Your account balance available for trading. Doesn’t include funds on your open orders.
     *   rights: The privileges of the current API key. At this time the privilege to withdraw is not used anywhere.
     *   transaction_count: Deprecated, is equal to 0.
     *   open_orders: The number of your open orders.
     *   server_time: Server time (MSK).
     ********/
    public function getInfo() {
        $url = $this->private_api;

        return self::getApiData('post', $url, 'getInfo');
    }


    /*******************
     *   The basic method that can be used for creating orders and trading on the exchange.
     *   To use this method you need an API key privilege to trade.
     *
     *   Parameters
     *
     *   pair	    pair	                                    btc_usd (example)
     *   type	    order type	                                buy or sell
     *   rate	    the rate at which you need to buy/sell	    numerical
     *   amount	    the amount you need to buy / sell	        numerical
     *
     *   Response
     *   received: The amount of currency bought/sold.
     *   remains: The remaining amount of currency to be bought/sold (and the initial order amount).
     *   order_id: Is equal to 0 if the request was fully “matched” by the opposite orders, otherwise the ID of the executed order will be returned.
     *   funds: Balance after the request.
     ********/
    public function makeOrder($type, $price, $amount, $pair = 'btc_usd') {
        $url = $this->private_api;
        $post_data = [
            'pair' => $pair,
            'type' => $type,
            'rate' => $price,
            'amount' => $amount
        ];

        return self::getApiData('post', $url, 'Trade', $post_data);
    }


    public function orderInfo($order_id) {
        $url = $this->private_api;
        $post_data = [
            'order_id' => $order_id
        ];

        return self::getApiData('post', $url, 'Trade', $post_data);
    }


    /*******************
     *   Returns trade history.
     *   To use this method you need a privilege of the info key.
     *
     *   Parameters
     *
     *   from	    trade ID, from which the display starts	    numerical	        0
     *   count	    the number of trades for display	        numerical	        1000
     *   from_id	trade ID, from which the display starts	    numerical	        0
     *   end_id	    trade ID on which the display ends	        numerical	        ∞
     *   order	    Sorting	                                    ASC or DESC	        DESC
     *   since	    the time to start the display	            UNIX time	        0
     *   end	    the time to end the display	                UNIX time	        ∞
     *   pair	    pair to be displayed	                    btc_usd (example)	all pairs
     *
     *   Response
     *   Array keys: Trade ID.
     *   pair: The pair on which the trade was executed.
     *   type: Trade type, buy/sell.
     *   amount: The amount of currency was bought/sold.
     *   rate: Sell/Buy price.
     *   order_id: Order ID.
     *   is_your_order: Is equal to 1 if order_id is your order, otherwise is equal to 0.
     *   timestamp: Trade execution time.
     ********/
    public function tradeHistory($from = null, $count = null, $from_id = null, $end_id = null, $order = null,
         $since = null, $end = null, $pair = null) {
            $url = $this->private_api;
            $post_data = [];
            if ($from) $post_data['from'] = $from;
            if ($count) $post_data['count'] = $count;
            if ($from_id) $post_data['from_id'] = $from_id;
            if ($end_id) $post_data['end_id'] = $end_id;
            if ($order) $post_data['order'] = $order;
            if ($since) $post_data['since'] = $since;
            if ($end) $post_data['end'] = $end;
            if ($pair) $post_data['pair'] = $pair;

            return self::getApiData('post', $url, 'TradeHistory', $post_data);
    }


    /*******************
     *   Returns trade history.
     *   Returns the history of transactions.
     *   To use this method you need a privilege of the info key.
     *
     *   Parameters
     *
     *   from	    transaction ID, from which the display starts	numerical	        0
     *   count	    number of transaction to be displayed           numerical	        1000
     *   from_id	transaction ID, from which the display starts	numerical	        0
     *   end_id	    transaction ID on which the display ends	    numerical	        ∞
     *   order	    Sorting	                                        ASC or DESC	        DESC
     *   since	    the time to start the display	                UNIX time	        0
     *   end	    the time to end the display	                    UNIX time	        ∞
     *
     *   Response
     *   Array keys: Transaction ID.
     *   type: Transaction type. 1/2 - deposit/withdrawal, 4/5 - credit/debit.
     *   amount: Transaction amount.
     *   currency: Transaction currency.
     *   desc: Transaction description.
     *   status: Transaction status. 0 - canceled/failed, 1 - waiting for acceptance, 2 - successful, 3 – not confirmed
     *   timestamp: Transaction time.
     ********/
    public function transHistory($from = null, $count = null, $from_id = null, $end_id = null, $order = null,
                                 $since = null, $end = null) {
        $url = $this->private_api;
        $post_data = [];
        if ($from) $post_data['from'] = $from;
        if ($count) $post_data['count'] = $count;
        if ($from_id) $post_data['from_id'] = $from_id;
        if ($end_id) $post_data['end_id'] = $end_id;
        if ($order) $post_data['order'] = $order;
        if ($since) $post_data['since'] = $since;
        if ($end) $post_data['end'] = $end;

        return self::getApiData('post', $url, 'TransHistory', $post_data);
    }

    /*******************
     *   Returns trade history.
     *   Returns the list of your active orders.
     *   To use this method you need a privilege of the info key.
     *
     *   Parameters
     *
     *   pair	    pair	btc_usd (example)	   all pairs
     *
     *   Response
     *   Array key : Order ID.
     *   pair: The pair on which the order was created.
     *   type: Order type, buy/sell.
     *   amount: The amount of currency to be bought/sold.
     *   rate: Sell/Buy price.
     *   timestamp_created: The time when the order was created.
     *   status: Deprecated, is always equal to 0.
     ********/
    public function activeOrders($pair = 'btc_usd') {
        $url = $this->private_api;
        $post_data = ['pair' => $pair];

        return self::getApiData('post', $url, 'ActiveOrders', $post_data);
    }

    /*******************
     *   This method provides the information about the last trades.
     *
     *   Additionally it accepts an optional GET-parameter limit, which indicates how many orders should be displayed (150 by default).
     *   The maximum allowable value is 2000.
     ********/
    public function getPlaceTrade($symbol = 'btc_usd', $limit = 150) {
        if ($limit > 2000) $limit = 2000;
        $url = $this->public_api . 'trades/' . $symbol;

        return self::getApiData('get', $url);
    }

    public function getApiData ($type, $url, $method = null, $post_data = []) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($type == 'post') {
            $post_data['method'] = $method;
            $post_data['nonce'] = time();

            $data = http_build_query($post_data, '', '&');

            $sign = hash_hmac("sha512", $data, $this->API_SECRET);

            $headers = [
                'Sign: '.$sign,
                'Key: '.$this->API_KEY,
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        }

        $content = curl_exec($ch);

        curl_close ($ch);
        return $content;
    }
}
