<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PatientsDiagnosis]].
 *
 * @see PatientsDiagnosis
 */
class PatientsDiagnosisQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PatientsDiagnosis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PatientsDiagnosis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}