<?php

    namespace app\models;

    class KrakenAPIException extends \ErrorException {};

	class krakenAPI {
        protected $key;     // API key
        protected $secret;  // API secret
        protected $url;     // API base URL
        protected $version; // API version
        protected $curl;    // curl handle

        /**
         * Constructor for KrakenAPI
         *
         * @param string $key API key
         * @param string $secret API secret
         * @param string $url base URL for Kraken API
         * @param string $version API version
         * @param bool $sslverify enable/disable SSL peer verification.  disable if using beta.api.kraken.com
         */
        function __construct($key, $secret, $url='https://api.kraken.com', $version='0', $sslverify=true)
        {
            $this->key = $key;
            $this->secret = $secret;
            $this->url = $url;
            $this->version = $version;
            $this->curl = curl_init();
            curl_setopt_array($this->curl, array(
                    CURLOPT_SSL_VERIFYPEER => $sslverify,
                    CURLOPT_SSL_VERIFYHOST => 2,
                    CURLOPT_USERAGENT => 'Kraken PHP API Agent',
                    CURLOPT_POST => true,
                    CURLOPT_RETURNTRANSFER => true)
            );
        }

        function __destruct()
        {
            curl_close($this->curl);
        }

        /**
         * Query public methods
         *
         * @param string $method method name
         * @param array $request request parameters
         * @return array request result on success
         * @throws KrakenAPIException
         */
        private function QueryPublic($method, array $request = array())
        {
            // build the POST data string
            $postdata = http_build_query($request, '', '&');
            // make request
            curl_setopt($this->curl, CURLOPT_URL, $this->url . '/' . $this->version . '/public/' . $method);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, array());
            $result = curl_exec($this->curl);
            if($result===false)
                throw new KrakenAPIException('CURL error: ' . curl_error($this->curl));
            // decode results
            $result = json_decode($result, true);
            if(!is_array($result))
                throw new KrakenAPIException('JSON decode error');
            return $result;
        }

        /**
         * Query private methods
         *
         * @param string $method method path
         * @param array $request request parameters
         * @return array request result on success
         * @throws KrakenAPIException
         */
        private function QueryPrivate($method, array $request = array())
        {
            if(!isset($request['nonce'])) {
                // generate a 64 bit nonce using a timestamp at microsecond resolution
                // string functions are used to avoid problems on 32 bit systems
                $nonce = explode(' ', microtime());
                $request['nonce'] = $nonce[1] . str_pad(substr($nonce[0], 2, 6), 6, '0');
            }
            // build the POST data string
            $postdata = http_build_query($request, '', '&');
            // set API key and sign the message
            $path = '/' . $this->version . '/private/' . $method;
            $sign = hash_hmac('sha512', $path . hash('sha256', $request['nonce'] . $postdata, true), base64_decode($this->secret), true);
            $headers = array(
                'API-Key: ' . $this->key,
                'API-Sign: ' . base64_encode($sign)
            );
            // make request
            curl_setopt($this->curl, CURLOPT_URL, $this->url . $path);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($this->curl);
            if($result===false)
                throw new KrakenAPIException('CURL error: ' . curl_error($this->curl));
            // decode results
            $result = json_decode($result, true);
            if(!is_array($result))
                throw new KrakenAPIException('JSON decode error');
            return $result;
        }

        /**********
         *   Get asset info
         ********/
		public function get_asset_info() {

            $res = $this->QueryPublic('Assets');
            return $res;
		}

        /**********
         *   Get order book
         *
         *   pair = asset pair to get market depth for
         *   count = maximum number of asks/bids (optional)
         ********/
        public function get_order_book() {

            $res = $this->QueryPublic('Depth', [
                'pair' => 'XBTCZUSD',
                'type' => 'sell'
            ]);
            return $res;
        }

        /**********
         *   Get order book
         *
         *   pair = comma delimited list of asset pairs to get info on
         ********/
        public function get_ticker_information($pair = 'XBTCZEUR') {

            $res = $this->QueryPublic('Ticker', [
                'pair' => $pair
            ]);
            return $res;
        }

        /**********
         *   Get recent trades
         **********/
        public function get_account_balance() {
            $res = $this->QueryPrivate('Balance');
            return $res;
        }

        /**********
         *   Get trade balance
         *
         *   aclass = asset class (optional):
         *      currency (default)
         *   asset = base asset used to determine balance (default = ZUSD)
         **********/
        public function get_trade_balance() {
            $res = $this->QueryPrivate('TradeBalance');
            return $res;
        }

        /**********
         *   Get trades history
         *
         *   type = type of trade (optional)
         *      all = all types (default)
         *      any position = any position (open or closed)
         *      closed position = positions that have been closed
         *      closing position = any trade closing all or part of a position
         *      no position = non-positional trades
         *   trades = whether or not to include trades related to position in output (optional.  default = false)
         *   start = starting unix timestamp or trade tx id of results (optional.  exclusive)
         *   end = ending unix timestamp or trade tx id of results (optional.  inclusive)
         **********/
        public function get_trades_history($type = 'all', $trades = false, $start = null, $end = null) {
            $param = [
                'type' => $type,
                'trades' => $trades
            ];
            if ($start) $param['start'] = $start;
            if ($end) $param['end'] = $end;

            $res = $this->QueryPrivate('TradesHistory', $param);
            return $res;
        }

        /**********
         *   Query trades info
         *
         *   txid = comma delimited list of transaction ids to query info about (20 maximum)
         *   trades = whether or not to include trades related to position in output (optional.  default = false)
         **********/
        public function get_trades_info($txid, $trades = false) {
            $param = [
                'txid' => $txid,
                'trades' => $trades
            ];

            $res = $this->QueryPrivate('QueryTrades', $param);
            return $res;
        }

        /**********
         *   Get open orders
         *
         *   trades = whether or not to include trades in output (optional.  default = false)
         *   userref = restrict results to given user reference id (optional)
         **********/
        public function get_open_orders($trades = true) {
            $param = [
                'trades' => $trades
            ];

            $res = $this->QueryPrivate('OpenOrders', $param);
            return $res;
        }


        /**********
         *   Get open positions
         *
         *   docalcs = whether or not to include profit/loss calculations (optional.  default = false)
         *   txid = comma delimited list of transaction ids to restrict output to
         ********/
        public function get_open_positions($txid, $docalcs = false) {
            $param = [
                'txid' => $txid,
                'docalcs' => $docalcs
            ];

            $res = $this->QueryPrivate('OpenPositions', $param);
            return $res;
        }
	}
?>