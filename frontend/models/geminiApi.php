<?php

namespace app\models;

class geminiApi
{
    protected $API_KEY = 'XUWd26XF4wtACjXeUuUV';

    protected $API_SECRET = '3FqHzTqkiXxqgTX9rUzGua2sV4h1';

    const API_URL = 'https://api.gemini.com/v1';

    public function __construct($api_key, $api_secret) {
        $this->API_KEY = $api_key;
        $this->API_SECRET = $api_secret;
    }

    /*******************
     *   This will show the available balances in the supported currencies
     *
     *   HTTP REQUEST
     *
     *   POST https://api.gemini.com/v1/balances
     *
     *   RESPONSE DETAILS
     *
     *   request	string	The literal string “/v1/balances”
     *   nonce	integer	The nonce, as described in Private API Invocation
     *
     ********/
    public function getAccountInfo() {
        $url = self::API_URL.'/balances';
        $post_data = ['request'=> '/v1/balances', 'nonce' => time()];

        return self::getApiData('post', $url, $post_data);
    }


    /*******************
     *   This will return the current order book, as two arrays, one of bids, and one of asks
     *
     *   HTTP REQUEST
     *
     *   GET https://api.gemini.com/v1/book/:symbol
     *
     ********/
    public function getOrderBook($symbol = 'btcusd') {
        $url = self::API_URL.'/book/'.$symbol;

        return self::getApiData('get', $url);
    }

    /*******************
     *  ??????
     ********/
    public function getHistoricalAccountOrders($symbol) {
        $url = self::API_URL.'/????/'.$symbol;

        return self::getApiData('get', $url);
    }

    public function getApiData ($type, $url, $post_data = null) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($type == 'post') {
            $xgemini_payload = self::getXGeminiPayload($post_data);
            $xgemini_signature = self::getXGeminiSignature($xgemini_payload);

            $header_array = ['Content-Type: text/plain', 'X-GEMINI-APIKEY: '.$this->API_KEY,
                'X-GEMINI-PAYLOAD: '.$xgemini_payload, 'X-GEMINI-SIGNATURE: '.$xgemini_signature];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }

        $content = curl_exec($ch);

        curl_close ($ch);
        return $content;
    }

    public function getXGeminiPayload ($post_data) {
        return base64_encode(json_encode($post_data));
    }

    public function getXGeminiSignature ($xgemini_payload) {
        return hash_hmac('sha384', $xgemini_payload, $this->API_SECRET);
    }
}
