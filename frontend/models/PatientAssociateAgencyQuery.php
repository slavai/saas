<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PatientAssociateAgency]].
 *
 * @see PatientAssociateAgency
 */
class PatientAssociateAgencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PatientAssociateAgency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PatientAssociateAgency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}