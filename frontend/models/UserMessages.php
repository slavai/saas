<?php

namespace app\models;

use common\models\Agencys;
use common\models\LoginData;
use common\models\Physician;
use Yii;
use common\models\User;

/**
 * This is the model class for table "user_messages".
 *
 * @property integer $id
 * @property integer $sender
 * @property integer $recipient
 * @property integer $message_id
 * @property integer $status
 * @property integer $read
 * @property string $created_at
 *
 * @property User $sender0
 * @property User $recipient0
 * @property Message $message
 */
class UserMessages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender', 'recipient', 'message_id', 'status'], 'required'],
            [['sender', 'recipient', 'message_id', 'status', 'read'], 'integer'],
            [[], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sender' => Yii::t('app', 'Sender'),
            'recipient' => Yii::t('app', 'Recipient'),
            'message_id' => Yii::t('app', 'Message ID'),
            'status' => Yii::t('app', 'Status'),
            'read' => Yii::t('app', 'Read'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::className(), ['id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(LoginData::className(), ['id' => 'sender']);
    }
}
