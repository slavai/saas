<?php

namespace app\models;

use Yii;
/**
 * Formatter is the model behind the formatter.
 */
class MyFormatter
{
    public static function phoneFormatter($phone)
    {
        return preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '($1) $2-$3', $phone);
    }

    public static function ssFormatter($phone)
    {
        return preg_replace('~.*(\d{3})[^\d]{0,7}(\d{2})[^\d]{0,7}(\d{3}).*~', '$1/$2/$3', $phone);
    }
}
