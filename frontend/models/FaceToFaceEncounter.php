<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "face_to_face_encounter".
 *
 * @property integer $id
 * @property integer $id_patient
 * @property string $date_encounter
 * @property string $medical_conditions
 * @property integer $medical_necessity
 * @property string $clinical_findings
 * @property string $homebound_status
 * @property string $date_of_drawing
 */
class FaceToFaceEncounter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'face_to_face_encounter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_encounter', 'medical_conditions', 'medical_necessity', 'clinical_findings', 'homebound_status', 'date_of_drawing'], 'required'],
            [['date_encounter', 'date_of_drawing'], 'safe'],
            [['medical_conditions', 'clinical_findings'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_patient' => Yii::t('app', 'Id Patient'),
            'id_physician' => Yii::t('app', 'Id Physician'),
            'date_encounter' => Yii::t('app', 'Date Encounter'),
            'medical_conditions' => Yii::t('app', 'Medical Conditions'),
            'medical_necessity' => Yii::t('app', 'Medical Necessity'),
            'clinical_findings' => Yii::t('app', 'Clinical Findings'),
            'homebound_status' => Yii::t('app', 'Homebound Status'),
            'date_of_drawing' => Yii::t('app', 'Date Of Drawing'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $face_date_encounter = explode('/', $this->date_encounter);
            $this->date_encounter = gmmktime(0,0,0,$face_date_encounter[0],$face_date_encounter[1],$face_date_encounter[2]);
            $face_date_of_drawing = explode('/', $this->date_of_drawing);
            $this->date_of_drawing = gmmktime(0,0,0,$face_date_of_drawing[0],$face_date_of_drawing[1],$face_date_of_drawing[2]);

            $this->medical_necessity = json_encode($this->medical_necessity);

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        $this->date_encounter = date('m/d/Y', $this->date_encounter);
        $this->date_of_drawing = date('m/d/Y', $this->date_of_drawing);
        $this->medical_necessity = json_decode($this->medical_necessity);

        parent::afterFind();
    }

    /**
     * @inheritdoc
     * @return FaceToFaceEncounterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FaceToFaceEncounterQuery(get_called_class());
    }
}
