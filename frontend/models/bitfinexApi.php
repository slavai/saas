<?php

namespace app\models;


class bitfinexApi
{
    protected $API_KEY = 'PHkEJZ0aQKkfS2kzYebclA9qGVVaqCZdCHjh5UalQ9w';

    protected $API_SECRET = 'nzt3WEDT4tuACYCwitfiPYCs7SpthaIuGrCscDTUPeL';

    const API_URL = 'https://api.bitfinex.com/v1';

    public function __construct($api_key, $api_secret) {
        $this->API_KEY = $api_key;
        $this->API_SECRET = $api_secret;
    }

    /*******************
     *   Return information about your account (trading fees).
     *
     *   RESPONSE DETAILS
     *
     *   pairs	[string]	The currency included in the pairs with this fee schedule
     *
     ********/
    public function getAccountInfo($pairs = null) {
        $url = '/account_infos';
        $post_data = ['request'=> '/v1/account_infos', 'nonce' => (string) time()];
        if ($pairs) $post_data['pairs'] = $pairs;

        return self::getApiData('post', $url, $post_data);
    }


    /*******************
     *   Get the full order book.
     *
     *   RESPONSE DETAILS
     *
     *   limit_bids	[int]	50	Limit the number of bids returned. May be 0 in which case the array of bids is empty.
     *   limit_asks	[int]	50	Limit the number of asks returned. May be 0 in which case the array of asks is empty.
     *
     ********/
    public function getOrderBook($symbol = 'BTCUSD', $limit_bids = 50, $limit_asks = 50) {
        $url = '/book/' . $symbol . '/?limit_bids=' . $limit_bids . '&limit_asks=' . $limit_asks;

        return self::getApiData('get', $url);
    }


    /*******************
     *   Get a list of the most recent trades for the given symbol.
     *
     *   RESPONSE DETAILS
     *
     *  timestamp	[time]		Only show trades at or after this timestamp.
     *  limit_trades	[int]	50	Limit the number of trades returned. Must be >= 1.
     *
     ********/
    public function getPlaceTrade($symbol = 'BTCUSD', $timestamp = null, $limit_trades = 10) {
        $url = '/trades/' . $symbol . '/?limit_trades=' . $limit_trades;

        if ($timestamp) $url .= '&timestamp=' . $timestamp;

        return self::getApiData('get', $url);
    }


    /*******************
     *   View all of your balance ledger entries.
     *
     *   RESPONSE DETAILS
     *
     *   currency	[string]	The currency to look for.
     *   since	[time]	Optional. Return only the history after this timestamp.
     *   until	[time]	Optional. Return only the history before this timestamp.
     *   limit	[int]	Optional. Limit the number of entries to return. Default is 500.
     *   wallet	[string]	Optional. Return only entries that took place in this wallet. Accepted inputs are: “trading”, “exchange”, “deposit”.
     *
     ********/
    public function getHistoricalAccountOrders($currency = 'USD', $limit = 500, $since = null, $until = null, $wallet = null) {
        $url = '/history';
        $post_data = ['request'=> '/v1/history', 'nonce' => (string) time(), 'currency' => $currency, 'limit' => $limit];
        if ($since) $post_data['since'] = $since;
        if ($until) $post_data['until'] = $until;
        if ($wallet) $post_data['wallet'] = $wallet;

        return self::getApiData('post', $url, $post_data);
    }


    /*******************
     *   View your active positions.
     ********/
    public function getOpenPositions() {
        $url = '/positions';
        $post_data = ['request'=> '/v1/positions', 'nonce' => (string) time()];

        return self::getApiData('post', $url, $post_data);
    }


    /*******************
     *  A position can be claimed if:
     *
     *  It is a long position: The amount in the last unit of the position pair that you have in your trading wallet
     *  AND/OR the realized profit of the position is greater or equal to the purchase amount of the position
     *  (base price * position amount) and the funds which need to be returned. For example, for a long BTCUSD position,
     *  you can claim the position if the amount of USD you have in the trading wallet is greater than the base price *
     *  the position amount and the funds used.
     *
     *  It is a short position: The amount in the first unit of the position pair that you have in your trading wallet is
     *  greater or equal to the amount of the position and the margin funding used.
     *
     *
     *  REQUEST DETAILS
     *
     *  position_id	[int]	The position ID given by `/positions`.
     *  amount	[decimal]	The partial amount you wish to claim
     ********/
    public function getPositionClaim($position_id, $amount = null) {
        $url = '/positions';
        $post_data = ['request'=> '/v1/positions', 'nonce' => (string) time(), 'position_id' => $position_id];
        if ($amount) $post_data['amount'] = $amount;

        return self::getApiData('post', $url, $post_data);
    }


    public function getApiData ($type, $url, $post_data = null) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::API_URL.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($type == 'post') {
            $bitfinex_payload = self::getXBfxPayload($post_data);
            $bitfinex_signature = self::getXBfxSignature($bitfinex_payload);

            $header_array = ['Content-Type: text/plain', 'X-BFX-APIKEY: '.$this->API_KEY,
                'X-BFX-PAYLOAD: '.$bitfinex_payload, 'X-BFX-SIGNATURE: '.$bitfinex_signature];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }

        $content = curl_exec($ch);

        curl_close ($ch);
        return $content;
    }

    public function getXBfxPayload ($post_data) {
        return base64_encode(json_encode($post_data));
    }

    public function getXBfxSignature ($xgemini_payload) {
        return hash_hmac('sha384', $xgemini_payload, $this->API_SECRET);
    }
}
