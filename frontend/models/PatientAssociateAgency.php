<?php

namespace app\models;

use common\models\Agencys;
use common\models\Patients;
use common\models\Physician;
use Yii;

/**
 * This is the model class for table "patient_associate_agency".
 *
 * @property integer $id
 * @property integer $id_patient
 * @property integer $id_agency
 * @property integer $status_send
 * @property integer $date_send
 * @property integer $date_accept
 * @property string $comment
 * @property integer $date_end_accept
 */
class PatientAssociateAgency extends \yii\db\ActiveRecord
{
    const STATUS_SEND = 0;
    const STATUS_ACCEPT = 1;
    const STATUS_REJECT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient_associate_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_patient', 'id_agency', 'status_send', 'date_send'], 'required'],
            [['id_patient', 'id_agency', 'status_send', 'date_send', 'date_accept', 'date_end_accept'], 'integer'],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_patient' => 'Id Patient',
            'id_agency' => 'Id Agency',
            'status_send' => 'Status Send',
            'date_send' => 'Date Send',
            'date_accept' => 'Date Accept',
            'comment' => Yii::t('app', 'Comment'),
            'date_end_accept' => Yii::t('app', 'Date End Accept'),
        ];
    }

    /**
     * @inheritdoc
     * @return PatientAssociateAgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatientAssociateAgencyQuery(get_called_class());
    }

    public function getPatient()
    {
        return $this->hasOne(Patients::className(), ['id' => 'id_patient']);
    }

    public function getAgency()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency']);
    }

    public function getPhysician()
    {
        return $this->hasOne(Physician::className(), ['id' => 'id_physician'])
            ->viaTable(Patients::tableName(), ['id' => 'id_patient']);
    }
}
