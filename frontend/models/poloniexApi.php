<?php

    namespace app\models;

	class poloniexApi {
		protected $api_key;
		protected $api_secret;
		protected $trading_url = "https://poloniex.com/tradingApi";
		protected $public_url = "https://poloniex.com/public";
		
		public function __construct($api_key, $api_secret) {
			$this->api_key = $api_key;
			$this->api_secret = $api_secret;
		}

        /**********
         *   Returns all of your available balances.
         ********/
		public function get_balances() {
			return $this->getApiData(
				[
					'command' => 'returnBalances'
				]
			);
		}

        /**********
         *   Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP".
         *   Set "currencyPair" to "all" to return open orders for all markets.
         ********/
		public function get_open_orders($pair) {		
			return $this->getApiData(
				[
					'command' => 'returnOpenOrders',
					'currencyPair' => strtoupper($pair)
				]
			);
		}

        /**********
         *   Returns your trade history for a given market, specified by the "currencyPair" POST parameter.
         *   You may specify "all" as the currencyPair to receive your trade history for all markets. You may optionally
         *   specify a range via "start" and/or "end" POST parameters, given in UNIX timestamp format; if you do not specify a range,
         *   it will be limited to one day.
         ********/
		public function get_my_trade_history($pair) {
			return $this->getApiData(
				 [
					'command' => 'returnTradeHistory',
					'currencyPair' => strtoupper($pair)
				 ]
			);
		}

        /**********
         *   Places a limit buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount".
         *   If successful, the method will return the order number.
         ********/
		public function buy($pair, $rate, $amount) {
			return $this->getApiData(
				[
					'command' => 'buy',	
					'currencyPair' => strtoupper($pair),
					'rate' => $rate,
					'amount' => $amount
				]
			);
		}

        /**********
         *   Places a sell order in a given market. Parameters and output are the same as for the buy method.
         ********/
		public function sell($pair, $rate, $amount) {
			return $this->getApiData(
				[
					'command' => 'sell',	
					'currencyPair' => strtoupper($pair),
					'rate' => $rate,
					'amount' => $amount
				]
			);
		}

        /**********
         *   Cancels an order you have placed in a given market. Required POST parameter is "orderNumber".
         ********/
		public function cancel_order($pair, $order_number) {
			return $this->getApiData(
				[
					'command' => 'cancelOrder',	
					'currencyPair' => strtoupper($pair),
					'orderNumber' => $order_number
				]
			);
		}

        /**********
         *   Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method,
         *   the withdrawal privilege must be enabled for your API key. Required POST parameters are "currency", "amount",
         *   and "address". For XMR withdrawals, you may optionally specify "paymentId".
         ********/
		public function withdraw($currency, $amount, $address) {
			return $this->getApiData(
				[
					'command' => 'withdraw',	
					'currency' => strtoupper($currency),				
					'amount' => $amount,
					'address' => $address
				]
			);
		}

        /**********
         *   If you are enrolled in the maker-taker fee schedule, returns your current trading fees and trailing 30-day
         *   volume in BTC. This information is updated once every 24 hours.
         ********/
        public function returnFeeInfo() {
            return $this->getApiData(
                [
                    'command' => 'returnFeeInfo',
                ]
            );
        }

        /**********
         *   Returns a summary of your entire margin account. This is the same information you will find in the Margin
         *   Account section of the Margin Trading page, under the Markets list.
         ********/
        public function returnMarginAccountSummary() {
            return $this->getApiData(
                [
                    'command' => 'returnMarginAccountSummary',
                ]
            );
        }



        /*******************
         *   Returns the past 200 trades for a given market, or up to 50,000 trades between a range specified in UNIX
         *   timestamps by the "start" and "end" GET parameters.
         *
         *   Call: https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NXT&start=1410158341&end=1410499372
         ********/
		public function get_trade_history($pair) {
			$trades = $this->retrieveJSON($this->public_url.'?command=returnTradeHistory&currencyPair='.strtoupper($pair));
			return $trades;
		}


        /*******************
         *   Returns the order book for a given market, as well as a sequence number for use with the Push API and an
         *   indicator specifying whether the market is frozen. You may set currencyPair to "all" to get the order books of all markets.
         *
         *   Call: https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NXT&depth=10
         ********/
		public function get_order_book($pair) {
			$orders = $this->retrieveJSON($this->public_url.'?command=returnOrderBook&currencyPair='.strtoupper($pair));
			return $orders;
		}


        /*******************
         *   Returns the 24-hour volume for all markets, plus totals for primary currencies.
         *
         *   Call: https://poloniex.com/public?command=return24hVolume
         ********/
		public function get_volume() {
			$volume = $this->retrieveJSON($this->public_url.'?command=return24hVolume');
			return $volume;
		}


        /*******************
         *   Returns the ticker for all markets.
         *
         *   Call: https://poloniex.com/public?command=returnTicker
         ********/
		public function get_ticker($pair = "ALL") {
			$pair = strtoupper($pair);
			$prices = $this->retrieveJSON($this->public_url.'?command=returnTicker');
			if($pair == "ALL"){
				return $prices;
			}else{
				$pair = strtoupper($pair);
				if(isset($prices[$pair])){
					return $prices[$pair];
				}else{
					return [];
				}
			}
		}

        /*******************
         *   Returns the ticker for all markets.
         *
         *   Call: https://poloniex.com/public?command=returnTicker
         ********/
		public function get_trading_pairs() {
			$tickers = $this->retrieveJSON($this->public_url.'?command=returnTicker');
			return array_keys($tickers);
		}
		
		public function get_total_btc_balance() {
			$balances = $this->get_balances();
			$prices = $this->get_ticker();
			
			$tot_btc = 0;
			
			foreach($balances as $coin => $amount){
				$pair = "BTC_".strtoupper($coin);
			
				// convert coin balances to btc value
				if($amount > 0){
					if($coin != "BTC"){
						$tot_btc += $amount * $prices[$pair];
					}else{
						$tot_btc += $amount;
					}
				}

				// process open orders as well
				if($coin != "BTC"){
					$open_orders = $this->get_open_orders($pair);
					foreach($open_orders as $order){
						if($order['type'] == 'buy'){
							$tot_btc += $order['total'];
						}elseif($order['type'] == 'sell'){
							$tot_btc += $order['amount'] * $prices[$pair];
						}
					}
				}
			}

			return $tot_btc;
		}


        private function getApiData(array $req = []) {
            // API settings
            $key = $this->api_key;
            $secret = $this->api_secret;

            // generate a nonce to avoid problems with 32bit systems
            $mt = explode(' ', microtime());
            $req['nonce'] = $mt[1].substr($mt[0], 2, 6);

            // generate the POST data string
            $post_data = http_build_query($req, '', '&');
            $sign = hash_hmac('sha512', $post_data, $secret);

            // generate the extra headers
            $headers = [
                'Key: '.$key,
                'Sign: '.$sign,
            ];

            // curl handle (initialize if required)
            static $ch = null;
            if (is_null($ch)) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERAGENT,
                    'Mozilla/4.0 (compatible; Poloniex PHP bot; '.php_uname('a').'; PHP/'.phpversion().')'
                );
            }
            curl_setopt($ch, CURLOPT_URL, $this->trading_url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            // run the query
            $res = curl_exec($ch);

            if ($res === false) throw new Exception('Curl error: '.curl_error($ch));
            //echo $res;
            $dec = json_decode($res, true);
            if (!$dec){
                //throw new Exception('Invalid data: '.$res);
                return false;
            }else{
                return $dec;
            }
        }

        protected function retrieveJSON($URL) {
            $opts = ['http' =>
                [
                    'method'  => 'GET',
                    'timeout' => 10
                ]
            ];
            $context = stream_context_create($opts);
            $feed = file_get_contents($URL, false, $context);
            $json = json_decode($feed, true);
            return $json;
        }
	}
?>