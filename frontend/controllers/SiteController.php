<?php
namespace frontend\controllers;

use app\models\bitfinexApi;
use app\models\bitfinexApiContact;
use app\models\BTCeApi;
use app\models\geminiApi;
use app\models\geminiApiContact;
use app\models\KrakenAPI;
use app\models\Message;
use app\models\PatientAssociateAgency;
use app\models\poloniex;
use common\models\LoginData;
use common\models\Patients;
use common\models\User;
use Yii;
//use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use app\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\LoginForm;
use common\models\AjaxValidation;

/**
 * Site controller
 */
class SiteController extends Controller
{
    use AjaxValidation;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public static function getMenuItems() {
        return [
            ['label' => Yii::t('app', 'Home' ), 'url' => ['/.']],
            ['label' => Yii::t('app', 'Why' ), 'url' => ['/site/why']],
            ['label' => Yii::t('app', 'What' ), 'url' => ['/site/what']],
            ['label' => Yii::t('app', 'How' ), 'url' => ['/site/how']],
            ['label' => Yii::t('app', 'About' ), 'url' => ['/site/about']],
            ['label' => Yii::t('app', 'Contact' ), 'url' => ['/site/contact']],
            ['label' => Yii::t('app', 'Blog' ), 'url' => ['/site/blog']],
        ];
    }

    public static function getSubMenuItems() {
        if (Yii::$app->user->identity->type == LoginData::TYPE_PHYSICIAN) {
            return [
                ['label' => 'Pending Referrals', 'url' => ['/physician/pending-referrals']],
                ['label' => 'Current Patients', 'url' => ['/physician/current-patients']],
                ['label' => 'Order Requests', 'url' => ['/physician/order-requests']],
                ['label' => 'Discharged Patients', 'url' => ['/physician/discharged-patients']]
            ];
        }
        elseif (Yii::$app->user->identity->type == LoginData::TYPE_AGENCY) {
            return [
                ['label' => 'New Patient Referrals', 'url' => ['/agencys/new-patient-referrals']],
                ['label' => 'Current Patients', 'url' => ['/agencys/current-patients']],
                ['label' => 'Order Requests', 'url' => ['/agencys/order-requests']],
                ['label' => 'Discharged Patients', 'url' => ['/agencys/discharged-patients']]
            ];
        }
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            $model = \Yii::createObject(LoginForm::className());

            $this->performAjaxValidation($model);

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $user_model = LoginData::findIdentity(\Yii::$app->user->id);
                $user_model->last_login = time();
                ++$user_model->logins;
                $user_model->save();
                return $this->goBack();
            }

            return $this->render('index', [
                'model' => $model,
            ]);
        } else {
            $role = $this->userRole;

            if ($role == 'admin') {
                Yii::$app->getSession()->destroy();
                $this->redirect('http://hpn-backend.onlinepc.com.ua');
            } elseif ($role == 'agency') {
                $this->redirect(array('/agencys'));
            } elseif ($role == 'physician') {
                $this->redirect(array('/physician'));
            } else {
                throw new BadRequestHttpException;
            }
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                \Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                \Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSendMessage() {
        Yii::$app->queue->subscribe(Yii::$app->user->getId());
        // send one message 'test'
        Yii::$app->queue->send('test');
        // receive all available messages for current user and immediately delete them from the queue
        $messages = Yii::$app->queue->receive(Yii::$app->user->getId());
    }
    /**
     * Why page
     */
    public function actionWhy()
    {
        return $this->render('why');
    }
    /**
     * What page
     */
    public function actionWhat()
    {
        return $this->render('what');
    }
    /**
     * How page
     */
    public function actionHow()
    {
        return $this->render('how');
    }
    /**
     * About page
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    /**
     * About page
     */
    public function actionBlog()
    {
        return $this->render('blog');
    }

    public function actionError()
    {
        $app = Yii::app();
        if ($error = $app->errorHandler->error->code) {
            if ($app->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error' . ($this->getViewFile('error' . $error) ? $error : ''), $error);
        }
    }




    public function actionTest()
    {
        $test = new KrakenAPI('73xU5mtqMFTDpz6Slx/Dl6L2oSR0Nmq+1TJLgmj4C4aGXU5QZTRXzoSs', 'TfeinbMHSAnp9JqYSkzQKqIhTeMneClEGWgRqIsY5LZEA4Hiuyx4F2GFHOplMsa9DMaMj4qjDlPKyY4gIgEHdA==');
        $result = $test->get_open_positions();
        var_dump($result);
    }
}
