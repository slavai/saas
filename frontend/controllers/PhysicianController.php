<?php

namespace frontend\controllers;

use app\models\OrdersRequest;
use app\models\PatientAssociateAgency;
use common\models\Patients;
use app\models\PlanCareRequest;
use Yii;
use common\models\Agencys;
use common\models\PhysicianRecomendationAgency;
use common\models\PhysicianRecomendationAgencySearch;
use common\models\AgencysSearch;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\db\Query;

/**
 * PhysicianController implements the CRUD actions for PhysicianRecomendationAgency model.
 */
class PhysicianController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search-agencys', 'view', 'agencylist', 'pending-referrals', 'current-patients', 'discharged-patients', 'order-requests'],
                        'roles' => ['physician'],
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all PhysicianRecomendationAgency models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgencysSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        if(\Yii::$app->request->isAjax && isset($_GET['AgencysSearch'])) {
            echo $this->renderPartial('_grid', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
            \Yii::$app->end();
        }

        $patient_queue = $this->getPatientQueue();

        return $this->render('index', [
            'patient_array' => $patient_queue,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    protected function getPatientQueue() {
        $patient_array = [];

        $patient = Patients::find()
            ->where(['id_physician' => \Yii::$app->user->id, 'status_care' => '0'])
            ->with(['agency', 'faceToFace', 'planOfCare'])
            ->all();

        foreach ($patient as $patient_detail) {
            if (!isset($patient_detail->faceToFace) || !isset($patient_detail->planOfCare)) {
                $data['patient_id'] = $patient_detail->id;
                $data['patient_name'] = $patient_detail->name;
                $data['agency_id'] = (isset($patient_detail->agency)) ? $patient_detail->agency->id : "";
                $data['agency_name'] = (isset($patient_detail->agency)) ? $patient_detail->agency->name : "";
                $data['agency_short_id'] = (isset($patient_detail->agency->user)) ? $patient_detail->agency->user->short_id : "";

                $data['date'] = $patient_detail->date_create + 2592000;
                $data['button'] = '';
                if (!isset($patient_detail->faceToFace)) {
                    $data['button'] .= Html::a(Yii::t('app', "Face-To-Face"), Url::toRoute(['patient/index', 'id' => $patient_detail->id, 'tab' => 'plan-of-care'], true), [
                        'title' => Yii::t('app', "Send Face-To-Face Encounter"),
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'class' => 'btn btn-danger btn-xs'
                    ]);
                    $data['button'] .= '&nbsp';
                }
                if (!isset($patient_detail->planOfCare)) {
                    $data['button'] .= Html::a(Yii::t('app', "Plan of care"), Url::toRoute(['patient/index', 'id' => $patient_detail->id, 'tab' => 'plan-of-care'], true), [
                        'title' => Yii::t('app', "Send plan of care"),
                        'data-toggle' => "tooltip",
                        'data-placement' => "bottom",
                        'class' => 'btn bg-purple btn-xs'
                    ]);
                }
                array_push($patient_array, $data);
            }
        }

        $order_patient = OrdersRequest::find()
            ->join('INNER JOIN','patients as patients','patients.id = id_patient')
            ->where('patients.id_physician = :id_physician and date_resend IS NULL and type = :type', ['id_physician' => \Yii::$app->user->id, 'type' => OrdersRequest::AGENCY_SEND])
            ->with(['patients', 'agency'])
            ->all();

        foreach ($order_patient as $order) {
            $data['patient_id'] = $order->patients->id;
            $data['patient_name'] = $order->patients->name;
            $data['agency_id'] = $order->agency->id;
            $data['agency_name'] = $order->agency->name;
            $data['agency_short_id'] = (isset($order->agency->user)) ? $order->agency->user->short_id : "";

            $data['button'] = Html::a(Yii::t('app', "Order Update"), Url::toRoute(['patient/index', 'id' => $order->patients->id, 'tab' => 'patient-orders'], true), [
                'title' => Yii::t('app', "View Order Update"),
                'data-toggle' => "tooltip",
                'data-placement' => "bottom",
                'class' => 'btn btn-xs btn-primary'
            ]);
            $data['date'] = $order->date_create;

            array_push($patient_array, $data);
        }

        $notification_plancare = PlanCareRequest::find()
            ->where('physician.id = :id and date_resend IS NULL and type = :type', ['id' => \Yii::$app->user->id, 'type' => PlanCareRequest::AGENCY_SEND])
            ->joinWith('physician')
            ->with(['patients', 'agency'])
            ->all();

        foreach ($notification_plancare as $plancare) {
            $data['patient_id'] = $plancare->patients->id;
            $data['patient_name'] = $plancare->patients->name;
            $data['agency_id'] = $plancare->agency->id;
            $data['agency_name'] = $plancare->agency->name;
            $data['agency_short_id'] = (isset($plancare->agency->user)) ? $plancare->agency->user->short_id : "";

            $data['date'] = $plancare->date_send;
            $data['button'] = Html::a('Update Plan Of Care', Url::toRoute(['patient/index', 'id' => $plancare->patients->id, 'tab' => 'plan-of-care'], true), [
                'data-placement' => "bottom",
                'class' => 'btn btn-xs bg-orange'
            ]);

            array_push($patient_array, $data);
        }
        if (count($patient_array) != 0) {
            foreach ($patient_array as $key => $arr) {
                $data_time[$key] = $arr['date'];
            }
        }
        array_multisort($data_time, SORT_DESC, SORT_NUMERIC, $patient_array);

        return $patient_array;
    }

    public function actionSearchAgencys()
    {
        $searchModel = new PhysicianRecomendationAgencySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        echo $this->renderPartial('_grid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
        \Yii::$app->end();
    }
    /**
     * Displays a single PhysicianRecomendationAgency model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PhysicianRecomendationAgency model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PhysicianRecomendationAgency();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PhysicianRecomendationAgency model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PhysicianRecomendationAgency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PhysicianRecomendationAgency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PhysicianRecomendationAgency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PhysicianRecomendationAgency::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAgencylist($q = null, $id = null) {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;

            $query->select('id, name AS text')
                ->from('agencys')
                ->where('name LIKE "%' . $q .'%"');

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionPendingReferrals()
    {
        $patient_sending = PatientAssociateAgency::find()
            ->where(['status_send' => PatientAssociateAgency::STATUS_SEND, 'patients.id_physician' => \Yii::$app->user->id])
            ->joinWith(['patient'])
            ->with(['agency'])
            ->orderBy(['date_send' => SORT_ASC])
            ->all();

        $patient_reject = PatientAssociateAgency::find()
            ->where(['status_send' => PatientAssociateAgency::STATUS_REJECT, 'patients.id_physician' => \Yii::$app->user->id])
            ->joinWith(['patient'])
            ->with(['agency'])
            ->orderBy(['date_send' => SORT_DESC])
            ->all();

        return $this->render('pending', [
            'patient_sending' => $patient_sending,
            'patient_reject' => $patient_reject
        ]);
    }

    public function actionCurrentPatients()
    {
        $patient_accept = Patients::find()
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and associate.status_send = '.PatientAssociateAgency::STATUS_ACCEPT)
            ->joinWith(['associateAgency'])
            ->where(['id_physician' => \Yii::$app->user->id])
            ->with(['agency'])
            ->orderBy(['patient_associate_agency.date_send' => SORT_ASC])
            ->all();

        $patient_queue = $this->getPatientQueue();

        return $this->render('current', [
            'patient_accept' => $patient_accept,
            'patient_array' => $patient_queue
        ]);
    }

    public function actionDischargedPatients()
    {
        $patient_care_end = Patients::find()->select('patients.*, ')
            ->join('INNER JOIN','patient_associate_agency as associate','associate.id_patient = patients.id and associate.status_send = '.PatientAssociateAgency::STATUS_ACCEPT)
            ->joinWith(['associateAgency'])
            ->where(['id_physician' => \Yii::$app->user->id, 'status_care' => Patients::STATUS_CARE_END])
            ->with(['agency'])
            ->orderBy(['patient_associate_agency.date_send' => SORT_ASC])
            ->all();

        return $this->render('discharged', [
            'patient_care_end' => $patient_care_end,
        ]);
    }

    public function actionOrderRequests()
    {
        $order_array = [];
        $order_patient = OrdersRequest::find()
            ->join('INNER JOIN','patients as patients','patients.id = id_patient')
            ->where('patients.id_physician = :id_physician and date_resend IS NULL and type = :type',
                ['id_physician' => \Yii::$app->user->id, 'type' => OrdersRequest::AGENCY_SEND])
            ->with(['patients', 'physician', 'profile', 'agency'])
            ->all();

        foreach ($order_patient as $order) {
            $data['patient_id'] = $order->patients->id;
            $data['patient_name'] = $order->patients->name;
            $data['agency_id'] = $order->agency->id;
            $data['agency_short_id'] = (isset($order->agency->user)) ? $order->agency->user->short_id : "";
            $data['agency_name'] = $order->agency->name;
            $data['agency_phone'] = $order->agency->phone;
            $data['text'] = $order->text;

            $data['button'] = "<a class='accept-patient btn btn-sm bg-olive' data-placement='bottom' title='"
                . Yii::t('app', 'Accept Order') ."' data-toggle='tooltip' href='javascript:;'
                onclick='App.Api.acceptOrderByPhysician(".$order->id.",this);'>".Yii::t('app', 'Accept')."</a>";
            $data['date'] = date('m/d/Y', (int)$order->date_create);

            array_push($order_array, $data);
        }

        $notification_plancare = PlanCareRequest::find()
            ->where('physician.id = :id and date_resend IS NULL and type = :type',
                ['id' => \Yii::$app->user->id, 'type' => PlanCareRequest::AGENCY_SEND])
            ->joinWith('physician')
            ->with(['patients', 'agency'])
            ->all();

        foreach ($notification_plancare as $plancare) {
            $data['patient_id'] = $plancare->patients->id;
            $data['patient_name'] = $plancare->patients->name;
            $data['agency_id'] = $plancare->physician->id;
            $data['agency_short_id'] = (isset($plancare->agency->user)) ? $plancare->agency->user->short_id : "";
            $data['agency_name'] = $plancare->physician->name;
            $data['agency_phone'] = $plancare->physician->phone;
            $data['text'] = "Plan Of Care POC update request";

            $data['date'] = date('m/d/Y', (int)$plancare->date_send);
            $data['button'] = Html::a('<i class="fa fa-paper-plane"></i>', Url::toRoute(['patient/index', 'id' => $plancare->patients->id, 'tab' => 'plan-of-care'], true), [
                'title' => Yii::t('app', "View Plan Of Care"),
                'data-toggle' => "tooltip",
                'data-placement' => "bottom",
                'class' => 'btn btn-sm bg-orange'
            ]);

            array_push($order_array, $data);
        }

        return $this->render('order_request', [
            'order_array' => $order_array,
        ]);
    }
}
