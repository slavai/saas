<?php

namespace frontend\controllers;

use Yii;
use app\components\Controller;
use yii\filters\VerbFilter;
use common\user_exceptions\classes\RenderedUserException;

class ErrorController extends Controller
{
    public function actionHandle() {
        // installs global error and exception handlers


//        // Message at level 'info'
//                \Rollbar::report_message('testing 123', 'info');
//
//        // Catch an exception and send it to Rollbar
//                try {
//                    throw new \Exception('test exception');
//                } catch (\Exception $e) {
//                    \Rollbar::report_exception($e);
//                }
//
//        // Will also be reported by the exception handler
//                throw new \Exception('test 2');
        $exception = \Yii::$app->errorHandler->exception;

        if($exception instanceof RenderedUserException)
        {
            return $this->render('@common/user_exceptions/views/layout', [
                'exception' => $exception
            ]);
        }

        if ($exception !== null) {
            return $this->render('error', [
                'exception' => $exception
            ]);
        }
    }
}
