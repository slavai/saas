<?php

namespace frontend\controllers;

use common\models\Agencys;
use common\models\LoginData;
use common\models\Physician;
use common\models\User;
use common\user_exceptions\classes\UserNotFoundException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\timeago\TimeAgo;
use app\components\Controller;

class ProfileController extends Controller
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['show'],
                        'roles'   => ['@']
                    ],
                ]
            ],
        ];
    }
    public function actionShow($shortId = 0)
    {
        if ($shortId === 0) $shortId = $this->user->short_id;

        $model = LoginData::findByShortId($shortId);
        if (!$model)
            throw new UserNotFoundException();

        $user_data = $model->userData;

        $role = ($model->type == LoginData::TYPE_AGENCY) ? LoginData::TYPE_AGENCY_NAME : LoginData::TYPE_PHYSICIAN_NAME;

        $userLocation = [];
        if ($user_data->address) array_push($userLocation, $user_data->address);
        if ($user_data->city) array_push($userLocation, $user_data->city);
        if ($user_data->state) array_push($userLocation, $user_data->state);
        if ($user_data->country_code) array_push($userLocation, $user_data->country->countries_name);
        return $this->render('_view_'.$role, [
            'model'=>$model,
            'user_data' => $user_data,
            'userLocation' => $userLocation
        ]);
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel($with = null)
    {
        if($this->_model===null)
        {
            if(isset($_GET['id'])) {
                if ( $with != null )
                    $this->_model = User::model()->with($with)->findbyPk($_GET['id']);
                else
                    $this->_model = User::model()->findbyPk($_GET['id']);
            }
            if($this->_model===null)
                throw new CHttpException(404,'The requested page does not exist.');
        }
        return $this->_model;
    }
}