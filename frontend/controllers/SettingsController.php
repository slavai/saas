<?php

namespace frontend\controllers;

use app\models\AjaxValidation;
use common\models\Agencys;
use common\models\Countries;
use common\models\LoginData;
use common\models\Physician;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use app\components\Controller;

class SettingsController extends Controller
{
    use AjaxValidation;

    /** @inheritdoc */
    public $defaultAction = 'profile';

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['detail', 'profile', 'account', 'confirm', 'networks', 'disconnect'],
                        'roles'   => ['@']
                    ],
                ]
            ],
        ];
    }

    /**
     * Shows profile settings form.
     * @return string|\yii\web\Response
     */
    public function actionDetail()
    {
        $model = LoginData::findIdentity(\Yii::$app->user->id);
        $detail = $model->userData;

        $data['countries'] = Countries::find()->orderBy('countries_name')->all();

        // ajax validator
        $this->performAjaxValidation($detail);

        if (\Yii::$app->request->isPost) {
            $oldAvatar = $detail->avatar_filename;
            $post = \Yii::$app->request->post();
            $detail->load($post);

            if (!ActiveForm::validate($detail)) {
                if (! empty($_FILES[$detail->shortClassName()]['name']['avatar_filename']))
                    $detail->avatar_filename = $detail->setUserAvatar($_FILES[$detail->shortClassName()]);
                else
                    $detail->avatar_filename = $oldAvatar;

                if ($detail->save())
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Your profile has been updated'));
            } else {
                echo json_encode(ActiveForm::validate($detail));
                \Yii::$app->end();
            }
        }

        return $this->render('edit', [
            'model'=>$model,
            'detail'=>$detail,
            'role'=>$this->userRole,
            'data' => $data
        ]);
    }

    public function actionAccount()
    {
        $model = LoginData::findIdentity(\Yii::$app->user->id);
        $detail = $model->userData;

        $model->setScenario(LoginData::SCENARIO_STEP_1);

        // ajax validator
        $this->performAjaxValidation($model);

        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $model->load($post);

            if (!ActiveForm::validate($model)) {
                $model->setPassword($post['passwordNew']);

                if ($model->save(false)) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Password was successfully changed'));
                    return $this->refresh();
                }
            }
            else {
                echo json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }

        return $this->render('account', [
            'model' => $model,
            'detail' => $detail,
            'role'=>$this->userRole
        ]);
    }
}
