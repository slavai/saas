<div class="filterContainer m-sm-bottom">
    <div class="dropdown nowrap upCategories btn-group m-sm-bottom m-sm-right facet-list"
         facets="facets" data-log-sublocation="facets_category" dropdown="" facetname="c" facet-list="">
        <div data-toggle="dropdown" class="btn btn-primary dropdown-toggle ellipsis" aria-haspopup="true" aria-expanded="false">
                    <span o-log-remove="facets_category" class="glyphicon-sm o-icon-x m-0-right"
                          data-ng-click="resetFacet(); $event.stopPropagation();" data-ng-mouseleave="hovered = false"
                          data-ng-mouseenter="hovered = true"
                          data-ng-class="{'hidden': !isActive(), 'text-danger': hovered}"></span>
            <strong class="m-right-10 binding">Any category</strong>
            <span class="glyphicon glyphicon-chevron-down p-sm-left"></span>
        </div>
        <ul class="dropdown-menu">
            <li class="hovered">
                <a o-log-click="facets_category" href="/o/profiles/browse/" data-facet-html="Any category"
                   data-facet-value="" class="default">
                    Any category
                </a></li>
            <li data-submenu-id="facetCategory1"><a o-log-click="facets_category"
                                                    href="/o/profiles/browse/c/web-mobile-software-dev/"
                                                    data-facet-html="Web, Mobile &amp; Software Dev"
                                                    data-facet-value="web-mobile-software-dev">Web,
                    Mobile &amp; Software Dev</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory1"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/web-mobile-software-dev/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['web-mobile-software-dev'] ? (facetCounters.category2['web-mobile-software-dev'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted"> (462,706)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/desktop-software-development/"
                           data-facet-html="Desktop Software Development" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="desktop-software-development">Desktop
                        Software Development<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['desktop-software-development'] ? (facetCounters.subcategory2['desktop-software-development'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted"> (132,824)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/ecommerce-development/"
                           data-facet-html="Ecommerce Development" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="ecommerce-development">Ecommerce
                        Development<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['ecommerce-development'] ? (facetCounters.subcategory2['ecommerce-development'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted"> (116,910)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/game-development/"
                           data-facet-html="Game Development" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="game-development">Game Development
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['game-development'] ? (facetCounters.subcategory2['game-development'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted"> (37,348)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/mobile-development/"
                           data-facet-html="Mobile Development" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="mobile-development">Mobile Development
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['mobile-development'] ? (facetCounters.subcategory2['mobile-development'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (99,692)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/product-management/"
                           data-facet-html="Product Management" o-log-click="facets_subcategory"
                           class="list-group-item active" data-facet-value="product-management">Product
                        Management<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['product-management'] ? (facetCounters.subcategory2['product-management'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (116,565)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/qa-testing/"
                           data-facet-html="QA &amp; Testing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="qa-testing">QA &amp; Testing
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['qa-testing'] ? (facetCounters.subcategory2['qa-testing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (92,781)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/scripts-utilities/"
                           data-facet-html="Scripts &amp; Utilities" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="scripts-utilities">Scripts &amp; Utilities
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['scripts-utilities'] ? (facetCounters.subcategory2['scripts-utilities'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (77,319)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/web-development/"
                           data-facet-html="Web Development" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="web-development">Web Development
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['web-development'] ? (facetCounters.subcategory2['web-development'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (252,148)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/web-mobile-design/"
                           data-facet-html="Web &amp; Mobile Design" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="web-mobile-design">Web &amp; Mobile Design
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['web-mobile-design'] ? (facetCounters.subcategory2['web-mobile-design'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (270,232)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/web-mobile-software-dev/sc/other-software-development/"
                           data-facet-html="Other - Software Development" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-software-development">Other -
                        Software Development<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-software-development'] ? (facetCounters.subcategory2['other-software-development'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (180,803)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory2" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/it-networking/"
                                                                     data-facet-html="IT &amp; Networking"
                                                                     data-facet-value="it-networking">IT &amp;
                    Networking</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory2"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/it-networking/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['it-networking'] ? (facetCounters.category2['it-networking'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (137,845)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/it-networking/sc/database-administration/"
                           data-facet-html="Database Administration" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="database-administration">Database
                        Administration<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['database-administration'] ? (facetCounters.subcategory2['database-administration'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (45,660)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/it-networking/sc/erp-crm-software/"
                           data-facet-html="ERP / CRM Software" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="erp-crm-software">ERP / CRM Software
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['erp-crm-software'] ? (facetCounters.subcategory2['erp-crm-software'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (22,188)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/it-networking/sc/information-security/"
                           data-facet-html="Information Security" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="information-security">Information Security
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['information-security'] ? (facetCounters.subcategory2['information-security'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (2,650)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/it-networking/sc/network-system-administration/"
                           data-facet-html="Network &amp; System Administration"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="network-system-administration">Network &amp; System Administration
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['network-system-administration'] ? (facetCounters.subcategory2['network-system-administration'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (43,185)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/it-networking/sc/other-it-networking/"
                           data-facet-html="Other - IT &amp; Networking" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-it-networking">Other - IT &amp;
                        Networking<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-it-networking'] ? (facetCounters.subcategory2['other-it-networking'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (34,946)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory3" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/data-science-analytics/"
                                                                     data-facet-html="Data Science &amp; Analytics"
                                                                     data-facet-value="data-science-analytics">Data
                    Science &amp; Analytics</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory3"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/data-science-analytics/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['data-science-analytics'] ? (facetCounters.category2['data-science-analytics'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (41,744)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/a-b-testing/"
                           data-facet-html="A/B Testing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="a-b-testing">A/B Testing
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['a-b-testing'] ? (facetCounters.subcategory2['a-b-testing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (705)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/data-visualization/"
                           data-facet-html="Data Visualization" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="data-visualization">Data Visualization
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['data-visualization'] ? (facetCounters.subcategory2['data-visualization'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (1,512)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/data-extraction-etl/"
                           data-facet-html="Data Extraction / ETL" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="data-extraction-etl">Data Extraction / ETL
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['data-extraction-etl'] ? (facetCounters.subcategory2['data-extraction-etl'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (1,901)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/data-mining-management/"
                           data-facet-html="Data Mining &amp; Management" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="data-mining-management">Data Mining &amp;
                        Management<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['data-mining-management'] ? (facetCounters.subcategory2['data-mining-management'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (2,374)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/machine-learning/"
                           data-facet-html="Machine Learning" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="machine-learning">Machine Learning
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['machine-learning'] ? (facetCounters.subcategory2['machine-learning'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (939)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/quantitative-analysis/"
                           data-facet-html="Quantitative Analysis" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="quantitative-analysis">Quantitative
                        Analysis<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['quantitative-analysis'] ? (facetCounters.subcategory2['quantitative-analysis'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (7,799)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/data-science-analytics/sc/other-data-science-analytics/"
                           data-facet-html="Other - Data Science &amp; Analytics"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="other-data-science-analytics">Other - Data Science &amp; Analytics
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-data-science-analytics'] ? (facetCounters.subcategory2['other-data-science-analytics'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (1,704)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory4" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/engineering-architecture/"
                                                                     data-facet-html="Engineering &amp; Architecture"
                                                                     data-facet-value="engineering-architecture">Engineering
                    &amp; Architecture</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory4"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/engineering-architecture/"
                            data-facet-html="All subcategories" data-facet-value="" o-log-click="facets_subcategory"
                            class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['engineering-architecture'] ? (facetCounters.category2['engineering-architecture'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (83,111)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/3d-modeling-cad/"
                           data-facet-html="3D Modeling &amp; CAD" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="3d-modeling-cad">3D Modeling &amp; CAD
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['3d-modeling-cad'] ? (facetCounters.subcategory2['3d-modeling-cad'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (16,022)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/architecture/"
                           data-facet-html="Architecture" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="architecture">Architecture
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['architecture'] ? (facetCounters.subcategory2['architecture'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (460)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/chemical-engineering/"
                           data-facet-html="Chemical Engineering" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="chemical-engineering">Chemical Engineering
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['chemical-engineering'] ? (facetCounters.subcategory2['chemical-engineering'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (58)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/civil-structural-engineering/"
                           data-facet-html="Civil &amp; Structural Engineering" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="civil-structural-engineering">Civil &amp;
                        Structural Engineering<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['civil-structural-engineering'] ? (facetCounters.subcategory2['civil-structural-engineering'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (196)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/contract-manufacturing/"
                           data-facet-html="Contract Manufacturing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="contract-manufacturing">Contract
                        Manufacturing<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['contract-manufacturing'] ? (facetCounters.subcategory2['contract-manufacturing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (71)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/electrical-engineering/"
                           data-facet-html="Electrical Engineering" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="electrical-engineering">Electrical
                        Engineering<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['electrical-engineering'] ? (facetCounters.subcategory2['electrical-engineering'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (793)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/interior-design/"
                           data-facet-html="Interior Design" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="interior-design">Interior Design
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['interior-design'] ? (facetCounters.subcategory2['interior-design'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (399)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/mechanical-engineering/"
                           data-facet-html="Mechanical Engineering" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="mechanical-engineering">Mechanical
                        Engineering<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['mechanical-engineering'] ? (facetCounters.subcategory2['mechanical-engineering'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (315)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/engineering-architecture/sc/product-design/"
                           data-facet-html="Product Design" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="product-design">Product Design
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['product-design'] ? (facetCounters.subcategory2['product-design'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (13,819)</span><!-- end ngIf: 1 -->
                    </a><a
                            href="/o/profiles/browse/c/engineering-architecture/sc/other-engineering-architecture/"
                            data-facet-html="Other - Engineering &amp; Architecture"
                            o-log-click="facets_subcategory" class="list-group-item"
                            data-facet-value="other-engineering-architecture">Other - Engineering &amp; Architecture
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-engineering-architecture'] ? (facetCounters.subcategory2['other-engineering-architecture'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (559)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory5" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/design-creative/"
                                                                     data-facet-html="Design &amp; Creative"
                                                                     data-facet-value="design-creative">Design
                    &amp; Creative</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory5"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/design-creative/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['design-creative'] ? (facetCounters.category2['design-creative'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (343,547)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/animation/" data-facet-html="Animation"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="animation">Animation<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['animation'] ? (facetCounters.subcategory2['animation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (18,124)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/audio-production/"
                           data-facet-html="Audio Production" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="audio-production">Audio Production
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['audio-production'] ? (facetCounters.subcategory2['audio-production'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (8,535)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/graphic-design/"
                           data-facet-html="Graphic Design" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="graphic-design">Graphic Design
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['graphic-design'] ? (facetCounters.subcategory2['graphic-design'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (102,003)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/illustration/"
                           data-facet-html="Illustration" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="illustration">Illustration
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['illustration'] ? (facetCounters.subcategory2['illustration'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (38,037)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/logo-design-branding/"
                           data-facet-html="Logo Design &amp; Branding" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="logo-design-branding">Logo Design &amp;
                        Branding<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['logo-design-branding'] ? (facetCounters.subcategory2['logo-design-branding'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (89,908)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/photography/"
                           data-facet-html="Photography" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="photography">Photography
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['photography'] ? (facetCounters.subcategory2['photography'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (3,125)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/presentations/"
                           data-facet-html="Presentations" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="presentations">Presentations
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['presentations'] ? (facetCounters.subcategory2['presentations'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (30,400)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/video-production/"
                           data-facet-html="Video Production" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="video-production">Video Production
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['video-production'] ? (facetCounters.subcategory2['video-production'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (17,090)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/voice-talent/"
                           data-facet-html="Voice Talent" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="voice-talent">Voice Talent
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['voice-talent'] ? (facetCounters.subcategory2['voice-talent'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (6,521)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/design-creative/sc/other-design-creative/"
                           data-facet-html="Other - Design &amp; Creative" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-design-creative">Other - Design &amp;
                        Creative<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-design-creative'] ? (facetCounters.subcategory2['other-design-creative'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (36,220)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory6" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/writing/"
                                                                     data-facet-html="Writing"
                                                                     data-facet-value="writing">Writing</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory6"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/writing/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['writing'] ? (facetCounters.category2['writing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (455,593)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/academic-writing-research/"
                           data-facet-html="Academic Writing &amp; Research" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="academic-writing-research">Academic Writing
                        &amp; Research<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['academic-writing-research'] ? (facetCounters.subcategory2['academic-writing-research'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (1,215)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/article-blog-writing/"
                           data-facet-html="Article &amp; Blog Writing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="article-blog-writing">Article &amp; Blog
                        Writing<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['article-blog-writing'] ? (facetCounters.subcategory2['article-blog-writing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (50,987)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/copywriting/" data-facet-html="Copywriting"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="copywriting">Copywriting<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['copywriting'] ? (facetCounters.subcategory2['copywriting'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (30,020)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/creative-writing/"
                           data-facet-html="Creative Writing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="creative-writing">Creative Writing
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['creative-writing'] ? (facetCounters.subcategory2['creative-writing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (28,211)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/editing-proofreading/"
                           data-facet-html="Editing &amp; Proofreading" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="editing-proofreading">Editing &amp;
                        Proofreading<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['editing-proofreading'] ? (facetCounters.subcategory2['editing-proofreading'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (2,026)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/grant-writing/" data-facet-html="Grant Writing"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="grant-writing">Grant Writing<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['grant-writing'] ? (facetCounters.subcategory2['grant-writing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (85)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/resumes-cover-letters/"
                           data-facet-html="Resumes &amp; Cover Letters" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="resumes-cover-letters">Resumes &amp; Cover
                        Letters<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['resumes-cover-letters'] ? (facetCounters.subcategory2['resumes-cover-letters'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (886)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/technical-writing/"
                           data-facet-html="Technical Writing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="technical-writing">Technical Writing
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['technical-writing'] ? (facetCounters.subcategory2['technical-writing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (39,287)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/web-content/" data-facet-html="Web Content"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="web-content">Web Content<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['web-content'] ? (facetCounters.subcategory2['web-content'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (55,767)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/writing/sc/other-writing/"
                           data-facet-html="Other - Writing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-writing">Other - Writing
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-writing'] ? (facetCounters.subcategory2['other-writing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (22,202)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory7" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/translation/"
                                                                     data-facet-html="Translation"
                                                                     data-facet-value="translation">Translation</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory7"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/translation/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['translation'] ? (facetCounters.category2['translation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (185,052)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/translation/sc/general-translation/"
                           data-facet-html="General Translation" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="general-translation">General Translation
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['general-translation'] ? (facetCounters.subcategory2['general-translation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (32,712)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/translation/sc/legal-translation/"
                           data-facet-html="Legal Translation" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="legal-translation">Legal Translation
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['legal-translation'] ? (facetCounters.subcategory2['legal-translation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (209)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/translation/sc/medical-translation/"
                           data-facet-html="Medical Translation" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="medical-translation">Medical Translation
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['medical-translation'] ? (facetCounters.subcategory2['medical-translation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (281)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/translation/sc/technical-translation/"
                           data-facet-html="Technical Translation" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="technical-translation">Technical
                        Translation<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['technical-translation'] ? (facetCounters.subcategory2['technical-translation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (1,009)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory8" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/legal/"
                                                                     data-facet-html="Legal"
                                                                     data-facet-value="legal">Legal</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory8"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/legal/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['legal'] ? (facetCounters.category2['legal'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (12,513)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/contract-law/" data-facet-html="Contract Law"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="contract-law">Contract Law<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['contract-law'] ? (facetCounters.subcategory2['contract-law'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (53)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/corporate-law/" data-facet-html="Corporate Law"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="corporate-law">Corporate Law<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['corporate-law'] ? (facetCounters.subcategory2['corporate-law'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (32)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/criminal-law/" data-facet-html="Criminal Law"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="criminal-law">Criminal Law<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['criminal-law'] ? (facetCounters.subcategory2['criminal-law'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (24)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/family-law/" data-facet-html="Family Law"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="family-law">Family Law<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['family-law'] ? (facetCounters.subcategory2['family-law'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (33)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/intellectual-property-law/"
                           data-facet-html="Intellectual Property Law" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="intellectual-property-law">Intellectual
                        Property Law<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['intellectual-property-law'] ? (facetCounters.subcategory2['intellectual-property-law'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (38)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/paralegal-services/"
                           data-facet-html="Paralegal Services" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="paralegal-services">Paralegal Services
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['paralegal-services'] ? (facetCounters.subcategory2['paralegal-services'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (50)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/legal/sc/other-legal/" data-facet-html="Other - Legal"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="other-legal">Other - Legal<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-legal'] ? (facetCounters.subcategory2['other-legal'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (1,052)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory9" class="ng-scope"><a o-log-click="facets_category"
                                                                     href="/o/profiles/browse/c/admin-support/"
                                                                     data-facet-html="Admin Support"
                                                                     data-facet-value="admin-support">Admin
                    Support</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group" id="facetCategory9"
                     data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/admin-support/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['admin-support'] ? (facetCounters.category2['admin-support'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (531,581)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/admin-support/sc/data-entry/" data-facet-html="Data Entry"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="data-entry">Data Entry<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['data-entry'] ? (facetCounters.subcategory2['data-entry'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (100,121)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/admin-support/sc/personal-virtual-assistant/"
                           data-facet-html="Personal / Virtual Assistant" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="personal-virtual-assistant">Personal /
                        Virtual Assistant<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['personal-virtual-assistant'] ? (facetCounters.subcategory2['personal-virtual-assistant'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (34,257)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/admin-support/sc/project-management/"
                           data-facet-html="Project Management" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="project-management">Project Management
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['project-management'] ? (facetCounters.subcategory2['project-management'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (21,952)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/admin-support/sc/transcription/"
                           data-facet-html="Transcription" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="transcription">Transcription
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['transcription'] ? (facetCounters.subcategory2['transcription'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (11,094)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/admin-support/sc/web-research/"
                           data-facet-html="Web Research" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="web-research">Web Research
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['web-research'] ? (facetCounters.subcategory2['web-research'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (59,252)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/admin-support/sc/other-admin-support/"
                           data-facet-html="Other - Admin Support" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-admin-support">Other - Admin Support
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-admin-support'] ? (facetCounters.subcategory2['other-admin-support'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (44,505)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory10" class="ng-scope"><a o-log-click="facets_category"
                                                                      href="/o/profiles/browse/c/customer-service/"
                                                                      data-facet-html="Customer Service"
                                                                      data-facet-value="customer-service">Customer
                    Service</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group"
                     id="facetCategory10" data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/customer-service/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['customer-service'] ? (facetCounters.category2['customer-service'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (265,503)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/customer-service/sc/customer-service/"
                           data-facet-html="Customer Service" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="customer-service">Customer Service
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['customer-service'] ? (facetCounters.subcategory2['customer-service'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (34,576)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/customer-service/sc/technical-support/"
                           data-facet-html="Technical Support" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="technical-support">Technical Support
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['technical-support'] ? (facetCounters.subcategory2['technical-support'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (36,911)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/customer-service/sc/other-customer-service/"
                           data-facet-html="Other - Customer Service" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-customer-service">Other - Customer
                        Service<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-customer-service'] ? (facetCounters.subcategory2['other-customer-service'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (24,750)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory11" class="ng-scope"><a o-log-click="facets_category"
                                                                      href="/o/profiles/browse/c/sales-marketing/"
                                                                      data-facet-html="Sales &amp; Marketing"
                                                                      data-facet-value="sales-marketing">Sales
                    &amp; Marketing</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group"
                     id="facetCategory11" data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/sales-marketing/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['sales-marketing'] ? (facetCounters.category2['sales-marketing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (302,392)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/display-advertising/"
                           data-facet-html="Display Advertising" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="display-advertising">Display Advertising
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['display-advertising'] ? (facetCounters.subcategory2['display-advertising'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (24,827)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/email-marketing-automation/"
                           data-facet-html="Email &amp; Marketing Automation" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="email-marketing-automation">Email &amp;
                        Marketing Automation<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['email-marketing-automation'] ? (facetCounters.subcategory2['email-marketing-automation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (26,276)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/lead-generation/"
                           data-facet-html="Lead Generation" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="lead-generation">Lead Generation
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['lead-generation'] ? (facetCounters.subcategory2['lead-generation'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (8,615)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/market-customer-research/"
                           data-facet-html="Market &amp; Customer Research" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="market-customer-research">Market &amp;
                        Customer Research<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['market-customer-research'] ? (facetCounters.subcategory2['market-customer-research'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (10,367)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/marketing-strategy/"
                           data-facet-html="Marketing Strategy" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="marketing-strategy">Marketing Strategy
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['marketing-strategy'] ? (facetCounters.subcategory2['marketing-strategy'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (11,782)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/public-relations/"
                           data-facet-html="Public Relations" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="public-relations">Public Relations
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['public-relations'] ? (facetCounters.subcategory2['public-relations'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (5,213)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/sem-search-engine-marketing/"
                           data-facet-html="SEM - Search Engine Marketing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="sem-search-engine-marketing">SEM - Search
                        Engine Marketing<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['sem-search-engine-marketing'] ? (facetCounters.subcategory2['sem-search-engine-marketing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (19,070)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/seo-search-engine-optimization/"
                           data-facet-html="SEO - Search Engine Optimization" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="seo-search-engine-optimization">SEO -
                        Search Engine Optimization<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['seo-search-engine-optimization'] ? (facetCounters.subcategory2['seo-search-engine-optimization'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (50,774)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/smm-social-media-marketing/"
                           data-facet-html="SMM - Social Media Marketing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="smm-social-media-marketing">SMM - Social
                        Media Marketing<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['smm-social-media-marketing'] ? (facetCounters.subcategory2['smm-social-media-marketing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (22,550)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/telemarketing-telesales/"
                           data-facet-html="Telemarketing &amp; Telesales" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="telemarketing-telesales">Telemarketing
                        &amp; Telesales<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['telemarketing-telesales'] ? (facetCounters.subcategory2['telemarketing-telesales'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (4,202)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/sales-marketing/sc/other-sales-marketing/"
                           data-facet-html="Other - Sales &amp; Marketing" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="other-sales-marketing">Other - Sales &amp;
                        Marketing<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-sales-marketing'] ? (facetCounters.subcategory2['other-sales-marketing'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (8,683)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
            <li data-submenu-id="facetCategory12" class="ng-scope"><a o-log-click="facets_category"
                                                                      href="/o/profiles/browse/c/accounting-consulting/"
                                                                      data-facet-html="Accounting &amp; Consulting"
                                                                      data-facet-value="accounting-consulting">Accounting
                    &amp; Consulting</a>

                <div ng-click="$event.stopPropagation();" class="popover submenu list-group"
                     id="facetCategory12" data-facet-name="sc" style="display: none; top: 0px;"><a
                            href="/o/profiles/browse/c/accounting-consulting/" data-facet-html="All subcategories"
                            data-facet-value="" o-log-click="facets_subcategory" class="default list-group-item">
                        All subcategories
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.category2['accounting-consulting'] ? (facetCounters.category2['accounting-consulting'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (163,059)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/accounting-consulting/sc/accounting/"
                           data-facet-html="Accounting" o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="accounting">Accounting<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['accounting'] ? (facetCounters.subcategory2['accounting'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (11,573)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/accounting-consulting/sc/financial-planning/"
                           data-facet-html="Financial Planning" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="financial-planning">Financial Planning
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['financial-planning'] ? (facetCounters.subcategory2['financial-planning'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (4,008)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/accounting-consulting/sc/human-resources/"
                           data-facet-html="Human Resources" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="human-resources">Human Resources
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['human-resources'] ? (facetCounters.subcategory2['human-resources'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (9,445)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/accounting-consulting/sc/management-consulting/"
                           data-facet-html="Management Consulting" o-log-click="facets_subcategory"
                           class="list-group-item" data-facet-value="management-consulting">Management
                        Consulting<!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['management-consulting'] ? (facetCounters.subcategory2['management-consulting'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (10,802)</span><!-- end ngIf: 1 -->
                    </a><a href="/o/profiles/browse/c/accounting-consulting/sc/other-accounting-consulting/"
                           data-facet-html="Other - Accounting &amp; Consulting"
                           o-log-click="facets_subcategory" class="list-group-item"
                           data-facet-value="other-accounting-consulting">Other - Accounting &amp; Consulting
                        <!-- ngIf: 1 --><span
                                ng-bind-html="' (' + (facetCounters.subcategory2['other-accounting-consulting'] ? (facetCounters.subcategory2['other-accounting-consulting'] | number:0) : '0') + ')'"
                                ng-if="1" class="text-muted "> (9,475)</span><!-- end ngIf: 1 -->
                    </a></div>
            </li>
        </ul>
    </div>
    <div class="dropdown btn-group m-sm-bottom m-sm-right facet-list" facets="facets"
         dropdown="" facetname="fb" facet-list="">
        <div data-toggle="dropdown" dropdown-toggle="" ng-class="{disabled: toggleDisabled}"
             class="btn btn-primary dropdown-toggle ellipsis" aria-haspopup="true" aria-expanded="false"><span
                    o-log-remove="facets_feedback" class="glyphicon-sm o-icon-x m-0-right hidden"
                    data-ng-click="resetFacet(); $event.stopPropagation();" data-ng-mouseleave="hovered = false"
                    data-ng-mouseenter="hovered = true"
                    data-ng-class="{'hidden': !isActive(), 'text-danger': hovered}"></span><strong
                    class="m-sm-right ng-binding" ng-bind-html="dropdownText | unsafe">Any feedback</strong><span
                    class="glyphicon glyphicon-chevron-down p-sm-left"></span></div>
        <ul ng-transclude="" ng-click="$event.stopPropagation();" role="menu" class="dropdown-menu">
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/"
                   data-facet-html="Any feedback" data-facet-value=""
                   o-log-click="facets_feedback" class="default">
                    Any feedback
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks[0] ? (facetCounters.feedbacks[0] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (116,565)</span><!-- end ngIf: 1 -->
                </a></li>
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/fb/45/"
                   data-facet-html="4.5 &amp; up stars" o-log-click="facets_feedback"
                   data-facet-value="45">
                    4.5 &amp; up stars
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks[4.5] ? (facetCounters.feedbacks[4.5] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (28,280)</span><!-- end ngIf: 1 -->
                </a></li>
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/fb/40/"
                   data-facet-html="4 &amp; up stars" o-log-click="facets_feedback"
                   data-facet-value="40">
                    4 &amp; up stars
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks[4] ? (facetCounters.feedbacks[4] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (31,583)</span><!-- end ngIf: 1 -->
                </a></li>
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/fb/30/"
                   data-facet-html="3 &amp; up stars" o-log-click="facets_feedback"
                   data-facet-value="30">
                    3 &amp; up stars
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks[3] ? (facetCounters.feedbacks[3] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (33,733)</span><!-- end ngIf: 1 -->
                </a></li>
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/fb/20/"
                   data-facet-html="2 &amp; up stars" o-log-click="facets_feedback"
                   data-facet-value="20">
                    2 &amp; up stars
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks[2] ? (facetCounters.feedbacks[2] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (34,500)</span><!-- end ngIf: 1 -->
                </a></li>
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/fb/10/"
                   data-facet-html="1 &amp; up stars" o-log-click="facets_feedback"
                   data-facet-value="10">
                    1 &amp; up stars
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks[1] ? (facetCounters.feedbacks[1] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (35,168)</span><!-- end ngIf: 1 -->
                </a></li>
            <li><a href="/o/profiles/browse/c/design-creative/sc/animation/fb/0/"
                   data-facet-html="No feedback yet" o-log-click="facets_feedback"
                   data-facet-value="0">
                    No feedback yet
                    <!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.feedbacks['no'] ? (facetCounters.feedbacks['no'] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (81,397)</span><!-- end ngIf: 1 -->
                </a></li>
        </ul>
    </div>
    <div class="dropdown btn-group m-sm-bottom m-sm-right" facets="facets"
         data-log-sublocation="facets_experience" dropdown="" facetname="exp" facet-list="">
        <div data-toggle="dropdown" dropdown-toggle="" ng-class="{disabled: toggleDisabled}"
             class="btn btn-primary dropdown-toggle ellipsis" aria-haspopup="true" aria-expanded="false"><span
                    o-log-remove="facets_experience" class="glyphicon-sm o-icon-x m-0-right hidden"
                    data-ng-click="resetFacet(); $event.stopPropagation();" data-ng-mouseleave="hovered = false"
                    data-ng-mouseenter="hovered = true"
                    data-ng-class="{'hidden': !isActive(), 'text-danger': hovered}"></span><strong
                    class="m-sm-right ng-binding" ng-bind-html="dropdownText | unsafe">Any experience</strong><span
                    class="glyphicon glyphicon-chevron-down p-sm-left"></span></div>
        <ul ng-transclude="" ng-click="$event.stopPropagation();" role="menu" class="dropdown-menu">
            <li><a href="?exp=1"
                   rel="nofollow" data-facet-html="Entry experience" data-facet-value="1"
                   o-log-click="facets_experience"><strong
                            class="pull-right">$</strong><strong>Entry</strong><!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.experience[1] ? (facetCounters.experience[1] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (<%=experience.entry%>)</span><!-- end ngIf: 1 -->
                    <div class="small text-muted m-sm-top">Looking for freelancers with the lowest rate</div>
                </a></li>
            <li><a href="?exp=2"
                   rel="nofollow" data-facet-html="Intermediate experience"
                   data-facet-value="2" o-log-click="facets_experience"><strong
                            class="pull-right">$$</strong><strong>Intermediate</strong><!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.experience[2] ? (facetCounters.experience[2] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (<%=experience.intermediate%>)</span><!-- end ngIf: 1 -->
                    <div class="small text-muted m-sm-top">Looking for a mix of experience and value</div>
                </a></li>
            <li><a href="?exp=3"
                   rel="nofollow" data-facet-html="Expert experience" data-facet-value="3"
                   o-log-click="facets_experience"><strong
                            class="pull-right">$$$</strong><strong>Expert</strong><!-- ngIf: 1 --><span
                            ng-bind-html="' (' + (facetCounters.experience[3] ? (facetCounters.experience[3] | number:0) : '0') + ')'"
                            ng-if="1" class="text-muted "> (<%=experience.expert%>)</span><!-- end ngIf: 1 -->
                    <div class="small text-muted m-sm-top">Willing to pay higher rates for most experienced
                        freelancers
                    </div>
                </a></li>
        </ul>
    </div>
    <span class="advanceFacetsContent hidden"></span>

    <a href="" class="btn btn-link p-0 m-top-0 m-left-10">
        <strong>Clear all filters</strong>
    </a>
</div>

<script>
//    $(document).ready(function() {
//        $('.filterContainer .dropdown-menu > li').hover(
//            function(){
//                $('.submenu').hide();
//                $('.filterContainer .dropdown-menu li').removeClass('hovered');
//                $(this).addClass('hovered');
//                $(this).find('.submenu').show();
//            }
//        );
//
////        try {
////            history.replaceState(null,null,'/id/');
////        }
////        catch(e) {
////            location.hash = '#id_';
////        }
//
//        return false;
//    });
</script>








<li class="m-0">
    <section data-nss-score="0" data-position="1" data-ng-click="loadProfile($event)" class="top-bottom-40 js-log-profile-tile">
        <div class="row">
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-10">
                        <p class="text-center m-bottom-0"><a result-click="" href="/portfolio/show/<%=profile.ciphertext%>">
                                <img alt="<%=profile.first_name%> <%=profile.last_name%>" src="<% if(!_.isNull(profile.avatar_filename)) { %>
                                    <%=getUrlPrefix(profile.avatar_filename)%>
                                    <% } else { %> /app/images/noavatar.png <% }; %>" class="avatar search avatar-lg m-top-0 m-left-0"></a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="text-center ng-isolate-scope">
                            <button class="btn btn-default m-left-right-0 m-top-bottom-10 p-left-right-20 sf-button-save <% if(!_.isNull(profile.favorite)) { %>active<% } %>" onclick="App.Api.modalShowSave($(this));" data-title="<%=profile.job_title%>" data-name="<%=user_name%>" data-ciphertext="<%=profile.ciphertext%>" data-note="<%=profile.note%>"
                                    data-avatar="<% if(!_.isNull(profile.avatar_filename)) { %>
                                            <%=getUrlPrefix(profile.avatar_filename)%>
                                            <% } else { %> /app/images/noavatar.png <% }; %>" data-active-save="<% if(_.isNull(profile.favorite)) { %>false<% } else { %>true<% } %>" data-false-value="Save" data-true-value="Saved">
                                <span class="sf-button-save-text"><span class="glyphicon <% if(_.isNull(profile.favorite)) { %>glyphicon-heart-empty<% } else { %>glyphicon-heart<% } %>"></span><span class="save-text"><% if(_.isNull(profile.favorite)) { %>Save<% } else { %>Saved<% } %></span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 p-left-10">
                <div>
                        <span class="profileTitle h3 m-top-bottom-0">
                            <a title="<%=profile.first_name%> <%=profile.last_name%>" class="jsShortName" href="/portfolio/show/<%=profile.ciphertext%>"><%=user_name%></a>
                        </span>
                    <h2 class="profileTitleExpert m-top-5 m-bottom-0 ng-binding"><%=profile.job_title%></h2>
                    <p class="m-top-20 m-bottom-0">
                        <span class="glyphicon glyphicon-map-marker m-left-0"></span>
                        <strong class="country"><%=profile.countries_name%></strong>
                            <span class="text-muted">
                                -
                                <span class="lastActivity">
                                    Last active: 8 days ago
                                </span>
                                <% if(profile.count_portfolio != 0) { %>
                                    -
                                    <span class="portfolios">
                                        Portfolio: <span class="button-link"><%=profile.count_portfolio%></span>
                                    </span>
                                <% } %>
                            </span>
                    </p>
                    <p class="description m-top-20 m-bottom-0"><%=profile.description%>
                    </p>
                    <%if(!_.isEmpty(profile.expertises)){ %>
                    <ul class="list-inline m-top-20">
                        <% var i=0;
                        _.each(profile.expertises, function(expert){
                        if (i==4) return false; %>
                        <li><a target="_self" class="button-link tag-skill m-0 show-pop" data-expertises="<%=expert.slug_name%>" data-content="<%=expert.expertises_description%>" href="/o/profiles/browse/skill/linux-system-administration/" eo-popover-append-to-body="true" eo-popover-hoverable="true" eo-popover-trigger="mouseenter" eo-popover-placement="bottom">
                                <%=expert.name%>
                            </a></li>
                        <% i++;
                        }); %>
                        <% if (profile.expertises.length - 4 > 0) { %>
                        <li><a title="6 more" class="btn button-link m-0 p-0" href="#">
                                <%=profile.expertises.length - 4 %> more
                            </a>
                        </li>
                        <% } %>
                    </ul>
                    <% } %>
                </div>
            </div>
            <div class="col-md-3 text-right">
                <div class="row">
                    <div class="col-md-12">
                        <a role="button" href="#" class="btn btn-default сontactBtn m-top-bottom-0 m-left-0 sf-button-save">Contact</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 m-top-20">
                        <div class="rate m-bottom-10">
                            <strong>$15.00</strong>
                            <span class="m-left-5"> /hr</span>
                        </div>
                        <div class="hours m-bottom-10">
                            <strong>
                                9
                            </strong>
                            <span class="m-left-5"> hours</span>
                        </div>
                        <div class="oFeedback m-sm-bottom">
                                <span popover-trigger="mouseenter" popover-placement="bottom" popover="5.00 stars, based on 2 reviews">
                                    <strong>5.00 </strong>
                                    <div class="m-xs-left display-inline-block ng-pristine ng-untouched ng-valid ng-isolate-scope" read-only="true" rating-define="star" ng-model="tile.fb_1" stars="5" eo-rating="" ng-init="tile.fb_1 = 5.00">
                                        <div ng-mouseleave="leaveRating()" ng-mousemove="changeRating($event)" class="stars" style="visibility: visible;">
                                            <canvas width="80" height="16" class="star ng-scope" ng-click="secureNewRating()"></canvas>
                                        </div>
                                    </div>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</li>