/**
 * Created by zhuk on 20.10.15.
 */
$(document).ready(function(){
    $('.timer-accept').each(function() {
        var obj = $(this);
        var ts = obj.attr('data-time-reject')*1000;
        App.Api.setTimer(ts, obj);
    });
});