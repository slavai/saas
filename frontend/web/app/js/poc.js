/**
 * Created by zhuk on 09.10.15.
 */
$(document).ready(function(){
    $('#sendPOCUpdate').on('click', '.request-update-poc', function(e) {
        e.preventDefault();
        App.Api.sendPOCUpdate($(this).parents('#sendPOCUpdate'));
    });

    $('#sendRequestMedication').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var patient = button.data('patient');
        $(this).find('.alert').hide();
        $(this).find('.message-text').val('');
        $(this).find('.request-medication').attr('data-patient',patient);
        $(this).find('.message-text').focus();
    });
});