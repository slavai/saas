/**
 * Created by ArsenM on 2/5/16.
 */
function showMsg(type, msg, title){
    var $content = $('#info-msg');
    var title = title ? title : type;
    var d = new Date();
    var id = 'info-' + d.getTime();

    var $html = '<div class="alert alert-' + type + ' alert-dismissible" id="' + id + '">\
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                    <h4><i class="icon fa fa-check"></i>' + title + '</h4>' + msg + '\
                </div>';
    $content.append($html);
    setTimeout(function(){
        $('#' + id).fadeOut(2000).remove();
    }, 5000);
}