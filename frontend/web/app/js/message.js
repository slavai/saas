/**
 * Created by zhuk on 09.10.15.
 */
$(document).ready(function(){
    //$('.div-popover').popover();

    /*$('#sendMessage').on('click', '.send-message', function(e) {
        e.preventDefault();
        App.Api.setSendingMessage($(this).parents('#sendMessage'));
    });*/

    $('#sendMessage').on('show.bs.modal', function () {
        $(this).find('.alert').hide();
        $(this).find('.message-text').val('');
        $('.message-text').focus();
    });

    /*==Slim Scroll ==*/
    if ($.fn.slimScroll) {
        $('.direct-chat-messages').slimscroll({
            height: '360px',
            wheelStep: 35
        });
    }

    /*Chat*/
    $(function () {
        $('.chat-input').keypress(function (ev) {
            var p = ev.which;
            var chatTime = moment().format("DD MMM h:mm a");
            var chatText = $('.chat-input').val();
            if (p == 13) {
                if (chatText == "") {
                    alert('Empty Field');
                } else {
                    $('<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">John Carry</span>'+
                    '<span class="direct-chat-timestamp pull-right">' + chatTime + '</span></div>' +
                    '<img class="direct-chat-img" src="/app/images/user1-128x128.jpg" alt="message user image">' +
                    '<div class="direct-chat-text">' + chatText + '</div></div>').appendTo('.direct-chat-messages');
                }
                $(this).val('');
                $('.direct-chat-messages').scrollTo('100%', '100%', {
                    easing: 'swing'
                });
                return false;
                ev.epreventDefault();
                ev.stopPropagation();
            }
        });


        $('.chat-send .btn').click(function () {
            var chatTime = moment().format("DD MMM a h:mm");
            var chatText = $('.chat-input').val();
            if (chatText == "") {
                alert('Empty Field');
                $(".chat-input").focus();
            } else {
                $('<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">John Carry</span>'+
                '<span class="direct-chat-timestamp pull-right">' + chatTime + '</span></div>' +
                '<img class="direct-chat-img" src="/app/images/user1-128x128.jpg" alt="message user image">' +
                '<div class="direct-chat-text">' + chatText + '</div></div>').appendTo('.direct-chat-messages');

                $('.chat-input').val('');
                $(".chat-input").focus();
                $('.direct-chat-messages').scrollTo('100%', '100%', {
                    easing: 'swing'
                });
            }
        });
    });
});