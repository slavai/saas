/**
 * Created by zhuk on 09.10.15.
 */
var App = {};

$(document).ready(function() {
    App.Api.getActiveNotifications();
//function getUrlPrefix($file){
//    var bucket = '88creatives-dev';
//    return 'https://'+ bucket +'.s3.amazonaws.com/'+$file;
//}
});

App.Api = {
    xhr: false,
    //initPopover: function() {
    //    var settings = {
    //        trigger:'hover',
    //        title:'Description',
    //        width:320,
    //        multi:true,
    //        closeable:false,
    //        style:'',
    //        delay:300,
    //        padding:true,
    //        animation:'pop'
    //    };
    //
    //    $('.show-pop').webuiPopover('destroy').webuiPopover($.extend({},settings))
    //},
   /* setSendingMessage: function(obj) {
        $('.section-loading').show();
        var param = {
            text: $(obj).find('.message-text').val(),
            recepient: $(obj).attr('data-recepient')
        };

        App.Api.xhr = $.ajax({
            method: 'POST',
            url: "/api/set-sending-message",
            dataType: 'json',
            data: param,
            success: function(response) {
                if (response.result == "success") {
                    $(obj).find('.alert').hide();
                    $('.section-loading').hide();
                    $(obj).modal('hide');
                }
                else {
                    $(obj).find('.error-message ul').html('');
                    jQuery.each(response.error_message, function() {
                        for(var i=0; this.length-1 >= i; i++)
                            $(obj).find('.error-message ul').append("<li>"+this[i]+"</li>");
                    });
                    $(obj).find('.alert').show();
                    $('.section-loading').hide();
                }
            }
        });
    },*/
    setRequestMedication: function(obj) {
        $('.section-loading').show();
        var param = {
            text: $(obj).find('.message-text').val(),
            patient: patient
        };

        App.Api.xhr = $.ajax({
            method: 'POST',
            url: "/api/set-request-medication",
            dataType: 'json',
            data: param,
            success: function(response) {

                /* send request medication to agency */
                socket.emit('send request medication', response);
                if (response.result == "success") {
                    $(obj).find('.alert').hide();
                    $('.section-loading').hide();
                    $(obj).modal('hide');

                }
                else {
                    $(obj).find('.error-message ul').html('');
                    jQuery.each(response.error_message, function() {
                        for(var i=0; this.length-1 >= i; i++)
                            $(obj).find('.error-message ul').append("<li>"+this[i]+"</li>");
                    });
                    $(obj).find('.alert').show();
                    $('.section-loading').hide();
                }
            }
        });
    },
    sendPOCUpdate: function(obj) {
        $('.section-loading').show();
        var param = {
            medications: $(obj).find('#modal-plancare-medications').val(),
            dmesupplies: $(obj).find('#modal-plancare-dme_supplies').val(),
            treatments: $(obj).find('#modal-plancare-treatments').val(),
            dischargeplans: $(obj).find('#modal-plancare-discharge_plans').val(),
            patient: $(obj).find('.request-update-poc').attr('data-patient')
        };

        App.Api.xhr = $.ajax({
            method: 'POST',
            url: "/api/set-request-poc-update",
            dataType: 'json',
            data: param,
            success: function(response) {
                if (response.result == "success") {
                    $(obj).find('.alert').hide();
                    $('.section-loading').hide();
                    $(obj).modal('hide');
                }
                else {
                    $(obj).find('.error-message ul').html('');
                    jQuery.each(response.error_message, function() {
                        for(var i=0; this.length-1 >= i; i++)
                            $(obj).find('.error-message ul').append("<li>"+this[i]+"</li>");
                    });
                    $(obj).find('.alert').show();
                    $('.section-loading').hide();
                }
            }
        });
    },
    acceptPatient: function(id, obj, page) {
        App.Api.xhr = $.ajax({
            method: 'POST',
            url: "/api/set-accept-patient",
            dataType: 'json',
            data: {
                id : id
            },
            success: function(response) {
                if (response.result == "success") {
                    swal({
                        title: "Patient Accepted!",
                        text: "You have accepted the patient for treatment!",
                        type: "success",
                        confirmButtonColor: "#DD6B55"
                    }, function () {
                        if (page == 'ref')
                            document.location.href = ('/agencys/current-patients');
                        else
                            $(obj).parents('.patient').remove();
                    });
                }
            }
        });
    },
    rejectPatient: function(id, obj, page) {
        swal({
            title: 'Patient Rejected!',
            text: "You have rejected the patient!<br> You may still accept the patient during 24 hours.<br>",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Write your comment",
            confirmButtonColor: "#DD6B55",
            html: true
        },
        function(inputValue) {
            if (inputValue === false) return false;

            App.Api.xhr = $.ajax({
                method: 'POST',
                url: "/api/set-reject-patient",
                dataType: 'json',
                data: {
                    id : id,
                    comment : inputValue
                },
                success: function(response) {
                    if (response.result == "success") {
                        swal.close();
                        if (page == 'ref')
                            document.location.href = ('/agencys/new-patient-referrals');
                        else {
                            var parobj = $(obj).parent();
                            $(obj).tooltip('hide');
                            $(obj).remove();
                            parobj.append('<span data-toggle="tooltip" data-placement="bottom"'+
                                'title=".... is left to take the final decision" class="timer-accept btn btn-sm btn-danger"></span>');
                            var ti = (new Date()).getTime() + 24*60*60*1000;
                            App.Api.setTimer(ti, parobj.find('.timer-accept'));
                        }
                    }
                }
            });
        });
    },
    getActiveNotifications: function() {
        App.Api.xhr = $.ajax({
            method: 'GET',
            url: "/api/get-active-notifications",
            dataType: 'json',
            success: function(response) {
                if (response.result == "success") {
                    if (response.count != 0) {
                        $('.notifications-count').text(response.count);
                        $('.notifications-count-title').text(response.count);
                    }
                    else
                        $('.notifications-count').text(response.count);
                    App.Render.appendPartial($('.head-notifications'), 'headerNotification', response);
                }
            }
        });
    },
    setTimer: function(ts, obj) {
        $('.test-timer').countdown({
            timestamp	: ts,
            callback	: function(days, hours, minutes, seconds) {
                var message = "";

                message += hours + ":";
                message += minutes + ":";
                message += seconds;

                obj.html(message);
            }
        });
    },
    acceptOrder: function(id, obj) {
        var param = {
            id: id
        };
        App.Api.xhr = $.ajax({
            method: 'POST',
            url: "/api/set-accept-order",
            dataType: 'json',
            data: param,
            success: function(response) {
                if (response.result == 'success') {
                    swal({
                        title: "Order Accepted!",
                        text: "You have accepted the order for patient!",
                        type: "success",
                        confirmButtonColor: "#DD6B55"
                    }, function () {
                        var parentobj = $(obj).parents('tbody')
                        $('.history-medication > tbody:last').append("<tr><td>"+response.data.date+"</td><td>"+response.data.text+"</td></tr>");
                        $(obj).parents('tr').remove();
                        if (parentobj.find('tr').length == 0) {
                            parentobj.append("<tr><td colspan='5'>No result</td></tr>");
                        }
                    });
                }
            }
        });
    },
    acceptOrderByPhysician: function(id, obj) {
        var param = {
            id: id
        };
        App.Api.xhr = $.ajax({
            method: 'POST',
            url: "/api/set-accept-order-physician",
            dataType: 'json',
            data: param,
            success: function(response) {
                if (response.result == 'success') {
                    swal({
                        title: "Order Accepted!",
                        text: "You have accepted the order for patient!",
                        type: "success",
                        confirmButtonColor: "#DD6B55"
                    }, function () {
                        var parentobj = $(obj).parents('tbody')
                        $('.history-medication > tbody:last').append("<tr><td>"+response.data.date+"</td><td>"+response.data.text+"</td><td>The agency hasn't accepted the request yet</td></tr>");
                        $(obj).parents('tr').remove();
                        if (parentobj.find('tr').length == 0) {
                            parentobj.append("<tr><td colspan='5'>No result</td></tr>");
                        }
                    });
                }
            }
        });
    }
    //modalShowSave: function(btn) {
    //    var modal = $('#favoriteFr');
    //    modal.find('.modal-footer').addClass('display-none');
    //    if ($(btn).attr('data-active-save') === 'false')
    //        modal.find('.add-favorite').removeClass('display-none');
    //    else
    //        modal.find('.update-favorite').removeClass('display-none');
    //
    //    modal.find('.modal-body img.avatar').attr('src',$(btn).attr('data-avatar'));
    //    modal.find('.modal-body img.avatar').attr('alt',$(btn).attr('data-name'));
    //    modal.find('.modal-body div.name-fr').text($(btn).attr('data-name'));
    //    modal.find('.modal-body .title-fr').text($(btn).attr('data-title'));
    //    modal.find('.modal-body .description').val($(btn).attr('data-note'));
    //    modal.find('.modal-body img.avatar').attr('data-ciphertext',$(btn).attr('data-ciphertext'));
    //
    //    $('#favoriteFr').modal('show');
    //},
    //editFavorite: function() {
    //    var ciphertext = $('#favoriteFr img').attr('data-ciphertext');
    //    var notes = $('#favoriteFr .description').val();
    //
    //    App.Api.xhr = $.ajax({
    //        method: 'POST',
    //        url: "/api/edit-favorite-fr",
    //        dataType: 'json',
    //        data: {
    //            notes: notes,
    //            ciphertext: ciphertext
    //        },
    //        success: function(response) {
    //            if (response.result == 'success') {
    //                $('#favoriteFr').modal('hide');
    //                var button = $(".content").find("button[data-ciphertext='" + ciphertext + "']");
    //                $(button).attr('data-note', notes);
    //            }
    //        }
    //    });
    //},
    //removeFavorite: function() {
    //    var ciphertext = $('#favoriteFr img').attr('data-ciphertext');
    //
    //    App.Api.xhr = $.ajax({
    //        method: 'POST',
    //        url: "/api/remove-favorite-fr",
    //        dataType: 'json',
    //        data: {ciphertext: ciphertext},
    //        success: function(response) {
    //            if (response.result == 'success') {
    //                $('#favoriteFr').modal('hide');
    //                var button = $(".content").find("button[data-ciphertext='" + ciphertext + "']");
    //                $(button).removeClass('active');
    //                $(button).attr('data-note', '');
    //                $(button).find('span.glyphicon').removeClass('glyphicon-heart').addClass('glyphicon-heart-empty');
    //                $(button).find('span.save-text').text($(button).attr('data-false-value'));
    //                $(button).attr('data-active-save','false');
    //            }
    //        }
    //    });
    //},
    //queryString: function() {
    //    var query_string = {};
    //    var query = window.location.search.substring(1);
    //    if (query != '') {
    //        var vars = query.split("&");
    //        for (var i=0;i<vars.length;i++) {
    //            var pair = vars[i].split("=");
    //            // If first entry with this name
    //            if (typeof query_string[pair[0]] === "undefined") {
    //                query_string[pair[0]] = decodeURIComponent(pair[1]);
    //                // If second entry with this name
    //            } else if (typeof query_string[pair[0]] === "string") {
    //                var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
    //                query_string[pair[0]] = arr;
    //                // If third or later entry with this name
    //            } else {
    //                query_string[pair[0]].push(decodeURIComponent(pair[1]));
    //            }
    //        }
    //    }
    //    return query_string;
    //}
};

App.Render = {
    appendPartial: function(el, template, data, fun, fun1) {
        $.ajax({
            type: 'GET',
            url: '/app/templates/'+template+'.tpl',
            success: function(response){
                var partial = _.template(response);
                el.append(partial(data));
            }
        }).done(function (data) {
            if( fun != null) fun();
            if( fun1 != null) fun1();
            $('.section-loading').hide();

        });
    },
    renderPartial: function(el, template, data, fun, fun1) {
        $.ajax({
            type: 'GET',
            url: '/app/templates/'+template+'.tpl',
            success: function(response) {
                var partial = _.template(response);
                el.html(partial(data));
            }
        }).done(function (data) {
            console.log('success');
            $('.section-loading').hide();
        });
    }
};

socket.on('send-request-medication', function(data){
   showMsg('info', 'You received  new request medication from user', 'New request medication')
});
if ($('.footer').hasClass('active')) $('.menu').addClass('test')
else $('.menu').removeClass('test')