<?php

namespace app\components;

use common\models\LoginData;
use Yii;
use app\models\Message;
use yii\helpers\ArrayHelper;

class Controller extends \yii\web\Controller
{
    public $active_message;
    public $userRole = false;
    public $user;
    function init(){
        if (!\Yii::$app->user->isGuest) {
//            $this->active_message = Message::getAllActiveMessage();
            $this->user = Yii::$app->user->identity;
            $this->userRole = Yii::$app->user->isGuest ? false
                : (Yii::$app->user->identity->type == LoginData::TYPE_AGENCY ? LoginData::TYPE_AGENCY_NAME : LoginData::TYPE_PHYSICIAN_NAME);
//            var_dump('<pre>', $this->user, '</pre>'); die();
        }
        parent::init();
    }
}
