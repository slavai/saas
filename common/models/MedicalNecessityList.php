<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "medical_necessity_list".
 *
 * @property integer $id
 * @property string $name
 */
class MedicalNecessityList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'medical_necessity_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return MedicalNecessityListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MedicalNecessityListQuery(get_called_class());
    }
}
