<?php

namespace common\models;

use common\models\PhysicianRecomendationAgency;
use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "agencys".
 *
 * @property integer $id
 * @property string $name
 * @property string $admin_name
 * @property string $phone
 * @property string $phone_fax
 * @property string $address
 * @property string $agency_email
 * @property string $skilled_nursing
 * @property string $physical_therapy
 * @property string $speech_language_therapy
 * @property string $occupational_therapy
 * @property string $home_health_aide
 * @property string $medical_social_services
 * @property string $summary
 * @property string $avatar_filename
 * @property string $city
 * @property string $state
 * @property string $country_code
 * @property string $postCode
 */
class Agencys extends \yii\db\ActiveRecord
{
    const ROLE_NAME = 'agency';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agencys';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'admin_name', 'phone', 'address', 'agency_email'], 'required'],
            [['id'], 'integer'],
            [['skilled_nursing', 'physical_therapy', 'speech_language_therapy', 'occupational_therapy', 'home_health_aide', 'medical_social_services', 'summary'], 'string'],
            [['name', 'admin_name'], 'string', 'max' => 100],
            [['phone', 'phone_fax'], 'string', 'max' => 21],
            [['address', 'avatar_filename', 'city', 'state', 'postCode'], 'string', 'max' => 255],
            [['agency_email'], 'string', 'max' => 50],
            [['country_code'], 'string', 'max' => 2],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'name' => Yii::t('app', 'Agency Name'),
            'admin_name' => Yii::t('app', 'Admin Name'),
            'phone' => Yii::t('app', 'Phone'),
            'phone_fax' => Yii::t('app', 'Phone Fax'),
            'address' => Yii::t('app', 'Address'),
            'agency_email' => Yii::t('app', 'Agency Email'),
            'skilled_nursing' => Yii::t('app', 'Skilled Nursing'),
            'physical_therapy' => Yii::t('app', 'Physical Therapy'),
            'speech_language_therapy' => Yii::t('app', 'Speech Language Therapy'),
            'occupational_therapy' => Yii::t('app', 'Occupational Therapy'),
            'home_health_aide' => Yii::t('app', 'Home Health Aide'),
            'medical_social_services' => Yii::t('app', 'Medical Social Services'),
            'summary' => Yii::t('app', 'Summary'),
            'avatar_filename' => Yii::t('app', 'Avatar Filename'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'country_code' => Yii::t('app', 'Country Code'),
            'postCode' => Yii::t('app', 'Post Code'),
        ];
    }

    public static function shortClassName() {
        return 'Agencys';
    }

    /**
     * @inheritdoc
     * @return AgencysQuery the active query used by this AR class.
     */

    public function getUser()
    {
        return $this->hasOne(LoginData::className(), ['id' => 'id']);
    }

    public function getPhysicianRecomAgency()
    {
        return $this->hasOne(PhysicianRecomendationAgency::className(), ['id_agency' => 'id'])
            ->andWhere([
                PhysicianRecomendationAgency::tableName().'.id_physician' => \Yii::$app->user->id
            ]);
    }

    public function getLogo($type = 'original') {
        if ($this->avatar_filename == null) {
            $logo_filename = Yii::$app->request->baseUrl . "/app/images/noavatar.png";
        } else {
            if ($type == 'original')
                $logo_filename = '/app/images/avatar/' . $this->avatar_filename;
            elseif ($type == 'square') {
                $path_parts = pathinfo($this->avatar_filename);
                $pieces = explode(".", basename($this->avatar_filename));
                $logo_filename = '/app/images/avatar/' . $pieces[0] . '_square.' . $path_parts['extension'];
            }
        }

        return $logo_filename;
    }

    public function getCountry() {
        return $this->hasOne(Countries::className(), ['countries_iso_code_2' => 'country_code']);
    }

    public function setUserAvatar($files) {
        if (file_exists($files['tmp_name']['avatar_filename'])) {

            if (!file_exists(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"]))
                mkdir(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"], 0777);
            $image = Yii::$app->image->load($files['tmp_name']['avatar_filename']);

            $size = getimagesize($files['tmp_name']['avatar_filename']);
            $crop_size = ($size[0] > $size[1]) ? $size[1] : $size[0];

            $fileinfo = pathinfo($files['name']['avatar_filename']);
            $file_name = md5_file($files['tmp_name']['avatar_filename']) . '.' . $fileinfo['extension'];
            $file_name_square = md5_file($files['tmp_name']['avatar_filename']).'_square'.'.'.$fileinfo['extension'];
            $file_name_full = Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"].$file_name;

            $status = $image->save($file_name_full);

            Image::thumbnail(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"].$file_name, $crop_size, $crop_size)
                ->save(Yii::getAlias('@frontend').Yii::$app->params["uploadUserFileDir"].$file_name_square, ['quality' => 100]);

            if ($status)
                return $file_name;
        }
    }
}
