<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "functional_limitations".
 *
 * @property integer $id
 * @property string $num
 * @property string $name
 */
class FunctionalLimitations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'functional_limitations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num', 'name'], 'required'],
            [['num'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'num' => Yii::t('app', 'Num'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return FunctionalLimitationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FunctionalLimitationsQuery(get_called_class());
    }
}
