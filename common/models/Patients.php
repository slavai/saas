<?php

namespace common\models;

use app\models\FaceToFaceEncounter;
use app\models\PatientAssociateAgency;
use app\models\PatientsDiagnosis;
use app\models\PlanCare;
use Yii;

/**
 * This is the model class for table "patients".
 *
 * @property integer $id
 * @property integer $id_physician
 * @property string $name
 * @property string $birthday
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $gender
 * @property string $photo
 * @property string $ss
 * @property string $emergency_contact_name
 * @property string $emergency_contact_phone
 * @property string $emergency_contact_address
 * @property string $insurance_plan1
 * @property string $insurance_policy1
 * @property string $insurance_plan2
 * @property string $insurance_policy2
 * @property string $clinical_findings
 * @property string $reason_patient_homebound
 * @property integer $date_create
 * @property string $status_care
 * @property integer $date_care_end
 */
class Patients extends \yii\db\ActiveRecord
{
    public $necessary = ['Skilled Nursing', 'Physical Therapy', 'Home Health Aides', 'Occupational Therapy', 'Speech Therapy', 'Medical Social Work'];
    private static $gender_array = ['F' => 'Female','M' => 'Male'];

    /**
     * @inheritdoc
     */
    const STATUS_CARE = '0';
    const STATUS_CARE_END = '1';

    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';

    const SCENARIO_PATIENT_0 = 'scenario.patientPlanOfCare';
    const SCENARIO_PATIENT_1 = 'scenario.patientReferralForm';
    const SCENARIO_PATIENT_2 = 'scenario.patientFaceToFace';

    public static function tableName()
    {
        return 'patients';
    }

    public static function getGender() {
        return self::$gender_array;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'address', 'birthday', 'ss', 'gender', 'emergency_contact_name', 'emergency_contact_phone', 'emergency_contact_address', 'insurance_plan1', 'insurance_policy1', 'insurance_plan2', 'insurance_policy2', 'clinical_findings', 'reason_patient_homebound'], 'required', 'on' => [self::SCENARIO_PATIENT_0]],
            [['birthday', 'date_care_end'], 'safe', 'on' => [self::SCENARIO_PATIENT_0]],
            [['gender', 'status_care'], 'string', 'on' => [self::SCENARIO_PATIENT_0]],
            [['name', 'insurance_plan1', 'insurance_policy1', 'insurance_plan2', 'insurance_policy2', 'reason_patient_homebound'], 'string', 'max' => 100, 'on' => [self::SCENARIO_PATIENT_0]],
            [['phone'], 'string', 'max' => 21, 'on' => [self::SCENARIO_PATIENT_0]],
            [['address', 'photo', 'emergency_contact_address', 'clinical_findings'], 'string', 'max' => 255, 'on' => [self::SCENARIO_PATIENT_0]],
            [['email', 'emergency_contact_name'], 'string', 'max' => 50, 'on' => [self::SCENARIO_PATIENT_0]],
            [['ss'], 'string', 'max' => 10, 'on' => [self::SCENARIO_PATIENT_0]],
            [['emergency_contact_phone'], 'string', 'max' => 30, 'on' => [self::SCENARIO_PATIENT_0]],

            [['name', 'address', 'birthday'], 'required', 'on' => [self::SCENARIO_PATIENT_1]],

            [['name', 'birthday'], 'required', 'on' => [self::SCENARIO_PATIENT_2]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_physician' => Yii::t('app', 'Id Physician'),
            'name' => Yii::t('app', 'Name'),
            'birthday' => Yii::t('app', 'Birthday'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Gender'),
            'photo' => Yii::t('app', 'Photo'),
            'ss' => Yii::t('app', 'Ss'),
            'emergency_contact_name' => Yii::t('app', 'Emergency Contact Name'),
            'emergency_contact_phone' => Yii::t('app', 'Emergency Contact Phone'),
            'emergency_contact_address' => Yii::t('app', 'Emergency Contact Address'),
            'insurance_plan1' => Yii::t('app', 'Insurance Plan'),
            'insurance_policy1' => Yii::t('app', 'Insurance Policy'),
            'insurance_plan2' => Yii::t('app', 'Insurance Plan'),
            'insurance_policy2' => Yii::t('app', 'Insurance Policy'),
            'clinical_findings' => Yii::t('app', 'Clinical Findings'),
            'reason_patient_homebound' => Yii::t('app', 'Reason Patient Homebound'),
            'date_create' => Yii::t('app', 'Date Create'),
            'status_care' => Yii::t('app', 'Status Care'),
            'date_care_end' => Yii::t('app', 'Date Care End'),
        ];
    }

    /**
     * @inheritdoc
     * @return PatientsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatientsQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $birthday = explode('/', $this->birthday);
            $this->birthday = gmmktime(0,0,0,$birthday[0],$birthday[1],$birthday[2]);

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        if ($this->birthday != NULL)
            $this->birthday = date('m/d/Y', $this->birthday);

        parent::afterFind();
    }

    public function getDiagnosis()
    {
        return $this->hasOne(PatientsDiagnosis::className(), ['patient_id' => 'id']);
    }
    public function getPhysician()
    {
        return $this->hasOne(Physician::className(), ['id' => 'id_physician']);
    }

    public function getAgency()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency'])
            ->viaTable('patient_associate_agency', ['id_patient' => 'id'],
                function($query) {
                    return $query->andWhere('status_send != :status or (status_send = :status and date_end_accept > :date_end_accept)',
                        [':status' => PatientAssociateAgency::STATUS_REJECT, 'date_end_accept' => time()]);
                });
    }
    public function getAgencyReject()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency'])
            ->viaTable('patient_associate_agency', ['id_patient' => 'id'],
                function($query) {
                    return $query->andWhere('status_send = :status',
                        [':status' => PatientAssociateAgency::STATUS_REJECT]);
                });
    }
    public function getAssociateAgency()
    {
        return $this->hasOne(PatientAssociateAgency::className(), ['id_patient' => 'id']);
    }
    public function getAssociateAgencyAccept()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency'])
            ->viaTable('patient_associate_agency', ['id_patient' => 'id'],
                function($query) {
                    return $query->andWhere('status_send = :status', [':status' => PatientAssociateAgency::STATUS_ACCEPT]);
                });
    }
    public function getPatientAgency()
    {
        return $this->hasOne(PatientAssociateAgency::className(), ['id_patient' => 'id'])
            ->andWhere([
                PatientAssociateAgency::tableName().'.status_send' => PatientAssociateAgency::STATUS_ACCEPT
            ]);
    }
    public function getPlanOfCare()
    {
        return $this->hasOne(PlanCare::className(), ['id_patient' => 'id']);
    }

    public function getFaceToFace()
    {
        return $this->hasOne(FaceToFaceEncounter::className(), ['id_patient' => 'id']);
    }
}
