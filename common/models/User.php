<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace common\models;

use Yii;
use yii\web\IdentityInterface;

class User extends \dektrium\user\models\User implements IdentityInterface
{
    public $myrole;
    public $myrole_name;

    public static function findIdentity($id)
    {
        $user = static::findOne($id);
        $user->myrole = \Yii::$app->authManager->getRolesByUser($id);
        $user->role = key($user->myrole);
        return $user;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['update'][]   = 'last_login';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['fieldRequired'] = ['last_login', 'required', 'on' => ['update']];
        $rules['fieldLength']   = ['last_login', 'integer', 'on' => ['update']];

        return $rules;
    }

    /**
     * @return bool Whether the user is an admin or not.
     */
    public function getIsAdmin()
    {
        return ($this->myrole_name == 'admin') ? true : false;
    }
}
