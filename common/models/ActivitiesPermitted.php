<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "activities_permitted".
 *
 * @property integer $id
 * @property string $num
 * @property string $name
 */
class ActivitiesPermitted extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activities_permitted';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num', 'name'], 'required'],
            [['num'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'num' => Yii::t('app', 'Num'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return ActivitiesPermittedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActivitiesPermittedQuery(get_called_class());
    }
}
