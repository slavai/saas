<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PhysicianRecomendationAgency]].
 *
 * @see PhysicianRecomendationAgency
 */
class PhysicianRecomendationAgencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PhysicianRecomendationAgency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PhysicianRecomendationAgency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}