<?php

namespace common\models;

use common\models\Agencys;
use Yii;

/**
 * This is the model class for table "physician_recomendation_agency".
 *
 * @property integer $id
 * @property integer $id_physician
 * @property integer $id_agency
 */
class PhysicianRecomendationAgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'physician_recomendation_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_physician', 'id_agency'], 'required'],
            [['id_physician', 'id_agency'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_physician' => Yii::t('app', 'Id Physician'),
            'id_agency' => Yii::t('app', 'Id Agency'),
        ];
    }

    /**
     * @inheritdoc
     * @return PhysicianRecomendationAgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhysicianRecomendationAgencyQuery(get_called_class());
    }

    public function getAgency()
    {
        return $this->hasOne(Agencys::className(), ['id' => 'id_agency']);
    }
}
