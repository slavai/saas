<?php
namespace common\user_exceptions\classes;




class AccessDeniedException extends RenderedUserException {
 
    
    public function getName() 
    {
        return "Access denied";
    }
}
