<?php
namespace common\user_exceptions\classes;


class ContestLaunchDisabledException extends RenderedUserException {

    
    
    //optional
    public function getName() 
    {
        return "Launch disabled";
    }
}
