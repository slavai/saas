<?php
namespace common\user_exceptions\classes;


class ExampleUserException extends RenderedUserException {
  
    /**
     * Uncomment below line to specify custom exception file name.
     * The file should be placed in "@common/user_exceptions/views"
     * 
     * In case the $viewFile is not specified it will look for default viewFile 
     * based on this class name (in this situation: example-user-exception).
     * If default viewFile also not exist, it will use generic exception view.
     * 
     * Specify the viewFile without extension (.php)
     */
    //public $viewFile = "any-filename-here";

    
    /**
     * Human-friendly message describing the exception. 
     * Should be written in English (sourceLanguage).
     * its translated to another languages.
     * 
     * The whole exception object is passed to view specified by $viewFile,
     * so it's available to display $exceptionMessage on that view if needed.
     * 
     */
    //public $exceptionMessage;
    
    
    //optional
    public function getName() 
    {
        return "Example exception";
    }
}
