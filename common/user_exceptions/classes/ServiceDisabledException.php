<?php
namespace common\user_exceptions\classes;


class ServiceDisabledException extends RenderedUserException {
 
    
    public function getName() 
    {
        return "Service disabled";
    }
}
