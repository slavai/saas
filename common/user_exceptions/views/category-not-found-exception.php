<?php
    use yii\helpers\Url;
?>

<div class="row">
    <div class="col-sm-6">
        <h1><?= Yii::t('user-exception', 'Category not found :('); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <p><?= Yii::t('user-exception', 'Please contact us if you think this is a server error. Thank you.'); ?></p>
        <p><a href="#" onclick="window.history.back();" class="btn btn-primary"><?= Yii::t('user-exception', 'Go back'); ?></a></p>
    </div>
</div>