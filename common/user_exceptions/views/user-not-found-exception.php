<?php
    use yii\helpers\Url;
?>

<div class="row">
    <div class="col-sm-6">

        <h1><?= Yii::t('app', 'User not found or blocked :('); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <p> <?= Yii::t('app', 'You can find all users'); ?></p>
        <p>
            <a href="<?= Url::to(['site/how']);?>" class="btn btn-default"><?= Yii::t('app', 'How it works'); ?></a>
            <a href="#" onclick="window.history.back();" class="btn btn-primary">
                <?= Yii::t('app', 'Go back'); ?>
            </a>
        </p>
    </div>
</div>