<?php
    use common\user_exceptions\classes\RenderedUserException;
    
/* @var $exception RenderedUserException */
?>


<div class="row">
    <div class="col-sm-12">
        <h1><?= Yii::t('user-exception', 'oops... something went wrong.'); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <p><?= $exception->exceptionMessage ?></p>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <p><?= Yii::t('user-exception', 'The page you were looking for appears to have been moved, deleted or does not exist. You could go back to where you were or head straight to our home page.'); ?></p>
        <p><a href="#" onclick="window.history.back();" class="btn btn-primary"><?= Yii::t('user-exception', 'Go back'); ?></a></p>
    </div>
</div>