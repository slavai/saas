<?php
    use yii\helpers\Url;
?>

<div class="row">
    <div class="col-sm-6">
        <h1><?= Yii::t('user-exception', 'Contest not found :('); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <p>
            <?= Yii::t('user-exception', 'You can find all contests '); ?>
            <a href="<?= Url::to('/search/contests'); ?>"><?= Yii::t('user-exception', 'here'); ?></a>.
        </p>
        <p>
            <a href="#" onclick="window.history.back();" class="btn btn-primary"><?= Yii::t('user-exception', 'Go back'); ?></a>
        </p>
    </div>
</div>