<?php
use common\models\LoginData;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\I18nSourceMessage;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$activkey_token = md5($user->email.$user->userData->name);
$resetLink = $user->type === LoginData::TYPE_ADMIN ?
    Yii::$app->urlManager->createAbsoluteUrl(['site/confirm-email', 'token' => $activkey_token, 'email'=>$user->email]) :
    \Yii::$app->urlManagerFrontEnd->baseUrl . Url::to(['site/confirm-email', 'token' => $activkey_token, 'email'=>$user->email]);

$siteName = 'Saas';
$backgroundColor = '#f7f7f7';
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>88creatives</title>
    <style>
        @media only screen and (max-width: 300px){
            body {
                width:218px !important;
                margin:auto !important;
            }
            .table {width:195px !important;margin:auto !important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}
            span.title{font-size:20px !important;line-height: 23px !important}
            span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}
            td.box p{font-size: 12px !important;font-weight: bold !important;}
            .table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr {
                display: block !important;
            }
            .table-recap{width: 200px!important;}
            .table-recap tr td, .conf_body td{text-align:center !important;}
            .address{display: block !important;margin-bottom: 10px !important;}
            .space_address{display: none !important;}
        }
        @media only screen and (min-width: 301px) and (max-width: 500px) {
            body {width:308px!important;margin:auto!important;}
            .table {width:285px!important;margin:auto!important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
            .table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr {
                display: block !important;
            }
            .table-recap{width: 293px !important;}
            .table-recap tr td, .conf_body td{text-align:center !important;}
        }
        @media only screen and (min-width: 501px) and (max-width: 768px) {
            body {width:478px!important;margin:auto!important;}
            .table {width:450px!important;margin:auto!important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
        }
        @media only screen and (max-device-width: 480px) {
            body {width:308px!important;margin:auto!important;}
            .table {width:285px;margin:auto!important;}
            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
            .table-recap{width: 285px!important;}
            .table-recap tr td, .conf_body td{text-align:center!important;}
            .address{display: block !important;margin-bottom: 10px !important;}
            .space_address{display: none !important;}
        }
    </style>
</head>
<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif; font-size:13px; line-height:18px; margin:auto">
<table class="table table-mail" bgcolor="<?= $backgroundColor?>" style="width:100%;margin-top:10px;">
    <tr>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        <td align="center" style="padding:7px 0">
            <table class="table" bgcolor="<?= $backgroundColor?>" style="width:100%">
                <tr>
                    <td align="left" class="titleblock" style="padding:7px 0">
                        <font face="Open-sans, sans-serif">
                            <span class="title" style="font-weight:500;line-height:18px"><?= Html::encode("Hello") ?>
                                <?= Html::encode($user->userData->name); ?>,
                            </span>
                        </font>
                    </td>
                </tr>

                <tr>
                    <td align="left" class="linkbelow" style="padding:7px 0">
                        <font face="Open-sans, sans-serif">
                            <span>
                                Welcome to the Saas community, it's great to have join us
                            </span><br><br>
                            <span>
                                Please confirm your email address by clicking the link above and then you can enter contests on Saas.
                            </span>
                        </font>
                    </td>
                </tr>

                <tr>
                    <td class="box" style="padding:7px 0">
                        <table class="table" style="width:100%">
                            <tr>
                                <td style="padding:7px 0" valign="middle" align="center">
                                    <table style="font-size:17px;background-color: #2bba7b;text-decoration: none;border-radius: 4px;cursor: pointer;">
                                        <tr>
                                            <td style="width:20px;">&nbsp;</td>
                                            <td height="40">
                                                <a href="<?= $resetLink;?>" style="text-decoration:none;color:#ffffff;">
                                                    Confirm your email address</a>
                                            </td>
                                            <td style="width:20px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="box" style="padding:7px 0">
                        <font face="Open-sans, sans-serif">
                            <span>
                                Your password: <?= $password; ?>
                            </span>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="linkbelow" style="padding:7px 0">
                        <font face="Open-sans, sans-serif">
                            <span>
                                Regards,<br>
                                Saas Support
                            </span>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td height="20" width="100%" style="border-collapse:collapse"></td>
                </tr>
                <tr>
                    <td class="space_footer" align="center" style="padding:0!important">
                        <span>
                            <a title="88creatives" href="http://dev.88creatives.com/" style="text-decoration: none;">
                                <img width="32" src="<?= Yii::$app->urlManager->createAbsoluteUrl('app/images/facebook_social_mail.png') ?>"/>
                            </a>
                        </span>
                        <span>
                            <a title="88creatives" href="http://dev.88creatives.com/" style="text-decoration: none;">
                                <img width="32" src="<?= Yii::$app->urlManager->createAbsoluteUrl('app/images/twitter_social_mail.png') ?>"/>
                            </a>
                        </span>
                    </td>
                </tr>
                <tr align="center">
                    <td class="footer" align="center" style="padding:7px 0;font-size: 12px;">
                        <font face="Open-sans, sans-serif">
                            <span><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('') ?>" style="text-decoration: none;">&copy; <?= $siteName?> GmbH</a></span><br>
                            <span>footer-contact</span><br>
                            <span>footer-contact2</span><br>
                            <span>footer-ceo</span><br>
                        </font>
                    </td>
                </tr>
            </table>
        </td>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
    </tr>
</table>
</body>
</html>