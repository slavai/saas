<?php
namespace backend\controllers;

use common\models\Agencys;
use common\models\Patients;
use common\models\Physician;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DashboardController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ];
    }

    public function actionIndex()
    {
        $patients = Patients::find()->count();

        $agencys = Agencys::find()
            ->count();

        $physicians = Physician::find()
            ->count();

        $currentDate = new \DateTime();
////        $monthRegistrations = User::find()
////            ->where(['between', 'FROM_UNIXTIME(created_at)', 'date_sub(NOW(), INTERVAL 1 MONTH)', 'NOW()' ])
////            ->all();
////        $command = Yii::$app->db->createCommand("SELECT COUNT(id), FROM_UNIXTIME(last_login) FROM user WHERE FROM_UNIXTIME(last_login) BETWEEN date_sub(NOW(), INTERVAL 1 MONTH) AND NOW() GROUP BY DAY(FROM_UNIXTIME(last_login))");
////        $dailyVisitors = $command->queryAll();
//
        return $this->render('index', [
//            'projects' => $projects,
//            'attachments' => $attachments,
            'patients' => $patients,
            'agencys' => $agencys,
            'currentDate' => $currentDate,
            'physicians' => $physicians
        ]);
    }
}
