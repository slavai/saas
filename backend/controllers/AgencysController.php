<?php

namespace backend\controllers;

use common\models\AjaxValidation;
use common\models\Countries;
use common\models\LoginData;
use thamtech\uuid\helpers\UuidHelper;
use Yii;
use common\models\Agencys;
use backend\models\AgencysSearch;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * AgencysController implements the CRUD actions for Agencys model.
 */
class AgencysController extends Controller
{
    use AjaxValidation;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'reset-password', 'active-block-user'],
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Agencys models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgencysSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agencys model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $data['countries'] = Countries::find()->orderBy('countries_name')->all();

        $userRole = Yii::$app->user->identity->type == LoginData::TYPE_AGENCY ? LoginData::TYPE_AGENCY_NAME : LoginData::TYPE_PHYSICIAN_NAME;

        return $this->render('view', [
            'data' => $data,
            'model' => $model
        ]);
    }

    /**
     * Creates a new Agencys model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agencys();
        $loginData = new LoginData();

        $data['countries'] = Countries::find()->orderBy('countries_name')->all();
        $data['detail'] = $loginData;

        // ajax validator
        $this->performAjaxValidation($loginData);
        $this->performAjaxValidation($model);

        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $model->load($post);
            $loginData->load($post);

            $password = Yii::$app->security->generateRandomString(12);
            $loginData->setPassword($password);
            $loginData->auth_key = Yii::$app->security->generateRandomString();

            if (!ActiveForm::validate($loginData)) {
                $loginData->type = LoginData::TYPE_AGENCY;
                $loginData->status = LoginData::STATUS_INACTIVE;
                $loginData->short_id = UuidHelper::uuid();

                if ($loginData->save()) {
                    $model->id = $loginData->id;
                    if (! empty($_FILES[$model->shortClassName()]['name']['avatar_filename']))
                        $model->avatar_filename = $model->setUserAvatar($_FILES[$model->shortClassName()]);

                    if (!ActiveForm::validate($model) && $model->save()) {
                        $auth = Yii::$app->authManager;
                        $authorRole = $auth->getRole(Agencys::ROLE_NAME);
                        $auth->assign($authorRole, $loginData->id);

                        \Yii::$app->mailer->compose(['html' => 'activation-backend-html'], ['user' => $loginData, 'password' => $password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                            ->setTo($loginData->email)
                            ->setSubject('Activate account for ' . \Yii::$app->name)
                            ->send();

                        \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Your profile has been created'));
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        echo json_encode(ActiveForm::validate($model));
                        \Yii::$app->end();
                    }
                }
            } else {
                echo json_encode(ActiveForm::validate($loginData));
                \Yii::$app->end();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Updates an existing Agencys model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $data['countries'] = Countries::find()->orderBy('countries_name')->all();

        // ajax validator
        $this->performAjaxValidation($model);

        if (\Yii::$app->request->isPost) {
            $oldAvatar = $model->avatar_filename;
            $post = \Yii::$app->request->post();
            $model->load($post);

            if (!ActiveForm::validate($model)) {
                if (! empty($_FILES[$model->shortClassName()]['name']['avatar_filename']))
                    $model->avatar_filename = $model->setUserAvatar($_FILES[$model->shortClassName()]);
                else
                    $model->avatar_filename = $oldAvatar;

                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Your profile has been updated'));
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                echo json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Deletes an existing Agencys model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Agencys model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agencys the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agencys::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionResetPassword($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = LoginData::findOne([
            'status' => LoginData::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if ($user) {
            if (!LoginData::isPasswordResetTokenValid($user->password_reset_token))
                $user->generatePasswordResetToken();

            if ($user->save())
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html'], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($user->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
        }

        return ['result' => 'error'];
    }

    public function renderGrid() {
        $searchModel = new AgencysSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return self::renderPartial('_grid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActiveBlockUser()
    {
        $get = Yii::$app->request->get();
        $model = LoginData::findOne($get['id']);

        if($model->status == LoginData::STATUS_ACTIVE)
            $model->status = LoginData::STATUS_BLOCKED;
        else $model->status = LoginData::STATUS_ACTIVE;

        $model->save();

        echo self::renderGrid();
        \Yii::$app->end();
    }
}
