<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Agencys;

/**
 * AgencysSearch represents the model behind the search form about `common\models\Agencys`.
 */
class AgencysSearch extends Agencys
{
    public $email;
    public $status;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'email', 'status', 'admin_name', 'phone', 'phone_fax', 'address', 'agency_email', 'skilled_nursing', 'physical_therapy',
                'speech_language_therapy', 'occupational_therapy', 'home_health_aide', 'medical_social_services', 'summary',
                'avatar_filename', 'city', 'state', 'country_code', 'postCode'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'User Email'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agencys::find()->joinWith(['user']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['status'] = [
            'asc' => ['user.status' => SORT_ASC],
            'desc' => ['user.status' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'user.email', $this->email]);
        $query->andFilterWhere(['like', 'user.status', $this->status]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'admin_name', $this->admin_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_fax', $this->phone_fax])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'agency_email', $this->agency_email])
            ->andFilterWhere(['like', 'skilled_nursing', $this->skilled_nursing])
            ->andFilterWhere(['like', 'physical_therapy', $this->physical_therapy])
            ->andFilterWhere(['like', 'speech_language_therapy', $this->speech_language_therapy])
            ->andFilterWhere(['like', 'occupational_therapy', $this->occupational_therapy])
            ->andFilterWhere(['like', 'home_health_aide', $this->home_health_aide])
            ->andFilterWhere(['like', 'medical_social_services', $this->medical_social_services])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'avatar_filename', $this->avatar_filename])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'country_code', $this->country_code])
            ->andFilterWhere(['like', 'postCode', $this->postCode]);
//        var_dump($dataProvider);
        return $dataProvider;
    }
}
