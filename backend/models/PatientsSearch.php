<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Patients;

/**
 * PatientsSearch represents the model behind the search form about `common\models\Patients`.
 */
class PatientsSearch extends Patients
{
    public $physician_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_physician', 'birthday', 'date_create', 'date_care_end'], 'integer'],
            [['name', 'physician_name', 'phone', 'address', 'email', 'gender', 'photo', 'ss', 'emergency_contact_name',
                'emergency_contact_phone', 'emergency_contact_address', 'insurance_plan1', 'insurance_policy1',
                'insurance_plan2', 'insurance_policy2', 'clinical_findings', 'reason_patient_homebound',
                'status_care'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'physician_name' => Yii::t('app', 'Physician name')
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patients::find()->joinWith('physician');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['physician_name'] = [
            'asc' => ['physician.name' => SORT_ASC],
            'desc' => ['physician.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_physician' => $this->id_physician,
            'birthday' => $this->birthday,
            'date_create' => $this->date_create,
            'date_care_end' => $this->date_care_end,
        ]);

        $query->andFilterWhere(['like', 'physician.name', $this->physician_name]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'ss', $this->ss])
            ->andFilterWhere(['like', 'emergency_contact_name', $this->emergency_contact_name])
            ->andFilterWhere(['like', 'emergency_contact_phone', $this->emergency_contact_phone])
            ->andFilterWhere(['like', 'emergency_contact_address', $this->emergency_contact_address])
            ->andFilterWhere(['like', 'insurance_plan1', $this->insurance_plan1])
            ->andFilterWhere(['like', 'insurance_policy1', $this->insurance_policy1])
            ->andFilterWhere(['like', 'insurance_plan2', $this->insurance_plan2])
            ->andFilterWhere(['like', 'insurance_policy2', $this->insurance_policy2])
            ->andFilterWhere(['like', 'clinical_findings', $this->clinical_findings])
            ->andFilterWhere(['like', 'reason_patient_homebound', $this->reason_patient_homebound])
            ->andFilterWhere(['like', 'status_care', $this->status_care]);

        return $dataProvider;
    }
}
