<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Physician;

/**
 * PhysicianSearch represents the model behind the search form about `common\models\Physician`.
 */
class PhysicianSearch extends Physician
{
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'email', 'manager_name', 'phone', 'phone_fax', 'address', 'license', 'education', 'physician_email',
                'specialization', 'summary', 'avatar_filename'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'User Email')
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Physician::find()->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'user.email', $this->email]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'manager_name', $this->manager_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_fax', $this->phone_fax])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'physician_email', $this->physician_email])
            ->andFilterWhere(['like', 'license', $this->license])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'specialization', $this->specialization])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'avatar_filename', $this->avatar_filename]);
        return $dataProvider;
    }
}
