'use strict';

var users = angular.module('users', ['ngSanitize', 'ui.select']);
users.run(function($http) {
    $http.defaults.headers.post = {'Content-Type': 'application/x-www-form-urlencoded'};
    $http.defaults.transformRequest = function(obj) {
        var str = [];
        for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    };
});

users.controller('main', ['$scope', '$http', function($scope, $http) {
    $scope.user = {};
    $scope.users = physician;
    $scope.associate_user = '';
    $scope.all_nonassociate_user = [];

    $scope.deleteAssociateAgency = function(agency) {
        swal({
            title: "Are you sure?",
            text: "You can restore this agency via 'Restore' button later",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Confirm",
            closeOnConfirm: false
        },
        function() {
            $http.post('/physicians/delete-associate-agency', {
                id_agency: agency.id,
                id_physician : $scope.user.selected.id,
                _csrf: yii.getCsrfToken()
            }).then(function(response) {
                if (response.data.result == 'success') {
                    swal("Deleted", "Associate agency successfully deleted", "success");
                    $scope.associate_user = response.data.userAssociate.physicianRecomendationAgency;
                    $scope.all_nonassociate_user = response.data.allAgency;
                }
                else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            });
        });
    };

    $scope.addAssociateAgency = function(associate_user) {
        $http.post('/physicians/add-associate-agency', {
            id_agency: associate_user.id,
            id_physician : $scope.user.selected.id,
            _csrf: yii.getCsrfToken()
        }).then(function(response) {
            if (response.data.result == 'success') {
                swal("Added", "Associate agency successfully added", "success");
                $scope.associate_user = response.data.userAssociate.physicianRecomendationAgency;
                $scope.all_nonassociate_user = response.data.allAgency;
            }
            else if (response.data.result == 'error') {
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
            else if (response.data.result == 'exist') {
                sweetAlert("Exist...", "Exist!", "success");
            }
        });
    };

    $scope.changeUser = function(user) {
        $http.post('/physicians/get-associate-user', {
            id: user.id,
            _csrf: yii.getCsrfToken()
        }).then(function (response) {
            if (response.data.result == 'success') {
                $scope.associate_user = response.data.userAssociate.physicianRecomendationAgency;
                $scope.all_nonassociate_user = response.data.allAgency;
            }
        });
    };
}]);