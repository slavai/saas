'use strict';

var users = angular.module('users', []);
users.run(function($http) {
    $http.defaults.headers.post = {'Content-Type': 'application/x-www-form-urlencoded'};
    $http.defaults.transformRequest = function(obj) {
        var str = [];
        for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    };
});

users.controller('main', ['$scope', '$http', function($scope, $http) {
    $scope.users = admins;

    $scope.blockUser = function(user) {
        swal({
            title: "Are you sure?",
            text: "You can unblock this user later",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Confirm",
            closeOnConfirm: false
        },
        function() {
            $http.post('/user/block', {
                id: user.id,
                _csrf: yii.getCsrfToken()
            }).then(function(response) {
                if (response.data.result == 'success') {
                    swal("Blocked", "User successfully blocked", "success");
                    user.loginData.status = response.data.userStatus;
                }
            });
        });
    };

    $scope.deleteUser = function(user) {
        swal({
            title: "Are you sure?",
            text: "You can restore this user via 'Restore' button later",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Confirm",
            closeOnConfirm: false
        },
        function() {
            $http.post('/user/delete', {
                id: user.id,
                _csrf: yii.getCsrfToken()
            }).then(function(response) {
                if (response.data.result == 'success') {
                    swal("Deleted", "User successfully deleted", "success");
                    user.loginData.status = response.data.userStatus;
                }
            });
        });
    };

    $scope.restoreUser = function(user) {
        swal({
            title: "Are you sure?",
            text: "User will be restored after you push 'Confirm' button",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Confirm",
            closeOnConfirm: false
        },
        function() {
            $http.post('/user/restore', {
                id: user.id,
                _csrf: yii.getCsrfToken()
            }).then(function(response) {
                if (response.data.result == 'success') {
                    swal("Restored", "User successfully restored", "success");
                    user.loginData.status = response.data.userStatus;
                }
            });
        });
    };

    $scope.getStatus = function(status, deleted, active, inactive, blocked) {
        switch (parseInt(status)) {
            case deleted:
                return 'Deleted';
            case active:
                return 'Active';
            case inactive:
                return 'Inactive';
            case blocked:
                return 'Blocked';
        }

        return 'Undefined';
    };

    $scope.resetPassword = function(email, isActive) {
        if (!isActive)
            return false;

        $http.post('/api/send-reset-password-email', {
            email: email,
            _csrf: yii.getCsrfToken()
        }).then(function(response) {
            if (response.data === true)
                swal('Success', 'Email with further instructions has been sent', 'success');
            else
                swal('Error', 'Something went wrong.', 'error');
        });
    };
}]);