<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AngularAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );

    public $css = [
        'angular-ui-select/dist/select.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.3/css/selectize.default.min.css',
    ];
    public $js = [
        'angular/angular.js',
        'angular-sanitize/angular-sanitize.min.js',
        'angular-ui-select/dist/select.min.js',
    ];
    public $depends = [
        'backend\assets\AppAsset'
    ];
}
