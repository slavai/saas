<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\View;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );
    public $css = [
        'css/bootstrap-reset.css',
        'css/style-responsive.css',
        'css/style.css',
        'css/site.css',
        'js/css3clock/css/style.css',
        'css/clndr.css'
    ];
    public $js = [
        'js/css3clock/js/css3clock.js',
        'js/calendar/clndr.js',
        'js/calendar/moment-2.2.1.js',
        'js/evnt.calendar.init.js',
        'js/jquery.dcjqaccordion.2.7.js',
        'js/scripts.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'backend\assets\BowerAsset'
    ];
}
