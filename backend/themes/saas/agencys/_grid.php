<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Agencys');

echo \kartik\grid\GridView::widget([
    'id' => 'grid-agency-list',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'panel'=>[
        'heading'=>'<h3 class="panel-title"><i class="fa fa-group"></i> ' . $this->title . '</h3>',
        'type'=>'success',
        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Agency'), ['create'], ['class' => 'btn btn-success btn-sm', 'data-pjax' => '0']),
        'footer'=>false
    ],
    'pjax'=>true,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'email',
            'value' => 'user.email'
        ],
        'name',
        'admin_name',
        'phone',
//        'phone_fax',
        'address',
        'agency_email:email',
        // 'skilled_nursing:ntext',
        // 'physical_therapy:ntext',
        // 'speech_language_therapy:ntext',
        // 'occupational_therapy:ntext',
        // 'home_health_aide:ntext',
        // 'medical_social_services:ntext',
        // 'summary:ntext',
        // 'logo',
        [
            'attribute' => 'status',
//            'value' => 'user.status',
            'value' => function ($model) {
                if ($model->user->status == 10) {
                    $act = 'block';
                    $act_text = "Сlick to lock the user";
                }
                else {
                    $act = 'activate';
                    $act_text = "Click to unlock the user";
                }

                return Html::a(Yii::t('app', $model->user->getStatus()), false, [
                    'class' => ($model->user->status == 10) ? 'btn btn-xs btn-block btn-activation btn-success' : 'btn btn-xs btn-block btn-activation btn-danger',
                    'data-confirm-val' => Yii::t('app', 'Are you sure you want to '. $act .' this newsletter?'),
                    'data-pjax' => '1',
                    'activate-url'=> '/agencys/active-block-user/'.$model->id,
                    'data-toggle' => 'tooltip',
                    'data-original-title' => $act_text
                ]);
            },
            'format' => 'raw'
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => true,
            'template' => '{view}{update}{reset-password}{delete}',
            'dropdownOptions'=>['class'=>'pull-right'],
            'headerOptions'=>['class'=>'kartik-sheet-style'],
            'buttons' => [
                'reset-password' => function ($url, $model) {
                    return  "<li>".Html::a('<span class="fa fa-refresh"></span> '.Yii::t('app', 'Reset Password'), $url,
                        [ 'title' => Yii::t('app', 'Reset Password'),
                            'data-url' => '/api/send-reset-password-email',
                            'data-email' => $model->user->email,
                            'class' => 'ajaxResetPassword',
                        ])."</li>" ;
                },
            ]
        ],
    ],
    'toolbar' => [
        [
            'content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], [
                    'class' => 'btn btn-default btn-sm',
                    'title' => Yii::t('app', 'Reset Grid')
                ]),
        ],
        '{toggleData}'
    ],
    'toggleDataOptions' => [
        'all' => [
            'icon' => 'resize-full',
            'class' => 'btn btn-default btn-sm',
        ]
    ],
    'responsive'=>false,
    'hover'=>true,
    'condensed'=>true,
    'striped'=>true,
]);

?>