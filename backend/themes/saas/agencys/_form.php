<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyAH3VMe0m6UPiblV96_PsAGS0aaCOC-JnM&libraries=places', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/account_info.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $model common\models\Agencys */
/* @var $form yii\widgets\ActiveForm */
$option = [
    'clientOptions' => ['height' => 100],
    'options' => ['rows' => 6],
    'preset' => 'basic'
];
?>

<div class="row agencys-form">
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            'validateOnSubmit' => true,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3',
                    'wrapper' => 'col-sm-9',
                    'error' => 'col-sm-9',
                    'hint' => '',
                ],
            ],
            'layout' => 'horizontal',
            'options' => ['enctype' => 'multipart/form-data']]);
        ?>

        <?php if ($model->isNewRecord) : ?>

            <?= $form->field($data['detail'], 'email')->textInput(['maxlength' => true]) ?>

        <?php endif; ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'admin_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone_fax')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'agency_email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'skilled_nursing')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'physical_therapy')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'speech_language_therapy')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'occupational_therapy')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'home_health_aide')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'medical_social_services')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'summary')->widget(CKEditor::className(), $option) ?>

        <?= $form->field($model, 'avatar_filename')->widget(FileInput::classname(), [
            'options' => ['multiple' => false, 'accept' => 'image/*'],
            'pluginOptions' => [
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'removeClass' => 'btn btn-danger',
                'mainClass' => 'input-group-sm',
                'initialPreview' => [
                    Html::img(\Yii::$app->urlManagerFrontEnd->baseUrl . $model->logo, ['class' => 'file-preview-image'])
                ],
                'pluginOptions' => ['previewFileType' => 'any'],
                'allowedFileExtensions'=>['jpg','gif','png']
            ]
        ]); ?>

        <?= $form->field($model, 'city', [
            'inputOptions' => [
                'id' => 'autocomplete-city',
                'placeholder' => $model->getAttributeLabel('city'),
            ]]); ?>

        <?= $form->field($model, 'state', [
            'inputOptions' => [
                'placeholder' => $model->getAttributeLabel('state'),
            ]]); ?>

        <?php
            $phoneCode = [];
            foreach ($data['countries'] as $country)
                $phoneCode[$country->countries_iso_code_2] = array('data-phone-code' => $country->phone_prefix);

            echo $form->field($model, 'country_code')->listBox(
                ArrayHelper::map($data['countries'], 'countries_iso_code_2',
                    function($model, $defaultValue) {
                        return $model->countries_name.' (+'.$model->phone_prefix.')';
                    }),
                ['prompt'=>Yii::t('app', 'Country'), 'multiple' => false, 'size'=>1, 'id'=>'field_country']
            )->label(Yii::t('app', 'Country').':');
        ?>

        <?= $form->field($model, 'postCode', [
            'inputOptions' => [
                'placeholder' => $model->getAttributeLabel('postCode'),
        ]]); ?>

        <div class="form-group row pull-right">
            <div class="col-sm-12">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

