<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Agencys */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agencys-view">
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <div class="panel-body profile-information">
                    <div class="col-md-2">
                        <div class="profile-pic text-center">
                            <img src="<?= \Yii::$app->urlManagerFrontEnd->baseUrl . $model->getLogo('square') ?>" alt=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="profile-desk">
                            <h1><?= $model->name ?></h1>
                            <span class="text-muted" style=""><?= $model::ROLE_NAME ?></span><br>
                            <?= $model->summary ?><br>

                            <p>
                                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="profile-statistics">
                            <h1><?= $model->admin_name ?></h1>
                            <p><?= Yii::t('app', 'Admin name') ?></p>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue">
                    <ul class="nav nav-tabs nav-justified ">
                        <li class="active">
                            <a data-toggle="tab" href="#overview">
                                <?= Yii::t('app', 'Overview') ?>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#contacts" class="contact-map">
                                <?= Yii::t('app', 'Contacts') ?>
                            </a>
                        </li>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content tasi-tab">
                        <div id="overview" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-8">
                                    <div>
                                        <h3><?= Yii::t('app', 'Skills') ?></h3>

                                        <?php if (!empty($model->skilled_nursing)): ?>
                                            <div class="activity-desk skill-view">
                                                <div class="activity-icon terques">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h2><?= Yii::t('app', "Skilled-Nursing");?></h2>
                                                    <?= $model->skilled_nursing; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($model->physical_therapy)): ?>
                                            <div class="activity-desk skill-view">
                                                <div class="activity-icon red ">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h2 class="red"><?= Yii::t('app', "Physical Therapy");?></h2>
                                                    <?= $model->physical_therapy; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($model->speech_language_therapy)): ?>
                                            <div class="activity-desk skill-view">
                                                <div class="activity-icon green">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h2 class="green"><?= Yii::t('app', "Speech-Language Therapy");?></h2>
                                                    <?= $model->speech_language_therapy; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($model->occupational_therapy)): ?>
                                            <div class="activity-desk skill-view">
                                                <div class="activity-icon yellow">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h2 class="yellow"><?= Yii::t('app', "Occupational Therapy");?></h2>
                                                    <?= $model->occupational_therapy; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($model->home_health_aide)): ?>
                                            <div class="activity-desk skill-view">
                                                <div class="activity-icon blue">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h2 class="blue"><?= Yii::t('app', "Home Health Aide");?></h2>
                                                    <?= $model->home_health_aide; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($model->medical_social_services)): ?>
                                            <div class="activity-desk skill-view">
                                                <div class="activity-icon bg-purple">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h2 class="purple"><?= Yii::t('app', "Medical Social Services");?></h2>
                                                    <?= $model->medical_social_services; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="prf-box">
                                        <h3 class="prf-border-head"><?= Yii::t('app', "performance status") ?></h3>
                                        <div class=" wk-progress pf-status">
                                            <div class="col-md-8 col-xs-8"><?= Yii::t('app', "Total patients") ?></div>
                                            <div class="col-md-4 col-xs-4">
                                                <strong>23545</strong>
                                            </div>
                                        </div>
                                        <div class=" wk-progress pf-status">
                                            <div class="col-md-8 col-xs-8"><?= Yii::t('app', "Total patients") ?></div>
                                            <div class="col-md-4 col-xs-4">
                                                <strong>235</strong>
                                            </div>
                                        </div>
                                        <div class=" wk-progress pf-status">
                                            <div class="col-md-8 col-xs-8"><?= Yii::t('app', "Total patients") ?></div>
                                            <div class="col-md-4 col-xs-4">
                                                <strong>235452344$</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="contacts" class="tab-pane ">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="prf-contacts">
                                        <h2> <span><i class="fa fa-map-marker"></i></span> <?= Yii::t('app', 'location') ?></h2>
                                        <div class="location-info">
                                            <p><?= Yii::t('app', 'Address') ?><br>
                                                <?= $model->address ?><br>
                                                <?= $model->city ?> <?= $model->state ?> <?= $model->postCode ?> <?= $model->country->countries_name ?></p>
                                        </div>
                                        <h2> <span><i class="fa fa-phone"></i></span> <?= Yii::t('app', 'contacts') ?></h2>
                                        <div class="location-info">
                                            <p><?= Yii::t('app', 'Phone') ?> : <?= $model->phone ?> <br>
                                                <?= Yii::t('app', 'Fax') ?> : <?= $model->phone_fax ?></p>
                                            <p><?= Yii::t('app', 'Email') ?> : <?= $model->agency_email ?></p>
<!--                                            <p>-->
<!--                                                Facebook	: https://www.facebook.com/themebuckets <br>-->
<!--                                                Twitter	: https://twitter.com/theme_bucket-->
<!--                                            </p>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
