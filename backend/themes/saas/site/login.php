<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container login-body">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-signin']
    ]); ?>

    <h2 class="form-signin-heading">sign in now</h2>
    <div class="login-wrap">
        <div class="user-login-info">
            <?= $form->field($model, 'email', [
                'inputOptions' => [
                    'autofocus' => 'autofocus', 'class' => 'form-control', 'placeholder' => $model->getAttributeLabel('email')
                ]
            ])->label(false); ?>
            <?= $form->field($model, 'password', [
                'inputOptions' => [
                    'class' => 'form-control', 'placeholder' => $model->getAttributeLabel('password')
                ]
            ])->passwordInput()->label(false); ?>
        </div>

        <?= $form->field($model, 'rememberMe', ['options' => ['class' => 'pull-left', 'style' => 'margin-left: -15px;']])->checkbox() ?>

        <span class="pull-right" style="padding-top: 10px;">
                <a data-toggle="modal" href="#myModal"> Forgot Password?</a>
            </span>

        <?= Html::submitButton('Sign in', ['class' => 'btn btn-lg btn-login btn-block', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Forgot Password ?</h4>
                </div>
                <div class="modal-body">
                    <p>Enter your e-mail address below to reset your password.</p>
                    <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    <button class="btn btn-success" type="button">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->
</div>