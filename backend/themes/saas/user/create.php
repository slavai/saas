<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\file\FileInput;

$this->title = Yii::t('app', $editing ? 'Create User' : 'Edit User');
$this->params['breadcrumbs'] = [
    ['label' => 'Admin users', 'url' => Url::to(['/user'])],
    ['label' => $editing ? 'Create' : 'Edit'],
];

$this->registerCssFile(Yii::$app->request->baseUrl.'/css/user.css');
?>
<section class="panel col-md-6 col-md-offset-3">
    <div class="create-user-wrapper panel-body">
        <h1 class="text-center">
            <?= Yii::t('app', $editing ? 'Create User' : 'Edit User'); ?>
        </h1>
        <?php $form = ActiveForm::begin([
            'id' => 'create-user-form',
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            'validateOnSubmit' => true,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-md-4',
                    'wrapper' => 'col-md-7',
                ],
            ],
            'options' => ['enctype' => 'multipart/form-data'],
            'layout' => 'horizontal',
        ]); ?>
        <?= $form->field($userData, 'name'); ?>
        <?= $form->field($loginData, 'email', [
            'inputOptions' => [
                'type' => 'email',
            ],
        ]); ?>
        <?= $form->field($userData, 'avatar_filename')->widget(FileInput::classname(), [
            'options' => ['multiple' => false, 'accept' => 'image/*'],
            'pluginOptions' => [
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'removeClass' => 'btn btn-danger',
                'mainClass' => 'input-group-sm',
                'initialPreview' => [
                    Html::img(\Yii::$app->urlManagerFrontEnd->baseUrl . $userData->logo, ['class' => 'file-preview-image'])
                ],
                'pluginOptions' => ['previewFileType' => 'any'],
                'allowedFileExtensions'=>['jpg','gif','png']
            ]
        ]); ?>
        <div class="form-group">
            <label class="control-label col-md-4"><?= $userData->getAttributeLabel('gender'); ?></label>
            <div class="col-md-7 user-field">
                <?= Html::activeDropDownList($userData, 'gender', [
                    'M' => Yii::t('app', 'Male'),
                    'F' => Yii::t('app', 'Female'),
                    '' => Yii::t('app', 'Rather not say'),
                ], [
                    'class' => 'form-control m-bot15',
                ]); ?>
            </div>
        </div>
        <div class="pull-right">
            <a class="btn btn-default" href="<?= Url::to(['/user']); ?>"><?= Yii::t('app', 'Cancel'); ?></a>
            <?= Html::submitButton(Yii::t('app', $editing ? 'Create' : 'Save'), [
                'class' => 'btn btn-primary',
            ]); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>