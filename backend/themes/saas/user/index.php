<?php
use yii\helpers\Url;
use common\models\LoginData;
use backend\assets\AngularAsset;

$this->title = Yii::t('app', 'Admin Users');
$this->params['breadcrumbs'] = [
    ['label' => 'Admin users'],
];

AngularAsset::register($this);
$this->registerCssFile(Yii::$app->request->baseUrl.'/css/user.css');
$this->registerJsFile(Yii::$app->request->baseUrl.'/js/user.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<script>
    var admins = <?= json_encode($users); ?>;
</script>
<section class="panel" ng-app="users">
    <div class="users panel-body" ng-controller="main">
        <a class="btn btn-primary pull-right new-user-link" href="<?= Url::to(['/user/create']); ?>"><?= Yii::t('app', 'New User'); ?></a>
        <table class="table table-striped table-hover table-bordered" id="users-table">
            <thead>
            <tr>
                <th><?= Yii::t('app', 'Name'); ?></th>
                <th><?= Yii::t('app', 'Email'); ?></th>
                <th><?= Yii::t('app', 'Role'); ?></th>
                <th><?= Yii::t('app', 'Status'); ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <tr class="user" id="{{user.id}}" ng-repeat="user in users" ng-cloak>
                    <td class="user-field">{{user.name}}</td>
                    <td class="user-field">{{user.loginData.email}}</td>
                    <td class="user-short-field">
                        <span class="user-role-text label label-primary">{{user.loginData.relationRole[0].name}}</span>
                    </td>
                    <td class="user-status-field">
                        {{getStatus(user.loginData.status, <?= LoginData::STATUS_DELETED; ?>, <?= LoginData::STATUS_ACTIVE; ?>, <?= LoginData::STATUS_INACTIVE; ?>, <?= LoginData::STATUS_BLOCKED; ?>)}}
                    </td>
                    <td class="user-short-field text-center col-sm-2 col-md-1 col-lg-1">
                        <div class="dropdown">
                            <button class="btn btn-info btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <?= Yii::t('app', 'Actions'); ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="<?= Url::to(['/user/create']); ?>/{{user.id}}"><?= Yii::t('app', 'Edit'); ?></a></li>
                                <li ng-class="{disabled: user.loginData.status !== '<?= LoginData::STATUS_ACTIVE; ?>'}">
                                    <span ng-click="resetPassword(user.loginData.email, user.loginData.status === '<?= LoginData::STATUS_ACTIVE; ?>')">
                                        <?= Yii::t('app', 'Reset Password'); ?>
                                    </span>
                                </li>
                                <li ng-show="user.loginData.status === <?= LoginData::STATUS_BLOCKED; ?>"><span ng-click="restoreUser(user)"><?= Yii::t('app', 'Unblock'); ?></span></li>
                                <li ng-show="user.loginData.status !== <?= LoginData::STATUS_BLOCKED; ?>"><span ng-click="blockUser(user)"><?= Yii::t('app', 'Block'); ?></span></li>
                                <li ng-show="user.loginData.status === <?= LoginData::STATUS_DELETED; ?>"><span ng-click="restoreUser(user)"><?= Yii::t('app', 'Restore'); ?></span></li>
                                <li ng-show="user.loginData.status !== <?= LoginData::STATUS_DELETED; ?>"><span ng-click="deleteUser(user)"><?= Yii::t('app', 'Delete'); ?></span></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
