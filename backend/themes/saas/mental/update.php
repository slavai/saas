<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MentalStatus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mental Status',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mental Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<section class="panel mental-status-update col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
