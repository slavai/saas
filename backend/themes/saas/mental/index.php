<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MentalStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mental Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'panel'=>[
        'heading'=>'<h3 class="panel-title"><i class="fa fa-group"></i> '. $this->title.'</h3>',
        'type'=>'success',
        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Mental Status'), ['create'], ['class' => 'btn btn-success btn-sm', 'data-pjax' => '0']),
        'footer'=>false
    ],
    'pjax'=>true,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'num',
        'name',

        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => true,
            'dropdownOptions'=>['class'=>'pull-right'],
            'headerOptions'=>['class'=>'kartik-sheet-style']
        ],
    ],
    'toolbar' => [
        [
            'content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], [
                    'class' => 'btn btn-default btn-sm',
                    'title' => Yii::t('app', 'Reset Grid')
                ]),
        ],
        '{toggleData}'
    ],
    'toggleDataOptions' => [
        'all' => [
            'icon' => 'resize-full',
            'class' => 'btn btn-default btn-sm',
        ]
    ],
    'responsive'=>true,
    'hover'=>true,
    'condensed'=>true,
    'striped'=>true,
]); ?>

