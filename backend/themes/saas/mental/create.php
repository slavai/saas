<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MentalStatus */

$this->title = Yii::t('app', 'Create Mental Status');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mental Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="panel mental-status-create col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
