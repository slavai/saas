<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $this  yii\web\View
 * @var $model dektrium\rbac\models\Role
 */

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(['fieldConfig' => [
    'horizontalCssClasses' => [
        'label' => 'col-sm-3',
        'wrapper' => 'col-sm-9',
        'error' => 'col-sm-9',
        'hint' => '',
    ],
],
    'layout' => 'horizontal']); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'rule') ?>

    <?= $form->field($model, 'children')->widget(Select2::className(), [
        'data' => $model->getUnassignedItems(),
        'options' => [
            'id' => 'children',
            'multiple' => true
        ],
    ]) ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-9">
            <?= Html::submitButton(Yii::t('rbac', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>