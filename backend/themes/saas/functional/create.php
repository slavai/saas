<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FunctionalLimitations */

$this->title = Yii::t('app', 'Create Functional Limitations');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Functional Limitations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="panel functional-limitations-create col-sm-12 col-md-10 col-xs-8">
    <header class="row panel-heading">
        <?= Html::encode($this->title) ?>
    </header>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
