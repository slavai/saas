<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use backend\controllers\SiteController;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
    <?php $this->beginBody() ?>
    <section id="container">
        <!--header start-->
        <header class="header fixed-top clearfix">
            <!--logo start-->
            <div class="brand">
                <a href="<?= Url::home(); ?>" class="logo">
                    Saas Admin
                </a>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
                </div>
            </div>
            <!--logo end-->

            <div class="nav notify-row" id="top_menu">
                <!-- Notifications here -->
            </div>
            <div class="top-nav clearfix">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <a href="<?= Url::to('/site/lock'); ?>" class="btn btn-primary pull-left"><?= Yii::t('app', 'Lock Screen'); ?></a>
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle icon-user" href="#">
                            <img src="<?= \Yii::$app->urlManagerFrontEnd->baseUrl . $this->context->user->userData->getLogo('square') ?>" alt="" />
                            <span class="username"><?= $this->context->user->userData->name; ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <!--                            <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>-->
                            <!--                            <li><a href="#"><i class="fa fa-cog"></i>Settings')</a></li>-->
                            <li><a data-method="post" href="<?= Url::toRoute(['/site/logout']) ?>"><i class="fa fa-key"></i><?= Yii::t('app', 'Log Out'); ?></a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
        <!--header end-->
        <!-- sidebar start -->
        <aside>
            <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start -->
                <div class="leftside-navigation">
                    <?= Menu::widget([
                        'items' => SiteController::getMenuItems(),
                        'options' => [
                            'class' => 'sidebar-menu',
                            'id' => 'nav-accordion'
                        ],
                    ]); ?>
                </div>
            </div>
        </aside>
        <!--sidebar end-->
        <!-- main content start -->
        <section id="main-content">
            <section class="wrapper">

                <?php if (! empty($this->params['breadcrumbs'])) : ?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--breadcrumbs start -->
                            <?= Breadcrumbs::widget([
                                'links' => $this->params['breadcrumbs']
                            ]); ?>
                            <!--breadcrumbs end -->
                        </div>
                    </div>
                <?php endif; ?>

                <!-- page start -->
                <div class="row">
                    <div class="col-md-12">
                        <?= $content; ?>
                    </div>
                </div>
                <!-- page end -->
            </section>
        </section>
        <!-- main content end -->
    </section>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
