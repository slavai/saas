<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta charset="<?= Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags(); ?>
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
    <?php $this->beginBody(); ?>

    <section id="container">
        <!-- page start -->
            <?= $content; ?>
        <!-- page end -->
    </section>

    <?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>