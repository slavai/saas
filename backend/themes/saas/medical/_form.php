<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MedicalNecessityList */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row medical-necessity-list-form">
    <div class="panel-body">
    <?php $form = ActiveForm::begin(['fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'wrapper' => 'col-sm-9',
            'error' => 'col-sm-9',
            'hint' => '',
        ],
    ],
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group row pull-right">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
